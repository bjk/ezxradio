/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <iostream>
#include <cerrno>
#include <qbuffer.h>
#include <qfile.h>
#include <qfileinfo.h>
#include <qregexp.h>
#include "playList.h"

RadioPlayList::RadioPlayList()
{
    playList.setAutoDelete(true);
}

RadioPlayList::~RadioPlayList()
{
    playList.clear();
}

void RadioPlayList::doParse(QTextStream &t, Type type, bool reset)
{
    /* Fixes some broken playlists. It'll only treat lines as URLs without
     * titles or length specification. */
    if (type == PlayListBroken) {
	QString line;
	int total = 0;

	while ((line = t.readLine()) != QString::null) {
	    line = line.simplifyWhiteSpace();

	    if (line.left(7).lower() == "http://") {
		if (!total++ && reset)
		    playList.clear();

		parseUrl(line, reset);
	    }
	}
    }
    else if (type == PlayListPLS) {
	int total = 0;
	QString line;
	QRegExp uexp("^file[0-9]*[ ]*=[ ]*", FALSE);
	QRegExp texp("^title[0-9]*[ ]*=[ ]*", FALSE);
	QRegExp lexp("^length[0-9]*[ ]*=[ ]*", FALSE);
	int n = -1;

	while ((line = t.readLine()) != QString::null) {
	    n = line.find("[playlist]");

	    if (n != -1)
		break;
	}

	if (line == QString::null || n == -1)
	    goto done;

	while ((line = t.readLine()) != QString::null) {
	    int len;
again:
	    bool match = false;
	    line = line.simplifyWhiteSpace();
	    int n = uexp.match(line, 0, &len);
	    QString url = QString::null, title = QString::null;
	    long length = -1;

	    if (n >= 0) {
		url = line.mid(len);
		line = t.readLine();

		if (line != QString::null) {
		    line = line.simplifyWhiteSpace();
		    n = texp.match(line, 0, &len);

		    if (n >= 0) {
			title = line.mid(len);
			line = t.readLine();

			if (line != QString::null) {
			    line = line.simplifyWhiteSpace();
			    n = lexp.match(line, 0, &len);

			    if (n >= 0) {
				match = true;
				bool ok;
				line = line.mid(len);
				length = line.toLong(&ok);
			    }
			}
		    }
		}

		if (!total++ && reset)
		    playList.clear();

		append(url, title);

		if (!match)
		    goto again;
	    }
	}
    }
    else if (type == PlayListM3U) {
	int total = 0;
	QString line;
	QRegExp texp("^#EXTINF:[-0-9]*,", FALSE);
	int n = -1;

	while ((line = t.readLine()) != QString::null) {
	    n = line.find("#EXTM3U");

	    if (n != -1)
		break;
	}

	if (line == QString::null || n == -1) {
	    t.reset();

	    while ((line = t.readLine()) != QString::null)
		parseUrl(line.simplifyWhiteSpace(), reset);

	    goto done;
	}

	while ((line = t.readLine()) != QString::null) {
	    line = line.simplifyWhiteSpace();
	    int len;
	    int n = texp.match(line, 0, &len);
	    long length = -1;
	    bool ok;
	    QString url = QString::null, title = QString::null;

	    if (n >= 0) {
		length = line.mid(8).toLong(&ok);
		title = line.mid(len);
		line = t.readLine();

		if (line == QString::null)
		    break;

		n = texp.match(line, 0, &len);

		/* two #EXTINF in a row, malformed m3u */
		if (n != -1)
		    goto done;

		url = line;

		if (!total++ && reset)
		    playList.clear();

		append(url, title);
	    }
	}
    }

done:
    return;
}

bool RadioPlayList::parse(const unsigned char *buf, size_t size, Type type)
{
    QBuffer b;
    QByteArray a;
    QTextStream t;

    a.setRawData((const char *)buf, size);
    b.setBuffer(a);

    if (!b.open(IO_ReadOnly)) {
	std::cerr << tr("Could not open buffer for reading, aborting.\n") << std::endl;
	return false;
    }

    t.setDevice(&b);
    doParse(t, type, true);
    b.close();
    a.resetRawData((const char *)buf, size);
    return true;
}

bool RadioPlayList::parse(const QString &filename, Type type, bool reset)
{
    QString file = filename;

    if (filename.left(7).lower() == "file://")
	file = filename.right(filename.length()-7);

    QFile f(file);
    QTextStream t;

    if (!f.open(IO_ReadOnly)) {
	std::cerr << filename.data() << ": " << tr("could not open file for reading.") << std::endl;
	return false;
    }

    t.setDevice(&f);
    doParse(t, type, reset);
    f.close();
    return true;
}

void RadioPlayList::append(const QString &url, const QString title)
{
    for (unsigned n = 0; n < playList.count(); n++) {
	RadioPlayListItem *p = playList.at(n);

	if (url == p->url())
	    return;
    }

    playList.append(new RadioPlayListItem(url,
		title == QString::null ? QString(tr("Unknown Track")) : title));
}

void RadioPlayList::append(RadioPlayListItem *p)
{
    for (unsigned n = 0; n < playList.count(); n++) {
	RadioPlayListItem *i = playList.at(n);

	if (p->url() == i->url())
	    return;
    }

    playList.append(p);
}

bool RadioPlayList::parseUrl(const QString &str, bool reset)
{
    QString title = QString::null;
    QString url = str;

    if (str.left(7).lower() != "http://")
	if (str.left(8).lower() != "https://")
	    if (str.left(6).lower() != "ftp://")
		if (str.left(7).lower() != "ftps://") {
		    if (str.right(4).lower() == ".pls")
			return parse(str, PlayListPLS, reset);

		    if (str.right(4).lower() == ".m3u")
			return parse(str, PlayListM3U, reset);

		    url = str;

		    if (str.left(7).lower() != "file://") {
			QFileInfo f(str);
			url = f.absFilePath();

			if (f.isDir())
			    return false;

			url.prepend("file://");
		    }

		    int n = url.findRev(QChar('/'));

		    if (n != -1)
			title = url.right(url.length()-n-1);
		    else
			title = url;
		}

    if (title == QString::null)
	title = QString(url);

    append(url, title);
    return true;
}

int RadioPlayList::writeToFile(const QString &filename)
{
    if (filename.isEmpty())
	return EINVAL;

    if (!playList.count()) {
	unlink(filename.data());
	return 0;
    }

    FILE *fp = fopen(filename, "w+");

    if (!fp)
	return errno;
    
    RadioPlayListItem *i;
    int count = 1;
    fprintf(fp, "[playlist]\n");
    fprintf(fp, "numberofentries=%i\n", playList.count());

    for (i = playList.first(); i; i = playList.next(), count++) {
	fprintf(fp, "File%i=%s\n", count, i->url().data());

	if (i->streamTitle())
	    fprintf(fp, "Title%i=%s\n", count, i->streamTitle().data());
	else if (i->url() != i->title())
	    fprintf(fp, "Title%i=%s\n", count, i->title().data());

	fprintf(fp, "Length%i=-1\n", count);
    }

    fprintf(fp, "Version=2\n");
    fclose(fp);
    return 0;
}

bool RadioPlayList::setSortType(int type, bool reverse)
{
    if (type >= SortMax || type < 0)
	return false;

    playList.setSortType(type, reverse);
    return true;
}

int RadioPlayList::sortType(bool *r)
{
    return playList.sortType(r);
}

int RadioPlayListList::compareItems(QCollection::Item arg1,
	QCollection::Item arg2)
{
    RadioPlayListItem *a = (RadioPlayListItem *) arg1;
    RadioPlayListItem *b = (RadioPlayListItem *) arg2;
    int n = 0;

    switch (_sortType) {
	case RadioPlayList::SortGenre:
	    n = QString::compare(a->genre().lower(), b->genre().lower());
	    break;
	case RadioPlayList::SortTitle:
	    n = QString::compare(a->title().lower(), b->title().lower());
	    break;
	case RadioPlayList::SortStreamTitle:
	    n = QString::compare(a->streamTitle().lower(), b->streamTitle().lower());
	    break;
	case RadioPlayList::SortUrl:
	    n = QString::compare(a->url().lower(), b->url().lower());
	    break;
	case RadioPlayList::SortId:
	    n = a->id() > b->id() ? 1 : -1;
	    break;
	case RadioPlayList::SortBitRate:
	    if (a->bitRate() == b->bitRate())
		n = 0;
	    else
		n = a->bitRate() > b->bitRate() ? 1 : -1;
	    break;
    }

    return revSort ? -n : n;
}
