/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __EZXRADIOCONFIG_H__
#define __EZXRADIOCONFIG_H__

#include "EZXRadio.h"

#define MAX_BINDINGS	3

class EZXRadio;
class EZXRadioConfig : public QObject
{
    Q_OBJECT

    public:
	EZXRadioConfig(EZXRadio *);
	bool readFile(const QString &filename);
	bool writeFile(const QString &filename);
	QStringList parseProxyPorts(const QString &);

	enum EZXRadioKey {
	    KeyVolumeUp, KeyVolumeDown, KeyMute,
	    KeyPlaylistNext, KeyPlaylistPrev, KeyPlaylistSave,
	    KeyPlaylistDelete, KeyPlaylistRemove, KeyPlaylistAdd,
	    KeyPlaylistTag,
	    KeyPlaystop, KeyBrowse, KeyPause, KeyRecall,
	    KeySort, KeySortReverse, KeyCopyTag,
	    KeyShuffle, KeyRepeat, KeyScan, KeyRecord, KeyQuit, KeyScanNext,
	    KeyOpenfolder, KeyOpenfile,
	    KeyPrefsIface, KeyPrefsNetwork,
	    KeyLeft, KeyRight, KeyUp, KeyDown, KeySelect, KeyModifier,
	    KeyPlaylistNextJump, KeyPlaylistPrevJump,
	    KeyOpenFavorites, KeyAddFavorite, KeyMenu,
#ifdef WITH_TTS
	    KeyTTS,
#endif
	    MaxKeys
	};

	int hardKey(EZXRadioKey);
	bool matchBinding(EZXRadioKey, int key, bool mod = false);
	bool hasBinding(int, bool mod = false);

    private:
	QString filename;
	bool strToBool(QString &str, bool, unsigned);
	void parseHardKey(EZXRadioKey, QString &);
	void writeHardKey(QTextStream &, int, QString);

	struct key_s {
	    int key;
	    bool mod;
	};

	struct hardKeyConfig_s {
	    struct key_s key[MAX_BINDINGS];
	    int keyN;
	} hardKeyConfig[MaxKeys];

	EZXRadio *parent;
};

#endif
