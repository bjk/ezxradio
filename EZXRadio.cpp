/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <iostream>
#include <csignal>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <ZApplication.h>
#include <ZMainWidget.h>
#include <ZMessageBox.h>
#include <ZGlobal.h>
#include <ZScrollView.h>
#include <ZPushButton.h>
#include <qfile.h>
#include <qbuffer.h>
#include <ezxres.h>
#include <qvbox.h>
#include <qspinbox.h>
#include <qcheckbox.h>
#include <qdir.h>
#include <qclipboard.h>
#include "EZXRadio.h"
#include "dsp.h"

void catchsig(int sig)
{
    switch (sig) {
	case SIGTERM:
	    qApp->quit();
	    break;
	default:
	    break;
    }
}

EZXRadio::EZXRadio(int argc, char **argv) : ZMainWidget(true, NULL, NULL, 0)
{
    int opt;
    int isizeArg = -1, osizeArg = -1;
    bool autoPlayArg = false, recordArg = false;
    QString proxyArg = QString::null;
    bool recurse = false;
    int verbose = 0;
    startup = true;
    streamState = 0;
    headerFontSize = 18;
    headerColor = "#f37e34";
    tagFontSize = 22;
    tagColor = "#b4e0f6";
    formatFontSize = 17;
    formatColor = "#eefd8b";
    statusFontSize = 17;
    statusColor = "#a0b2b3";
    defaultFontSize = 16;
    defaultColor = "#ffffff";
    defaultVolume = 45;
    soundVolume = defaultVolume;
    browseMode = false;
    recall = 0;
    cyclePlayList = true;
    sortType = 0;
    reverseSort = false;
    keyMod = false;
    bufferEmptySound = QString::null;
    bufferEmptySoundLoop = false;
    bufferEmptySoundInterval = 0;
    streamErrorSound = QString::null;
    networkProfile = QString::null;
    sessionLink = QString::null;
    scanSound = QString::null;
    marqueeInterval = 150;
    marqueeSleepInterval = 1500;
    playListJump = 5;
    repeatSound = repeatAllSound = QString::null;
    connectingSound = QString::null;
    connectingSoundLoop = false;
    connectingSoundInterval = 0;
    connectingSoundId = 0;
    openUrlLast = QString::null;
    searchQuery = QString::null;
    searchShoutCastQuery = QString::null;
    wantRefreshGenres = false;
    lineEditDialog = NULL;
    currentGenre = NULL;
    shoutCastCommand = -1;
    shoutCastRetries = 0;
    shoutCastMaxItems = 0;
    playListSingle = true;
    streamRetries = 0;
    saveDirectory = "/mmc/mmca1";
    retrySound = QString::null;
    favorites = true;
#ifdef WITH_TTS
    useTTS = false;
#endif

    rcFile = QString(QDir::homeDirPath() + "/.ezxradio/config");
    favoritesFile = QString(QDir::homeDirPath() + "/.ezxradio/favorites.pls");
    std::signal(SIGTERM, catchsig);

    while ((opt = getopt(argc, argv, "Radi:o:hp:f:vr")) != EOF) {
	switch (opt) {
	    // For file associations (EZX), this gets passed for some reason.
	    case 'd':
		break;
	    case 'r':
		recurse = true;
		break;
	    case 'R':
		recordArg = true;
		break;
	    case 'a':
		autoPlayArg = true;
		break;
	    case 'v':
		verbose++;
		break;
	    case 'c':
		rcFile = QString(optarg);
		break;
	    case 'p':
		proxyArg = QString(optarg);
		break;
	    case 'i':
		isizeArg = strtol(optarg, NULL, 10) * 1024;
		break;
	    case 'o':
		osizeArg = strtol(optarg, NULL, 10) * 1024;
		break;
	    case 'h':
	    default:
		std::cerr << tr("Usage: [-h] [-R] [-i <bufsize>] [-o <bufsize>] [-p <proxy[:port]>]") << std::endl;
		qApp->quit();
		break;
	}
    }

    static RadioStreamDsp streamDsp(openDsp, closeDsp, setDspVolume,
	    setDspFormat, writeDsp, syncDsp, setDspFormats, dspVolume, this);
    stream = new RadioStream(this, &streamDsp, verbose);
    connect(stream, SIGNAL(streamStart()), this, SLOT(slotStreamStart()));
    connect(stream, SIGNAL(streamStop()), this, SLOT(slotStreamStop()));
    connect(stream, SIGNAL(streamPaused(bool)), this, SLOT(slotStreamPaused(bool)));
    connect(stream, SIGNAL(streamBufferEmpty(bool)), this,
	    SLOT(slotStreamBufferEmpty(bool)));

    sc = new ShoutCast(this, stream);
    connect(sc, SIGNAL(shoutCastGenres(QList<ShoutCastGenre>, ShoutCast::ShoutCastState)), this, SLOT(slotShoutCastGenres(QList<ShoutCastGenre>, ShoutCast::ShoutCastState)));
    connect(sc, SIGNAL(shoutCastFetch(ShoutCastGenre *, ShoutCast::ShoutCastState)), this, SLOT(slotShoutCastFetch(ShoutCastGenre *, ShoutCast::ShoutCastState)));
    sc->parseGenreIndexFile(QString(QDir::homeDirPath() + "/.ezxradio/genre.cache"));
    recordDirectory = QString("/mmc/mmca1");
    recordOneFile = false;
    enableMarquee = true;
    powerSaving = false;
    stream->setDecodeWait(3);
    scanInterval = 15;
    autoPlay = false;
    config = new EZXRadioConfig(this);
    shoutCastSearchesLB = new EZXRadioListBox();
    config->readFile(rcFile);
    shoutCastSearchesLB->setKeys(config->hardKey(EZXRadioConfig::KeyUp), config->hardKey(EZXRadioConfig::KeyDown), config->hardKey(EZXRadioConfig::KeySelect));

    if (isizeArg != -1)
	stream->setInputBufferSize(isizeArg);

    if (osizeArg != -1)
	stream->setOutputBufferSize(osizeArg);

    if (proxyArg != QString::null)
	stream->setProxy(proxyArg);

    if (autoPlayArg)
	autoPlay = true;

    stream->setVolume(defaultVolume);
    stream->setSoundVolume(soundVolume);
    connectingSoundTimer = new QTimer(this);
    connect(connectingSoundTimer, SIGNAL(timeout()), this,
	    SLOT(slotConnectingSoundTimer()));
#if 0
    titlebar_l = new QLabel("EZX Radio", this);
    titlebar_l->setScaledContents(TRUE);
    setTitleBarWidget(titlebar_l);
    titlebar_l->show();
#endif
    
    setWidgetFontSize(this, defaultFontSize);
    setWidgetColor(this, defaultColor);
    ZScrollView *sv = new ZScrollView(this, "sv");
    sv->setResizePolicy(QScrollView::AutoOneFit);
    setContentWidget(sv);
    //sv->viewport()->setFocus();
    QBoxLayout *main_bl = new QBoxLayout(sv->viewport(), QBoxLayout::TopToBottom);
    QBoxLayout *hbox1 = new QBoxLayout(main_bl, QBoxLayout::TopToBottom);
    playing_l = new QLabel(tr("-[ Playing ]-"), this);
    playing_l->setAlignment(AlignHCenter);
    setWidgetFontSize(playing_l, headerFontSize);
    setWidgetColor(playing_l, headerColor);
    hbox1->addWidget(playing_l);

    title_l = new EZXRadioMarquee(tr("unavailable"), this, 0);
    title_l->setAlignment(AlignHCenter);
    connect(title_l, SIGNAL(MarqueeMousePressEvent(EZXRadioMarquee *)), this, SLOT(slotMarqueeMousePressEvent(EZXRadioMarquee *)));
    connect(title_l, SIGNAL(MarqueeMouseReleaseEvent(EZXRadioMarquee *)), this, SLOT(slotMarqueeMouseReleaseEvent(EZXRadioMarquee *)));
    connect(title_l, SIGNAL(MarqueeMouseDoubleClickEvent(EZXRadioMarquee *)), this, SLOT(slotMarqueeMouseDoubleClickEvent(EZXRadioMarquee *)));
    setWidgetFontSize(title_l, tagFontSize);
    setWidgetColor(title_l, tagColor);
    hbox1->addWidget(title_l, 1, AlignTop);

    QBoxLayout *hbox2 = new QBoxLayout(main_bl, QBoxLayout::TopToBottom);
    format_l = new QLabel(tr("-[ Stream Info ]-"), this);
    setWidgetFontSize(format_l, headerFontSize);
    setWidgetColor(format_l, headerColor);
    hbox2->addWidget(format_l, 0, AlignTop|AlignCenter);

    info_l = new EZXRadioMarquee(tr("unavailable"), this);
    connect(info_l, SIGNAL(MarqueeMousePressEvent(EZXRadioMarquee *)), this, SLOT(slotMarqueeMousePressEvent(EZXRadioMarquee *)));
    connect(info_l, SIGNAL(MarqueeMouseReleaseEvent(EZXRadioMarquee *)), this, SLOT(slotMarqueeMouseReleaseEvent(EZXRadioMarquee *)));
    setWidgetFontSize(info_l, formatFontSize);
    setWidgetColor(info_l, formatColor);
    hbox2->addWidget(info_l, 0, AlignTop);
    info_l->setAlignment(AlignHCenter);
    info2_l = new QLabel(" ", this);
    setWidgetFontSize(info2_l, formatFontSize);
    setWidgetColor(info2_l, formatColor);
    hbox2->addWidget(info2_l, 1, AlignTop);
    info2_l->setAlignment(AlignHCenter);

    QBoxLayout *hbox3 = new QBoxLayout(main_bl, QBoxLayout::TopToBottom);
    statusTitle_l = new QLabel(tr("-[ Network Status ]-"), this);
    setWidgetFontSize(statusTitle_l, headerFontSize);
    setWidgetColor(statusTitle_l, headerColor);
    hbox3->addWidget(statusTitle_l, 0, AlignTop|AlignCenter);
    status_l = new EZXRadioMarquee(tr("not connected"), this);
    setWidgetFontSize(status_l, statusFontSize);
    setWidgetColor(status_l, statusColor);
    updateStatusLabel();
    connect(status_l, SIGNAL(MarqueeMousePressEvent(EZXRadioMarquee *)), this, SLOT(slotMarqueeMousePressEvent(EZXRadioMarquee *)));
    hbox3->addWidget(status_l, 1, AlignTop);
    status_l->setAlignment(AlignHCenter);

    pausedL = new EZXRadioBlink(this, "", 500, false);
    connect(pausedL, SIGNAL(blinkChanged(bool)), this, SLOT(slotPaused(bool)));
    hbox3->addWidget(pausedL, 1, AlignTop);
    pausedL->setAlignment(AlignHCenter);
    setWidgetFontSize(pausedL, formatFontSize);
    setWidgetColor(pausedL, formatColor);

    QBoxLayout *hbox4 = new QBoxLayout(main_bl, QBoxLayout::LeftToRight);
    repeatB = new EZXRadioRepeat(sv->viewport(), EZXRadioRepeat::None);
    hbox4->addWidget(repeatB, 1, AlignLeft);

    shuffleB = new EZXRadioShuffle(sv->viewport());
    connect(shuffleB, SIGNAL(shuffleChanged(bool)), this, SLOT(slotShuffle(bool)));
    hbox4->addWidget(shuffleB, 1, AlignLeft);

    scanB = new EZXRadioBlink(sv->viewport(), "scan.xpm");
    connect(scanB, SIGNAL(blinkChanged(bool)), this, SLOT(slotScan(bool)));
    hbox4->addWidget(scanB, 1, AlignLeft);

    recordB = new EZXRadioBlink(sv->viewport(), "record.xpm");
    //recordB->setScaledContents(true);
    connect(recordB, SIGNAL(blinkChanged(bool)), this, SLOT(slotRecord(bool)));
    hbox4->addWidget(recordB, 1, AlignLeft);

    shoutCastL = new EZXRadioBlink(sv->viewport(), QString("SC"), 500, false, true);
    connect(shoutCastL, SIGNAL(blinkChanged(bool)), this, SLOT(slotShoutCastBlink(bool)));
    shoutCastL->off();
    hbox4->addWidget(shoutCastL, 1, AlignLeft);

    keyModL = new EZXRadioBlink(sv->viewport(), QString("M"), 500, false);
    connect(keyModL, SIGNAL(blinkChanged(bool)), this, SLOT(slotKeyModBlink(bool)));
    keyModL->off();
    hbox4->addWidget(keyModL, 1, AlignLeft);

    volume_s = new QSlider(0, 100, 5, stream->volume(),
	    QSlider::Horizontal, this, NULL);
    volume_s->setFocusPolicy(QWidget::NoFocus);
    volume_s->hide();
    connect(volume_s, SIGNAL(valueChanged(int)), SLOT(slotAdjustVolume(int)));

    volumeL = new EZXRadioVolume(sv->viewport(), stream->volume());
    volumeL->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
    connect(volumeL, SIGNAL(muteChanged(bool)), this, SLOT(slotMute(bool)));
    hbox4->addWidget(volumeL, 1, AlignRight);
    hbox4->activate();

    cst = new UTIL_CST(this, tr("Play"));
    setCSTWidget(cst);
    cst->show();

    ZPushButton *lb = cst->getLeftBtn();
    connect(lb, SIGNAL(clicked()), this, SLOT(CSTMenu()));
    cstpopup = new QPopupMenu(lb);

    QPopupMenu *openPopup = new QPopupMenu();
    openPopup->insertItem(tr("Open URL"), this, SLOT(slotOpenURL()));
    openPopup->insertItem(tr("Open File"), this, SLOT(slotOpenFile()));
    openPopup->insertItem(tr("Open Folder"), this, SLOT(slotOpenFolder()));
    openPopup->insertItem(tr("About"), this, SLOT(slotAboutDialog()));
    cstpopup->insertItem(tr("File"), openPopup);

    QPopupMenu *favPopup= new QPopupMenu();
    favPopup->insertItem(tr("Open"), this, SLOT(slotOpenFavorites()));
    favPopup->insertItem(tr("Add current"), this, SLOT(slotAppendToFavorites()));
    favPopup->insertItem(tr("Save as favorites"), this, SLOT(slotSaveAsFavorites()));
    cstpopup->insertItem(tr("Favorites"), favPopup);

    QPopupMenu *playListPopup = new QPopupMenu();
    playListPopup->insertItem(tr("Add"), this, SLOT(slotAppendToPlayList()));
    playListPopup->insertItem(tr("Remove current"), this, SLOT(slotRemovePlayListItem()));
    playListPopup->insertItem(tr("Delete current"), this, SLOT(slotDeletePlayListFile()));
    playListPopup->insertItem(tr("Save"), this, SLOT(slotSavePlayList()));
    playListPopup->insertItem(tr("Save as"), this, SLOT(slotSaveAsPlayList()));
    QPopupMenu *searchPopup = new QPopupMenu();
    cstpopup->insertItem(tr("Playlist"), playListPopup);

    searchPopup->insertItem(tr("Title"), this, SLOT(slotSearchTitle()));
    searchPopup->insertItem(tr("Album"), this, SLOT(slotSearchAlbum()));
    searchPopup->insertItem(tr("Artist"), this, SLOT(slotSearchArtist()));
    searchPopup->insertItem(tr("Id"), this, SLOT(slotSearchId()));
    searchPopup->insertItem(tr("Url"), this, SLOT(slotSearchUrl()));
    searchPopup->insertItem(tr("Stream Title"), this, SLOT(slotSearchStreamTitle()));
    playListPopup->insertItem(tr("Search"), searchPopup);

    QPopupMenu *p = new QPopupMenu();
    p->insertItem(tr("Interface"), this, SLOT(slotPrefsInterface()));
    p->insertItem(tr("Network"), this, SLOT(slotPrefsNetwork()));
    p->insertItem(tr("Sounds"), this, SLOT(slotPrefsSound()));
    p->insertItem(tr("Advanced"), this, SLOT(slotPrefsAdvanced()));
    p->insertItem(tr("ShoutCast"), this, SLOT(slotPrefsShoutCast()));
    cstpopup->insertItem(tr("Preferences"), p);

    p = new QPopupMenu();
    p->insertItem(tr("Connect"), this, SLOT(slotLinkConnect()));
    p->insertItem(tr("Disconnect"), this, SLOT(slotLinkDisconnect()));
    p->insertItem(tr("Flush fill buffer"), this, SLOT(slotFlushFillBuffer()));
    p->insertItem(tr("Preferences"), this, SLOT(slotPrefsNetwork()));
    cstpopup->insertItem(tr("Network"), p);

    p = new QPopupMenu();
    p->insertItem(tr("Genres"), this, SLOT(slotGenres()));
    p->insertItem(tr("Refresh this genre"), this, SLOT(slotRefreshGenre()));
    p->insertItem(tr("Refresh index"), this, SLOT(slotRefreshGenres()));
    p->insertItem(tr("Search"), this, SLOT(slotShoutCastSearch()));
    p->insertItem(tr("Search results"), this, SLOT(slotShoutCastSearchResults()));
    shoutCastSearchesCB = new QComboBox(p);
    shoutCastSearchesCB->setSizeLimit(7);
    shoutCastSearchesCB->setListBox(shoutCastSearchesLB);
    shoutCastSearchesLB->insertItem(tr("Recent searches"), 0);
    connect(shoutCastSearchesCB, SIGNAL(activated(int)), this, SLOT(shoutCastSearchSelected(int)));
    p->insertItem(shoutCastSearchesCB);
    p->insertItem(tr("Preferences"), this, SLOT(slotPrefsShoutCast()));
    cstpopup->insertItem(tr("SHOUTcast"), p);

    lb->setPopup(cstpopup);

    ZPushButton *rb = cst->getRightBtn();
    connect(rb, SIGNAL(clicked()), this, SLOT(slotQuit()));

    ZPushButton *mb = cst->getMidBtn();
    connect(mb, SIGNAL(clicked()), this, SLOT(slotStartStopStream(bool)));
    sessionMgr = new ZDataSessionManager(this);
    linkId = -1;
    sessionMgr->init(true);
    connect(sessionMgr, SIGNAL(connected(int, ZLinkInfo &)),
	    SLOT(slotLinkConnected(int, ZLinkInfo &)));
    connect(sessionMgr,
	    SIGNAL(openFailed(int, unsigned short, unsigned short)),
	    SLOT(slotLinkOpenFailed(int, unsigned short, unsigned short)));
    connect(sessionMgr, SIGNAL(broken(int, ZLinkInfo &)),
	    SLOT(slotLinkBroken(int, ZLinkInfo &)));
    prefsIfaceDialog = NULL;
    prefsIfaceCst = NULL;
    prefsNetworkDialog = NULL;
    prefsNetworkCst = NULL;
    prefsSoundDialog = NULL;
    prefsSoundCst = NULL;
    prefsAdvancedDialog = NULL;
    prefsAdvancedCst = NULL;
    prefsShoutCastDialog = NULL;
    prefsShoutCastCst = NULL;
    genreDialog = NULL;
    lastDir = QString::null;
    connect(this, SIGNAL(kbStateChanged(bool)), this, SLOT(slotKbStateChanged(bool)));

    backLightFd = open("/dev/fb0", O_RDONLY);
    pausedInCall = inCall = false;
    QTimer *ct = new QTimer(this, NULL);
    connect(ct, SIGNAL(timeout()), this, SLOT(slotCallHandler()));
    ct->start(200);
    sortTagTimer = new QTimer(this, NULL);
    connect(sortTagTimer, SIGNAL(timeout()), this, SLOT(slotSortTagTimer()));
    keyModTimer = new QTimer(this, NULL);
    connect(keyModTimer, SIGNAL(timeout()), this, SLOT(slotKeyModTimer()));

    /* Now events can be sent since the widgets have been created. */
    RadioPlayList *pl = new RadioPlayList();
    bool haveArg = false;

    for (; optind < argc; optind++) {
	QFileInfo f(argv[optind]);
	haveArg = true;

	if (f.isDir()) {
	    recursiveAdd(pl, QString(argv[optind]), recurse);
	    continue;
	}

	pl->parseUrl(QString(argv[optind]));
    }
   
    connect(qApp, SIGNAL(signalOpenDoc(const QString &)), this,
	    SLOT(slotOpenDoc(const QString &)));
    connect(qApp, SIGNAL(quickQuit()), this, SLOT(slotQuit()));
    connect(qApp, SIGNAL(shutdown()), this, SLOT(slotQuit()));

    if (pl->playList.count())
	doSetPlayList(pl);
    else
	delete pl;

    if (!haveArg && favorites)
	slotOpenFavorites();

    /* Theres a race condition with linkId and doLinkConnect() so try to avoid
     * it with a timer. The timer lets the slotLinkConnected() event from the
     * window server be processed when a link is already available. */
    startup = !autoPlay;

    if (autoPlay)
	QTimer::singleShot(1500, this, SLOT(slotAutoPlayStart()));

    recordB->setActive(recordArg);
    resetMarquee(enableMarquee ? QButton::On : QButton::Off);

    /* To hide pausedL. */
    stream->setPaused(false);
}

void EZXRadio::slotKeyModTimer()
{
    keyMod = false;
    keyModL->setActive(false);
}

void EZXRadio::shoutCastSearchSelected(int n)
{
    if (!n)
	return;

    searchShoutCastQuery = shoutCastSearchesCB->text(n);
    shoutCastSearchesCB->setCurrentItem(0);
    cstpopup->close();
    setCurrentGenre(0, true);
}

void EZXRadio::slotKeyModBlink(bool v)
{
    if (!v)
	keyModL->off();
}

void EZXRadio::slotOpenFavorites()
{
    openURLFinalize(favoritesFile, false);
}

void EZXRadio::slotSaveAsFavorites()
{
    int rc = stream->playList()->writeToFile(favoritesFile);

    if (rc)
	showError(strerror(rc));
}

void EZXRadio::slotAppendToFavorites()
{
    RadioPlayList *pl = new RadioPlayList();

    pl->parse(favoritesFile, RadioPlayList::PlayListPLS);
    RadioPlayListItem *p = stream->current(browseMode)->dup();
    pl->playList.append(p);
    int rc = pl->writeToFile(favoritesFile);
    delete pl;
    
    if (rc)
	showError(strerror(rc));
}

void EZXRadio::slotRefreshGenre()
{
    if (!currentGenre)
	return;

    QList<ShoutCastGenre> list = sc->genres();

    if (currentGenre == list.at(0))
	setCurrentGenre(list.findRef(currentGenre), true, true);
    else
	setCurrentGenre(list.findRef(currentGenre), false, true);
}

void EZXRadio::slotShoutCastBlink(bool v)
{
    (void)v;

    if (!stream->fetches())
	shoutCastL->off();
    else
	shoutCastL->setText(QString("%1").arg(stream->fetches()));
}

void EZXRadio::slotShoutCastGenres(QList<ShoutCastGenre> list,
	ShoutCast::ShoutCastState state)
{
    if (!stream->fetches())
	shoutCastL->off();

    if (state == ShoutCast::Error)
	return;

    QFile f(QString("%1/.ezxradio/genre.cache").arg(QDir::homeDirPath()));

    if (!f.open(IO_WriteOnly)) {
	showError(tr("Could not open file for writing."));
	return;
    }

    QTextStream t;
    t.setDevice(&f);

    for (unsigned i = 0; i < list.count(); i++) {
	ShoutCastGenre *g = list.at(i);

	t << g->name() << "\n";
    };

    f.close();
}

void EZXRadio::slotShoutCastFetch(ShoutCastGenre *g, ShoutCast::ShoutCastState state)
{
    if (!stream->fetches())
	shoutCastL->off();

    if (!g->items.count())
	return;

    QList<ShoutCastGenre> list = sc->genres();
    ShoutCastData *d = (ShoutCastData *)g->data();
    bool multi = g == currentGenre ? false : true;
    d->state = state;

    if (state == ShoutCast::Error) {
	return;
    }
    else if (state == ShoutCast::Cancel) {
	return;
    }
    else if (state == ShoutCast::Done) {
	return;
    }
    else if (state == ShoutCast::Init) {
	RadioPlayList *pl = new RadioPlayList();
	title_l->at(1);

	for (unsigned i = 0; i < g->items.count(); i++) {
	    if (d && d->maxItems && i >= d->maxItems) {
		g->cancel();
		break;
	    }

	    ShoutCastGenreItem *p = g->items.at(i);
	    pl->append(new RadioPlayListItem(p->url(), p->playing(), 0, 0,
			p->genre(), p->title(), p->bitRate()));
	}

	d->playList = pl;

	if (!multi) {
	    QCustomEvent *ev = new QCustomEvent(QEvent::Type(EZXRadioShoutCast));
	    ev->setData(d);
	    qApp->postEvent(this, ev);
	}

	return;
    }
    else if (state == ShoutCast::More) {
	unsigned cur = d->playList->playList.at();

	for (unsigned i = d->playList->playList.count(); i < g->items.count(); i++) {
	    if (d && d->maxItems && i >= d->maxItems) {
		g->cancel();
		break;
	    }

	    ShoutCastGenreItem *p = g->items.at(i);
	    d->playList->playList.append(new RadioPlayListItem(p->url(),
		    p->playing(), 0, 0, p->genre(), p->title(), p->bitRate()));
	}

	d->playList->playList.at(cur);

	if (!multi) {
	    qApp->sendPostedEvents(this, EZXRadioShoutCast);
	    qApp->postEvent(this, EVENT(StreamPlaying));
	}
    }
}

void EZXRadio::slotRefreshGenres()
{
    wantRefreshGenres = true;

    if (linkId == -1) {
	if (doLinkConnect())
	    shoutCastCommand = ShoutCastGenresCmd;

	return;
    }

    shoutCastConnectFinalize(ShoutCastGenresCmd);
}

void EZXRadio::slotGenres()
{
    ZPushButton *cancelBtn;
    ZPushButton *okBtn;
    QHBox *hb;
    QVBox *vb;
    QVBox *cw;
    static GenreBrowser *g;

    if (genreDialog) {
	if (wantRefreshGenres) {
	    g->setGenreList(sc->genres());
	    wantRefreshGenres = false;
	}

	genreDialog->show();
	goto done;
    }

    genreDialog = new UTIL_Dialog(UTIL_Dialog::DTLargeSize, true, this,
	    0,1,0);
    genreDialog->setHasTitle(true);
    genreDialog->setDlgTitle(tr("SHOUTcast genres:"));
    cw = new QVBox(genreDialog);
    genreDialog->setDlgContent(cw);
    vb = (QVBox*)genreDialog->getDlgContent();

    g = new GenreBrowser(vb, config->hardKey(EZXRadioConfig::KeyUp), config->hardKey(EZXRadioConfig::KeyDown), config->hardKey(EZXRadioConfig::KeySelect));
    connect(sc, SIGNAL(shoutCastGenres(QList<ShoutCastGenre>)), g,
	    SLOT(slotSetGenres(QList<ShoutCastGenre>)));

    g->setGenreList(sc->genres());
    hb = new QHBox(genreDialog);
    genreDialog->setDlgCst(hb);

    okBtn = new ZPushButton(hb, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    connect( okBtn, SIGNAL( clicked() ), genreDialog, SLOT(accept()));

    cancelBtn = new ZPushButton(hb, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    connect( cancelBtn, SIGNAL( clicked() ), genreDialog, SLOT(reject()));
    genreDialog->show();

done:
    int item = -1;

    if (genreDialog->exec() == QDialog::Accepted)
	item = g->genre();

    genreDialog->hide();

    if (item == -1)
	return;

    setCurrentGenre(item);
}

void EZXRadio::slotSortTagTimer()
{
    qApp->postEvent(this, EVENT(StreamPlaying));
}

void EZXRadio::recursiveAdd(RadioPlayList *pl, const QString &dir, bool recurse)
{
    QDir d(dir);

    for (unsigned i = 0; i < d.count(); i++) {
	if (d[i] ==  "." || d[i] == "..")
	    continue;

	QString fn;

	if (dir.right(1) != "/")
	    fn = dir + "/" + d[i];
	else
	    fn = dir + d[i];

	QFileInfo fi(fn);

	if (fi.isDir() && recurse) {
	    recursiveAdd(pl, fn, recurse);
	    continue;
	}

	pl->parseUrl(fn);
    }
}

void EZXRadio::slotConnectingSoundTimer()
{
#ifdef WITH_TTS
    if (useTTS)
	connectingSoundId = stream->playTTS(tr("Connecting"), true,
		connectingSoundLoop, connectingSoundInterval);
    else
#endif
    connectingSoundId = stream->playSound(connectingSound, true,
	    connectingSoundLoop, connectingSoundInterval);
}

void EZXRadio::slotAutoPlayStart()
{
    slotStartStopStream();
}

void EZXRadio::slotFlushFillBuffer()
{
    stream->flushFillBuffer();
}

void EZXRadio::slotLinkDisconnect()
{
    if (linkId == -1)
	return;

    if (stream->isPlaying() && !stream->isLocal()) {
#ifdef WITH_TTS
	if (useTTS)
	    stream->playTTS(tr("Stream Error"));
	else
#endif
	stream->playSound(streamErrorSound);
	stream->stop();
    }

    sessionMgr->closeLink(linkId);
    linkId = -1;
}

void EZXRadio::slotLinkConnect()
{
    if (linkId != -1)
	return;

    doLinkConnect(true);
}

void EZXRadio::slotStreamBufferEmpty(bool b)
{
    static unsigned id;

    if (b) {
	if (!id || !stream->hasSoundId(id)) {
#ifdef WITH_TTS
	    if (useTTS)
		id = stream->playTTS(tr("Buffering"), true,
			bufferEmptySoundLoop, bufferEmptySoundInterval);
	    else
#endif
	    id = stream->playSound(bufferEmptySound, true,
		    bufferEmptySoundLoop, bufferEmptySoundInterval);
	}
    }
    else {
	if (id && id == stream->soundId())
	    stream->stopSound();
    }

    updateStatusLabel();
}

void EZXRadio::slotPaused(bool b)
{
    if (!b) {
	pausedL->hide();
	return;
    }

    pausedL->show();
}

void EZXRadio::slotStreamPaused(bool b)
{
    pausedL->setText(b && inCall ?
	    tr("(paused - in call)") : b ? tr("(paused)") : "");
    pausedL->setActive(b);
}

void EZXRadio::slotOpenDoc(const QString &doc)
{
    if (openURLFinalize("file://" + doc, false))
	startStream();
}

void EZXRadio::setWidgetColor(QWidget *w, QColor c)
{
    QPalette p = w->palette();
    p.setColor(QPalette::Normal, QColorGroup::Foreground, c);
    p.setColor(QPalette::Active, QColorGroup::Foreground, c);
    p.setColor(QPalette::Inactive, QColorGroup::Foreground, c);
    w->setPalette(p);
}

void EZXRadio::setWidgetFontSize(QWidget *w, int size)
{
    QFont f = w->font();
    f.setPointSize(size);
    w->setFont(f);
}

void EZXRadio::slotStreamStart()
{
}

void EZXRadio::slotStreamStop()
{
    connectingSoundTimer->stop();

    if (connectingSoundId && connectingSoundId == stream->soundId())
	stream->stopSound();
}

void EZXRadio::slotScan(bool v)
{
    stream->setScanEnabled(v, scanInterval);
}

void EZXRadio::slotShuffle(bool v)
{
    stream->resetPlayed();

    if (v) {
	doPlayListNext(false);
	resetPlayListItemData();
    }
}

void EZXRadio::slotSaveTextChanged()
{
    if (!saveFilenameLE->text().length())
	saveConfirmB->setEnabled(false);
    else
	saveConfirmB->setEnabled(true);
}

void EZXRadio::slotSavePlayList()
{
    bool canceled;
    QString f = openFileCommon(tr("Save to *.pls:"), "*.pls",
	    FileBrowser::FilesOnly, &canceled);

    if (canceled || f.isEmpty())
	return;

    int n = stream->savePlayList(f);

    if (n)
	showError(strerror(n));
}

void EZXRadio::slotSaveAsPlayList()
{
    UTIL_Dialog *d = new UTIL_Dialog(UTIL_Dialog::DTLargeSize,true, this, 0,1,0);
    d->setHasTitle(true);
    d->setDlgTitle(tr("Save playlist as:"));
    QFrame *f = new QFrame(d);
    d->setDlgContent(f);
    f = (QFrame *)d->getDlgContent();
    QBoxLayout *bl = new QBoxLayout(f, QBoxLayout::TopToBottom, 0, 5);
    QBoxLayout *hbl = new QBoxLayout(bl, QBoxLayout::LeftToRight);

    QLabel *l = new QLabel(tr("Name:"), f);
    hbl->addWidget(l);
    saveFilenameLE = new ZMultiLineEdit(f, true, 1);
    saveFilenameLE->setMaxLines(1);
    hbl->addWidget(saveFilenameLE);
    connect(saveFilenameLE, SIGNAL(textChanged()), this, SLOT(slotSaveTextChanged()));

    hbl = new QBoxLayout(bl, QBoxLayout::LeftToRight);
    l = new QLabel(tr("Save in:"), f);
    hbl->addWidget(l, 0, Qt::AlignLeft|Qt::AlignTop);
    QFileInfo fi(saveDirectory);
    saveDirectoryL = new QLabel(fi.fileName(), f);
    hbl->addWidget(saveDirectoryL, 0, Qt::AlignTop);
    ZPushButton *b = new ZPushButton("FMMS_Personal_Folder_S", f);
    b->setMaximumSize(24, 24);
    hbl->addWidget(b, 0, Qt::AlignRight|Qt::AlignTop);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectSaveDirectory()));

    QHBox *hb = new QHBox(d);
    d->setDlgCst(hb);
    saveConfirmB = new ZPushButton(hb, 0, -1, -1);
    saveConfirmB->setText(tr("OK"));	
    connect(saveConfirmB, SIGNAL(clicked()), d, SLOT(accept()));
    saveConfirmB->setEnabled(false);
    ZPushButton *cancel = new ZPushButton(hb, 0, -1, -1);
    cancel->setText(tr("Cancel"));	
    connect(cancel, SIGNAL(clicked()), d, SLOT(reject()));
    d->show();

    if (d->exec() == QDialog::Accepted) {
	QString s = saveDirectory + "/" + saveFilenameLE->text();
       
	if (s.find(".pls") == -1)
	    s += ".pls";

	int n = stream->savePlayList(s);

	if (n)
	    showError(strerror(n));
    }

    delete d;	
}

void EZXRadio::slotSelectSaveDirectory()
{
    bool canceled;
    QString s = openFileCommon(tr("Select folder:"), "*",
	    FileBrowser::DirsOnly, &canceled);

    if (canceled)
	return;

    saveDirectory = s;
    QFileInfo fi(saveDirectory);
    saveDirectoryL->setText(fi.fileName());
}

void EZXRadio::slotAppendToPlayList()
{
    bool canceled;
    QString f = openFileCommon(tr("Add folder or file:"), "*",
	    FileBrowser::All, &canceled);

    if (canceled || f.isEmpty())
	return;

    if (QFileInfo(f).isDir()) {
	QDir d = QDir(f);

	for (unsigned i = 0; i < d.count(); i++) {
	    if (d[i] == "." || d[i] == "..")
		continue;

	    QString fn = QString(f + "/" + d[i]);

	    if (QFileInfo(fn).isDir())
		continue;

	    openURLFinalize(QString("file://" + fn), true);
	}
    }
    else
	openURLFinalize(QString("file://" + f), true);

    qApp->postEvent(this, EVENT(StreamPlaying));
}

void EZXRadio::slotDeletePlayListFile()
{
    if (!stream->isLocal(browseMode)) {
	slotRemovePlayListItem();
	return;
    }

    RadioPlayListItem *p = stream->current(browseMode);

    if (!p)
	return;

    RES_ICON_Reader r;
    ZMessageBox *m = new ZMessageBox(this, 
	    r.getIcon("Dialog_Empty_Trash.gif",false),
	    tr("Do you really want to delete this file?"), tr("OK"), tr("Cancel"), NULL);
    int rc = m->exec();

    delete m;

    if (!rc) {
	QString f = p->url();
	slotRemovePlayListItem();
	unlink(f.right(f.length()-7).data());
    }
}

void EZXRadio::slotRemovePlayListItem()
{
    stream->removeCurrentPlayListEntry(browseMode);
}

void EZXRadio::slotCallHandler()
{
    bool b;

#if 0
    std::cerr << UTIL_GetPhoneInCall() << " "
	<< UTIL_GetIncomingCallStatus() << " "
	<< UTIL_GetPhoneCallIconIndex() << " "
	<< UTIL_GetPhoneApplicationStatus() << " "
	<< UTIL_GetPhoneSystemStatus() << std::endl;
#endif

    /* The marquee does eat CPU. */
    if (backLightFd != -1) {
	int n = 0;

	ioctl(backLightFd, FBIOGETBKLIGHT, &n);

	if (!powerSaving && n == BKLIGHT_OFF) {
	    powerSaving = true;
	    resetMarquee(QButton::Off);
	}
	else if (powerSaving && n == BKLIGHT_ON && hasFocus()) {
	    powerSaving = false;

	    if (enableMarquee)
		resetMarquee(QButton::On);

	    if (streamState == StreamConnected)
		qApp->postEvent(this, EVENT(StreamConnected));
	}
	else if (!powerSaving)
	    powerSaving = !hasFocus();
    }
    else
	powerSaving = !hasFocus();

    b = UTIL_GetIncomingCallStatus() || UTIL_GetPhoneInCall();

    if (b && !inCall) {
	inCall = true;
	pausedInCall = stream->isPaused();
	stream->setPaused(true);
    }
    else if (!b && inCall) {
	inCall = false;
	stream->setPaused(pausedInCall);
    }
}

void EZXRadio::slotMute(bool m)
{
    if (m)
	stream->setVolume(0);
    else
	stream->setVolume(volume_s->value());
}

void EZXRadio::slotQuit()
{
    stream->stop();
    delete stream;
    qApp->quit();
}

void EZXRadio::slotRecordOneFile(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    recordOneFile = state == QButton::Off ? false : true;
}

void EZXRadio::slotCyclePlayList(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    cyclePlayList = state == QButton::On;
}

void EZXRadio::slotAutoPlay(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    autoPlay= state == QButton::On;
}

void EZXRadio::focusOutEvent(QFocusEvent *ev)
{
    (void)ev;
    resetMarquee(QButton::Off);
}

void EZXRadio::focusInEvent(QFocusEvent *ev)
{
    (void)ev;

    if (enableMarquee)
	resetMarquee(QButton::On);
}

void EZXRadio::slotRecord(bool state)
{
    (void)state;

    int n = -1;
    time_t t = time(NULL);

    stream->stopRecord();

    if (!stream->current())
	return;

    if (stream->isLocal() || !recordB->isActive()) {
	recordB->stop();
	return;
    }

    if (!recordOneFile || stream->recordFilename() == QString::null)
	n = stream->startRecord(QString(recordDirectory + "/" + "%1" + "-record.mp3").arg(t));
    else if (recordOneFile)
	n = stream->startRecord(stream->recordFilename());

    if (n)
	std::cerr << "recordStart failed: " << strerror(n) << std::endl;

    qApp->postEvent(this, EVENT(StreamPlaying));
}

void EZXRadio::slotLinkConnected(int id, ZLinkInfo &info)
{
    std::cerr << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl;
    linkId = id;
    (void)info;
    networkProfile = sessionLink ? sessionLink : networkProfile;

    updateStatusLabel();

    if (startup)
	return;

    if (shoutCastCommand != -1)
	shoutCastConnectFinalize();
    else if (sessionLink || autoPlay)
	startStream();

    if (!stream->isLocal())
	qApp->postEvent(this, EVENT(StreamConnecting));
}

void EZXRadio::slotLinkOpenFailed(int id, unsigned short a, unsigned short b)
{
    std::cerr << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl;
    (void)id;
    (void)a;
    (void)b;

    linkId = -1;
    shoutCastCommand = -1;
    qApp->postEvent(this, EVENT(StreamInit));
}

void EZXRadio::slotLinkBroken(int id, ZLinkInfo &info)
{
    std::cerr << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl;
    (void)id;
    (void)info;

    if (!stream->isLocal() && stream->isPlaying()) {
#ifdef WITH_TTS
	if (useTTS)
	    stream->playTTS(tr("Stream Error"));
	else
#endif
	stream->playSound(streamErrorSound);
	stream->stop();
    }

    linkId = -1;
    updateStatusLabel();
}

void EZXRadio::cyclePlayListTag()
{
    title_l->next();
}

void EZXRadio::keyPressEvent(QKeyEvent *k)
{
    int i = 0;

    std::cerr << __FUNCTION__ << ": " << std::dec << k->key() << " 0x" << std::hex << k->key() << std::endl;

    if (config->matchBinding(EZXRadioConfig::KeyModifier, k->key(), keyMod)) {
	keyModL->setActive(true);
	keyMod = !keyMod;
	keyModTimer->start(3000, true);
	return;
    }

    if (stream->isPlaying() && scanB->isActive() && !browseMode &&
	    config->hasBinding(k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyPause, k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyScanNext, k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyBrowse, k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyVolumeUp, k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyVolumeDown, k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyAddFavorite, k->key(), keyMod) &&
	    !config->matchBinding(EZXRadioConfig::KeyMute, k->key(), keyMod)) {
	scanB->setActive(false);
	goto done;
    }

    if (config->matchBinding(EZXRadioConfig::KeyBrowse, k->key(), keyMod)) {
	browseMode = !browseMode;
	qApp->postEvent(this, EVENT(StreamPlaying));
	qApp->postEvent(this, EVENT(StreamFormat));
	qApp->postEvent(this, EVENT(StreamConnected));
	stream->scanFormat(stream->current(browseMode), browseMode);
	goto done;
    }

    if (browseMode) {
	for (i = 0; i < EZXRadioConfig::MaxKeys; i++) {
	    if (config->matchBinding(EZXRadioConfig::KeyPlaylistSave, k->key(), keyMod)) {
		slotSavePlayList();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyPlaylistDelete, k->key(), keyMod)) {
		slotDeletePlayListFile();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyPlaylistRemove, k->key(), keyMod)) {
		slotRemovePlayListItem();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyPlaylistAdd, k->key(), keyMod)) {
		slotAppendToPlayList();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyOpenfile, k->key(), keyMod)) {
		slotOpenFile();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyOpenfolder, k->key(), keyMod)) {
		slotOpenFolder();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyPlaylistTag, k->key(), keyMod)) {
		cyclePlayListTag();
		sortTagTimer->start(2000, true);
		qApp->postEvent(this, EVENT(EZXRadioTag));
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyOpenFavorites, k->key(), keyMod)) {
		slotOpenFavorites();
		goto done;
	    }
	    else if (config->matchBinding(EZXRadioConfig::KeyAddFavorite, k->key(), keyMod)) {
		slotAppendToFavorites();
		goto done;
	    }
	}
    }

    for (i = 0; i < EZXRadioConfig::MaxKeys; i++) {
	if (config->matchBinding(EZXRadioConfig::KeyMute, k->key(), keyMod)) {
	    volumeL->setMuted(!volumeL->isMuted());
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyOpenFavorites, k->key(), keyMod)) {
	    slotOpenFavorites();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyAddFavorite, k->key(), keyMod)) {
	    slotAppendToFavorites();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyVolumeUp, k->key(), keyMod)) {
	    volume_s->addStep();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeySort, k->key(), keyMod)) {
	    if (!stream->sortPlayList(++sortType)) {
		sortType = 0;
		stream->sortPlayList(sortType);
	    }

	    sortTagTimer->start(2000, true);
	    qApp->postEvent(this, EVENT(EZXRadioSort));
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeySortReverse, k->key(), keyMod)) {
	    reverseSort = !reverseSort;

	    if (!stream->sortPlayList(sortType, reverseSort))
		stream->sortPlayList(--sortType, reverseSort);

	    sortTagTimer->start(2000, true);
	    qApp->postEvent(this, EVENT(EZXRadioSort));
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyVolumeDown, k->key(), keyMod)) {
	    volume_s->subtractStep();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPlaylistNext, k->key(), keyMod)) {
	    doPlayListNext(false);
	    resetPlayListItemData();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPlaylistNextJump, k->key(), keyMod)) {
	    if (!stream->playListAt(stream->playListAt(browseMode)+playListJump,
		    browseMode))
		stream->playListAt(stream->playListTotal()-1, browseMode);
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPlaylistPrev, k->key(), keyMod)) {
	    doPlayListPrev();
	    resetPlayListItemData();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPlaylistPrevJump, k->key(), keyMod)) {
	    if (!stream->playListAt(stream->playListAt(browseMode)-playListJump,
		    browseMode))
		stream->playListAt((unsigned)0, browseMode);
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyRecall, k->key(), keyMod)) {
	    int n = stream->playListAt();

	    if (recall != n) {
		stream->playListAt(recall, false);
		recall = n;
	    }

	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPlaystop, k->key(), keyMod)) {
	    if (!stream->current())
		break;

	    if (browseMode) {
		scanB->setActive(false);
		recall = stream->playListAt();

		if (stream->current(browseMode) != stream->current() ||
			!stream->isPlaying()) {
		    if (stream->isPlaying())
			stream->stop();

		    stream->playListAt(stream->current(browseMode));
		}
	    }

	    slotStartStopStream(!browseMode);
	    browseMode = false;
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPause, k->key(), keyMod)) {
	    if (stream->isPlaying() || stream->soundId())
		stream->setPaused(!stream->isPaused());
	    else {
		if (stream->isPaused() && !inCall)
		    stream->setPaused(false);

		slotStartStopStream(!browseMode);
	    }
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPrefsIface, k->key(), keyMod)) {
	    slotPrefsInterface();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPrefsNetwork, k->key(), keyMod)) {
	    slotPrefsNetwork();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyShuffle, k->key(), keyMod)) {
	    shuffleB->setShuffled(!shuffleB->isShuffled());
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyRepeat, k->key(), keyMod)) {
	    repeatB->next();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyScan, k->key(), keyMod)) {
	    scanB->setActive(!scanB->isActive());
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyScanNext, k->key(), keyMod)) {
	    if (!stream->isPlaying()) {
		if (!stream->isLocal() && linkId != -1 && !startStream())
		    break;
		else if (!stream->isLocal() && linkId == -1) {
		    if (doLinkConnect())
			scanB->setActive(true);

		    break;
		}

		if (!startStream())
		    break;
	    }

	    scanB->setActive(true);
	    qApp->postEvent(this, EVENT(StreamScan));
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyRecord, k->key(), keyMod)) {
	    recordB->setActive(!recordB->isActive());
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyQuit, k->key(), keyMod)) {
	    slotQuit();
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyPlaylistTag, k->key(), keyMod)) {
	    cyclePlayListTag();
	    sortTagTimer->start(2000, true);
	    qApp->postEvent(this, EVENT(EZXRadioTag));
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyCopyTag, k->key(), keyMod)) {
	    QClipboard *cb = ZApplication::clipboard();
	    cb->setText(title_l->text());
	    break;
	}
	else if (config->matchBinding(EZXRadioConfig::KeyMenu, k->key(), keyMod)) {
	    ZPushButton *lb = cst->getLeftBtn();
	    lb->animateClick();
	    break;
	}
#ifdef WITH_TTS
	else if (config->matchBinding(EZXRadioConfig::KeyTTS, k->key(), keyMod)) {
	    useTTS = !useTTS;
	    playTitle(useTTS);
	    break;
	}
#endif
    }

done:
    keyMod = false;
    keyModL->setActive(false);
    keyModTimer->stop();

    if (i == EZXRadioConfig::MaxKeys)
	k->ignore();
}

void EZXRadio::slotAdjustVolume(int value)
{
    volumeL->setVolume(value);

    if (volumeL->isMuted())
	return;

    stream->setVolume(value);
}

void EZXRadio::slotAboutDialog()
{
    ZMessageBox *m = new ZMessageBox(this, NULL,
	    "EZXRadio "VERSION"\nCopyright (C) Ben Kibbey bjk@luxsci.net", "OK");
    m->show();
}

void EZXRadio::doSetPlayList(RadioPlayList *pl, bool rm)
{
    if (!pl)
	return;

    if (title_l->at() != 0)
	title_l->at(0);

    lastPlayListItem = pl->playList.at(0);
    stream->setPlayList(pl, rm);

    if (shuffleB->isShuffled())
	slotShuffle(true);
}

void EZXRadio::updateGenrePlayListIndex()
{
    if (!currentGenre)
	return;

    ShoutCastData *d = (ShoutCastData *)currentGenre->data();
    
    if (d->playList == stream->playList())
	d->playListIndex = stream->playListAt();
}

bool EZXRadio::updateShoutCastGenre(RadioPlayList *pl)
{
    ShoutCastGenre *g = findGenrePlayList(stream->playList());

    if (pl && playListSingle && (g || !currentGenre)) {
	if (currentGenre) {
	    ShoutCastData *d = (ShoutCastData *)g->data();
	    updateGenrePlayListIndex();

	    if (stream->playList()->playList.findRef(d->selected) == -1)
		return false;
	}
	
	stream->current()->setUrl(QString(pl->playList.at(0)->url()));
	stream->current()->setIcyUrl(QString(pl->playList.at(0)->icyUrl()));
	stream->current()->setTitle(QString(pl->playList.at(0)->title()));
	stream->current()->setGenre(QString(pl->playList.at(0)->genre()));
	resetPlayListItemData();
	delete pl;
	return true;
    }

    return false;
}

bool EZXRadio::openURLFinalize(QString url, bool append)
{
    RES_ICON_Reader r;

    updateGenrePlayListIndex();

    if (url == QString::null || url.isEmpty())
	return false;

    RadioPlayList *pl = new RadioPlayList();

    if (pl->parseUrl(url)) {
	if (append && stream->playListTotal()) {
	    RadioPlayListItem *i;

	    for (i = pl->playList.first(); i; i = pl->playList.next())
		stream->addPlayListItem(i);
	}
	else {
	    if (findGenrePlayList(stream->playList()))
		doSetPlayList(pl, false);
	    else
		doSetPlayList(pl);
	}

	currentGenre = NULL;
	return true;
    }
    else
	delete pl;

    ZMessageBox *m = new ZMessageBox(this,
	    r.getIcon("Dialog_Exclamatory_Mark.gif",false),
	    tr("Unrecognized or invalid URL"), "OK", NULL, NULL);
    m->exec();
    delete m;
    return false;
}

QString EZXRadio::openFileCommon(QString title, QString filter, int type,
	bool *canceled, QString dir)
{
    QString result = QString::null;
    UTIL_Dialog *Dialog = new UTIL_Dialog(UTIL_Dialog::DTLargeSize,true, this,
	    0,1,0);
    Dialog->setHasTitle(true);
    Dialog->setDlgTitle(title);

    QVBox *cw = new QVBox(Dialog);
    Dialog->setDlgContent(cw);
    QVBox *vb = (QVBox*)Dialog->getDlgContent();

    fileBrowser = new FileBrowser(filter, vb, 0, type,
	    config->hardKey(EZXRadioConfig::KeyUp), config->hardKey(EZXRadioConfig::KeyDown), config->hardKey(EZXRadioConfig::KeySelect));

    if (dir)
	fileBrowser->setDir(dir);
    else
	fileBrowser->setDir(lastDir == QString::null ? recordDirectory : lastDir);

    QObject::connect(fileBrowser, SIGNAL(isFilePicked(bool)), SLOT(filePicked(bool)));

    QHBox *hb = new QHBox(Dialog);
    Dialog->setDlgCst(hb);

    ZPushButton *okBtn = new ZPushButton(hb, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    connect( okBtn, SIGNAL( clicked() ), Dialog, SLOT(accept()));

    ZPushButton *cancelBtn = new ZPushButton(hb, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    connect( cancelBtn, SIGNAL( clicked() ), Dialog, SLOT(reject()));

    Dialog->show();

    if (Dialog->exec() == QDialog::Accepted) {
	*canceled = false;

	switch (type) {
	    case FileBrowser::FilesOnly:
		result = fileBrowser->getFileName();
		break;
	    case FileBrowser::DirsOnly:
		result = fileBrowser->getDirPath();
		break;
	    default:
		if (fileBrowser->getFileName().isEmpty())
		    result = fileBrowser->getDirPath();
		else
		    result = fileBrowser->getFileName();
		break;
	}
    }
    else
	*canceled = true;

    if (!dir)
	lastDir = fileBrowser->getDirPath();

    delete Dialog;	
    return result;
}

void EZXRadio::slotOpenFile()
{
    bool canceled;
    QString f = openFileCommon(tr("Open filename:"), "*",
	    FileBrowser::FilesOnly, &canceled);

    if (canceled || f.isEmpty())
	return;

    openURLFinalize(QString("file://" + f), false);
}

void EZXRadio::slotOpenFolder()
{
    bool canceled;
    QString f = openFileCommon(tr("Open all in folder:"), "*",
	    FileBrowser::DirsOnly, &canceled);

    updateGenrePlayListIndex();

    if (canceled || f.isEmpty())
	return;

    QDir d(f, "*");

    if (!d.count())
	return;

    RadioPlayList *pl = new RadioPlayList();

    for (unsigned i = 0; i < d.count(); i++) {
	if (d[i] == "." || d[i] == "..")
	    continue;

	QString f = QString(lastDir + "/" + d[i]);

	if (QFileInfo(f).isDir())
	    continue;

	pl->parseUrl(QString("file://" + lastDir + "/" + d[i]));
    }

    if (pl->playList.count()) {
	if (findGenrePlayList(stream->playList()))
	    doSetPlayList(pl, false);
	else
	    doSetPlayList(pl);
    }
    else
	delete pl;
}

void EZXRadio::searchCommon(QString title, QString &str,
	RadioStream::SearchType type)
{
    str = lineEdit(title, str);
    QChar c = str.at(str.length()-1);

    if (c.latin1() == '\n')
	str.truncate((str.length()-1));

    stream->search(str, type, browseMode);
}

void EZXRadio::slotSearchTitle()
{
    searchCommon(tr("Search playlist title:"), searchQuery, RadioStream::Title);
}

void EZXRadio::slotSearchStreamTitle()
{
    searchCommon(tr("Search playlisti stream title:"), searchQuery, RadioStream::StreamTitle);
}

void EZXRadio::slotSearchArtist()
{
    searchCommon(tr("Search playlist artist:"), searchQuery, RadioStream::Artist);
}

void EZXRadio::slotSearchAlbum()
{
    searchCommon(tr("Search playlist album:"), searchQuery, RadioStream::Album);
}

void EZXRadio::slotSearchUrl()
{
    searchCommon(tr("Search playlist URL:"), searchQuery, RadioStream::Url);
}

void EZXRadio::slotSearchId()
{
    searchCommon(tr("Search playlist ID:"), searchQuery, RadioStream::Id);
}

void EZXRadio::shoutCastConnectFinalize(int which)
{
    int n = which == -1 ? shoutCastCommand : which;
    bool match = false;

    switch (n) {
	case ShoutCastSearchCmd:
	    for (unsigned i = 0; i < shoutCastSearchesLB->count(); i++) {
		if (i && shoutCastSearchesLB->text(i) == searchShoutCastQuery) {
		    match = true;
		    break;
		}
	    }

	    if (!match) {
		shoutCastSearchesLB->insertItem(searchShoutCastQuery);
		config->writeFile(rcFile);
	    }

	    if (sc->search(searchShoutCastQuery))
		shoutCastL->setActive(true);
	    break;
	case ShoutCastGenreCmd:
	    if (sc->fetchGenre(currentGenre) && stream->fetches())
		shoutCastL->setActive(true);
	    break;
	case ShoutCastGenresCmd:
	    {
		QList<ShoutCastGenre> list = sc->genres();

		for (unsigned i = 0; i < list.count(); i++) {
		    ShoutCastGenre *g = list.at(i);
		    ShoutCastData *d = (ShoutCastData *)g->data();

		    if (d)
			delete d;

		    g->setData(NULL);
		}
	    }

	    sc->refreshGenres();
	    shoutCastL->setActive(true);
	    break;
    }

    shoutCastCommand = -1;
}

void EZXRadio::resetShoutCastData(ShoutCastGenre *g)
{
    ShoutCastData *d;

    if (!g)
	return;

    if (!g->data()) {
	d = new ShoutCastData(shoutCastMaxItems);
	g->setData(d);
    }

    d = (ShoutCastData *)g->data();
    d->retry = 0;
    d->maxItems = shoutCastMaxItems;
}

void EZXRadio::setCurrentGenre(unsigned n, bool search, bool reset)
{
    QList<ShoutCastGenre> list = sc->genres();
    ShoutCastData *d;

    if (currentGenre) {
	d = (ShoutCastData *)currentGenre->data();

	if (d && d->playList)
	    d->playListIndex = d->playList->playList.at();
    }

    currentGenre = list.at(n);

    if (!currentGenre)
	return;

    resetShoutCastData(currentGenre);
    d = (ShoutCastData *)currentGenre->data();

    if (d->playList && !search && !reset) {
	d->state = ShoutCast::Done;
	QCustomEvent *ev = new QCustomEvent(QEvent::Type(EZXRadioShoutCast));
	ev->setData(d);
	qApp->postEvent(this, ev);
	return;
    }

    d->playListIndex = 0;

    if (linkId == -1) {
	if (doLinkConnect())
	    shoutCastCommand = search ? ShoutCastSearchCmd : ShoutCastGenreCmd;

	return;
    }

    shoutCastConnectFinalize(search ? ShoutCastSearchCmd : ShoutCastGenreCmd);
}

void EZXRadio::slotShoutCastSearchResults()
{
    setCurrentGenre(0);
}

void EZXRadio::slotShoutCastSearch()
{
    searchShoutCastQuery = lineEdit(tr("Search SHOUTcast for:"), searchShoutCastQuery);
    QChar c = searchShoutCastQuery.at(searchShoutCastQuery.length()-1);

    if (c.latin1() == '\n')
	searchShoutCastQuery.truncate((searchShoutCastQuery.length()-1));

    setCurrentGenre(0, true);
}

QString EZXRadio::lineEdit(QString title, QString &str)
{
    QString result = QString::null;
    lineEditDialog = new UTIL_Dialog(UTIL_Dialog::DTLargeSize,true, this,
	    0,1,0);
    lineEditDialog->setHasTitle(true);
    lineEditDialog->setDlgTitle(title);

    QVBox *cw = new QVBox(lineEditDialog);
    lineEditDialog->setDlgContent(cw);
    QVBox *vb = (QVBox*)lineEditDialog->getDlgContent();
    ZScrollView *sv = new ZScrollView(vb);
    sv->enableClipper(true);
    ZMultiLineEdit *lineEditLE = new ZMultiLineEdit(sv->viewport(), true);
    connect(lineEditLE, SIGNAL(returnPressed()), lineEditDialog, SLOT(accept()));
    lineEditLE->setGeometry(ZGlobal::getContentR());
    lineEditLE->setText(str);
    sv->addChild(lineEditLE, 0, 0, true);

    QHBox *hb = new QHBox(lineEditDialog);
    lineEditDialog->setDlgCst(hb);

    ZPushButton *okBtn = new ZPushButton(hb, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    connect(okBtn, SIGNAL(clicked()), lineEditDialog, SLOT(accept()));

    ZPushButton *cancelBtn = new ZPushButton(hb, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    connect(cancelBtn, SIGNAL(clicked()), lineEditDialog, SLOT(reject()));

    lineEditDialog->show();

    if (lineEditDialog->exec() == QDialog::Accepted)
	result = lineEditLE->text();

    delete lineEditDialog;	
    lineEditDialog = NULL;
    return result;
}

void EZXRadio::slotOpenURL()
{
    openUrlLast = lineEdit(tr("Open URL:"), openUrlLast);
    QChar c = openUrlLast.at(openUrlLast.length()-1);

    if (c.latin1() == '\n')
	openUrlLast.truncate((openUrlLast.length()-1));

    openURLFinalize(openUrlLast, false);
}

void EZXRadio::slotInbufSizeChanged(int value)
{
    stream->setInputBufferSize(value*1024);
}

void EZXRadio::slotOutbufSizeChanged(int value)
{
    stream->setOutputBufferSize(value*1024);
}

void EZXRadio::slotPrefsShoutCastFinalize()
{
    prefsShoutCastDialog->hide();
    prefsShoutCastCst->hide();
    QList<ShoutCastGenre> list = sc->genres();

    for (unsigned i = 0; i < list.count(); i++) {
	ShoutCastGenre *g = list.at(i);
	ShoutCastData *d = (ShoutCastData *)g->data();

	if (d)
	    d->maxItems = shoutCastMaxItems;
    }

    config->writeFile(rcFile);
}

void EZXRadio::slotPrefsShoutCastCancel()
{
    prefsShoutCastDialog->hide();
    prefsShoutCastCst->hide();
}

void EZXRadio::slotShoutCastMaxItemsChanged(int v)
{
    shoutCastMaxItems = v;
}

void EZXRadio::slotPlayListSingle(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    playListSingle = state == 2 ? true : false;
}

void EZXRadio::slotClearShoutCastSearches()
{
    shoutCastSearchesLB->clear();
    shoutCastSearchesCB->clear();
    shoutCastSearchesLB->insertItem(tr("Recent searches"));
    config->writeFile(rcFile);
}

void EZXRadio::slotPrefsShoutCast()
{
    if (prefsShoutCastDialog) {
	prefsShoutCastDialog->show();
	prefsShoutCastCst->show();
	return;
    }

    prefsShoutCastDialog = new QFrame(this);
    ZSetLayout(prefsShoutCastDialog, ZGlobal::getContentR());
    ZScrollView *sv = new ZScrollView(prefsShoutCastDialog);
    sv->enableClipper(true);
    QBoxLayout *bl = new QBoxLayout(prefsShoutCastDialog, QBoxLayout::TopToBottom);

    QLabel *l = new QLabel(tr("Retries:"), sv->viewport());
    sv->addChild(l, 0, 0, true);
    QSpinBox *s = new QSpinBox(sv->viewport());
    s->setRange(0, 999);
    s->setValue(shoutCastRetries);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotShoutCastRetriesChanged(int)));
    sv->addChild(s, 90, 0, true);

    l = new QLabel(tr("Max items:"), sv->viewport());
    sv->addChild(l, 0, 32, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(0, 99999);
    s->setValue(shoutCastMaxItems);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotShoutCastMaxItemsChanged(int)));
    sv->addChild(s, 90, 32, true);

    l = new QLabel(tr("Reset searches:"), sv->viewport());
    sv->addChild(l, 0, 64, true);
    QHBox *hb = new QHBox(sv->viewport());
    ZPushButton *b = new ZPushButton(0, hb);
    b->setText(tr("Reset"));
    connect(b, SIGNAL(clicked()), this, SLOT(slotClearShoutCastSearches()));
    sv->addChild(hb, 90, 64, true);

    bl->addWidget(sv);
    prefsShoutCastCst = new QFrame(this);
    ZSetLayout(prefsShoutCastCst, ZGlobal::getCstR());
    ZPushButton *okBtn = new ZPushButton(prefsShoutCastCst, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    ZSetLayout(okBtn, ZGlobal::getCst2a_1R());
    connect(okBtn, SIGNAL(clicked()), this, SLOT(slotPrefsShoutCastFinalize()));

    ZPushButton *cancelBtn = new ZPushButton(prefsShoutCastCst, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    ZSetLayout(cancelBtn, ZGlobal::getCst2a_2R());
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(slotPrefsShoutCastCancel()));

    prefsShoutCastDialog->show();
    prefsShoutCastCst->show();
}

void EZXRadio::slotPrefsNetworkFinalize()
{
    prefsNetworkDialog->hide();
    prefsNetworkCst->hide();
    QStringList l = config->parseProxyPorts(proxyPortsLE->text());
    stream->setProxyPorts(l);
    stream->setProxy(proxyLE->text());
    stream->setProxyUsername(proxyUsernameLE->text());
    stream->setProxyPassword(proxyPasswordLE->text());
    RadioStream::ProxyType n = proxyTypeToEnum(proxyTypeCB->text(proxyTypeCB->currentItem()));
    stream->setProxyType(n);
    recordDirectory = recordDirLE->text();
    config->writeFile(rcFile);
}

void EZXRadio::slotPrefsNetworkCancel()
{
    prefsNetworkDialog->hide();
    prefsNetworkCst->hide();
    proxyLE->setText(stream->proxy());
    proxyUsernameLE->setText(stream->proxyUsername());
    proxyPasswordLE->setText(stream->proxyPassword());

    if (stream->proxyPorts().count())
	proxyPortsLE->setText(stream->proxyPorts().join(" "));

    recordDirLE->setText(recordDirectory);
}

void EZXRadio::slotPrefsInterfaceFinalize()
{
    prefsIfaceDialog->hide();
    prefsIfaceCst->hide();
    slotMarquee(enableMarquee ? QButton::On : QButton::Off);
    config->writeFile(rcFile);
}

void EZXRadio::slotPrefsInterfaceCancel()
{
    prefsIfaceDialog->hide();
    prefsIfaceCst->hide();
}

void EZXRadio::slotKbStateChanged(bool show)
{
    if ((prefsNetworkDialog && (proxyPortsLE->hasFocus() ||
		    proxyLE->hasFocus() || recordDirLE->hasFocus() ||
		    proxyUsernameLE->hasFocus() || proxyPasswordLE->hasFocus()))) {
	if (show)
	    ZSetLayout(prefsNetworkDialog, ZGlobal::getSmallContentR());
	else
	    ZSetLayout(prefsNetworkDialog, ZGlobal::getContentR());
    }
}

void EZXRadio::slotSelectRecordDirectory()
{
    QFileInfo fi(recordDirectory);
    QString d = recordDirectory;
    bool canceled;

    if (!fi.isDir()) {
	int n = recordDirectory.findRev('/');

	if (n != -1)
	    d = recordDirectory.left(n);
    }

    QString f = openFileCommon(tr("Record directory:"), "*",
	    FileBrowser::DirsOnly, &canceled, d);

    if (canceled)
	return;

    recordDirLE->setText(fileBrowser->getDirPath());
}

void EZXRadio::resetMarquee(QButton::ToggleState state)
{
    int val = state == QButton::On ? marqueeInterval : 0;

    title_l->setInterval(val);
    title_l->setSleepInterval(marqueeSleepInterval);
    info_l->setInterval(val);
    info_l->setSleepInterval(marqueeSleepInterval);
}

void EZXRadio::slotMarquee(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    resetMarquee(state);
    enableMarquee = state == QButton::On ? true : false;
}

void EZXRadio::slotSkipBad(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    stream->setSkipBad(state == 2 ? true : false);
}

void EZXRadio::slotFillOnEmpty(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    stream->setFillOnEmpty(state == 2 ? true : false);
}

void EZXRadio::slotSelectErrorSoundFile()
{
    selectLEFile(errorSoundFileLE);
}

void EZXRadio::slotSelectScanSoundFile()
{
    selectLEFile(scanSoundFileLE);
}

void EZXRadio::slotSelectEmptyBufferSoundFile()
{
    selectLEFile(emptyBufferSoundFileLE);
}

void EZXRadio::slotBufferEmptyLoopChanged(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    bufferEmptySoundLoop = state == 2 ? true : false;
}

void EZXRadio::slotBufferEmptyIntervalChanged(int n)
{
    bufferEmptySoundInterval = n;
}

void EZXRadio::slotSelectNetworkProfile()
{
    if (getProfileNamebySelect(networkProfile))
	return;
}

void EZXRadio::slotRecordBufSizeChanged(int value)
{
    stream->setRecordBufferSize(value*1024);
}

void EZXRadio::slotShoutCastRetriesChanged(int v)
{
    shoutCastRetries = v;
}

void EZXRadio::slotStreamRetriesChanged(int v)
{
    streamRetries = v;
}

void EZXRadio::slotStreamConnectTimeoutChanged(int v)
{
    stream->setConnectTimeout(v);
}

QString EZXRadio::proxyTypeToString(RadioStream::ProxyType t)
{
    QString type;

    switch (t) {
	case RadioStream::Socks4:
	    return "socks4";
	case RadioStream::Socks4a:
	    return "socks4a";
	case RadioStream::Socks5:
	    return "socks5";
	default:
	    return "http";
    }

    return QString::null;
}

RadioStream::ProxyType EZXRadio::proxyTypeToEnum(const QString &s)
{
    if (s.lower() == "socks4")
	return RadioStream::Socks4;
    else if (s.lower() == "socks4a")
	return RadioStream::Socks4a;
    else if (s.lower() == "socks5")
	return RadioStream::Socks5;
    else
	return RadioStream::Http;
}

void EZXRadio::slotPrefsNetwork()
{
    QString type = proxyTypeToString(stream->proxyType());

    if (prefsNetworkDialog) {
	for (int i = 0; i < proxyTypeCB->count(); i++) {
	    if (proxyTypeCB->text(i) == type) {
		proxyTypeCB->setCurrentItem(i);
		break;
	    }
	}

	prefsNetworkDialog->show();
	prefsNetworkCst->show();
	return;
    }

    prefsNetworkDialog = new QFrame(this);
    ZSetLayout(prefsNetworkDialog, ZGlobal::getContentR());
    ZScrollView *sv = new ZScrollView(prefsNetworkDialog);
    sv->enableClipper(true);
    QBoxLayout *bl = new QBoxLayout(prefsNetworkDialog, QBoxLayout::TopToBottom);

    QLabel *l = new QLabel(tr("Profile:"), sv->viewport());
    sv->addChild(l, 0, 0, true);
    QHBox *hb = new QHBox(sv->viewport());
    ZPushButton *networkProfileB = new ZPushButton(0, hb);
    networkProfileB->setText(tr("Select"));
    connect(networkProfileB, SIGNAL(clicked()), this, SLOT(slotSelectNetworkProfile()));
    sv->addChild(hb, 90, 0, true);

    QLabel *recL = new QLabel(tr("Rec. bufsize:"), sv->viewport());
    sv->addChild(recL, 0, 32, true);
    QSpinBox *recS = new QSpinBox(sv->viewport());
    recS->setRange(1, 999);
    recS->setSuffix("k");
    recS->setValue(stream->recordBufferSize()/1024);
    connect(recS, SIGNAL(valueChanged(int)), this, SLOT(slotRecordBufSizeChanged(int)));
    sv->addChild(recS, 90, 32, true);

    QLabel *fl = new QLabel(tr("Fill on empty:"), sv->viewport());
    sv->addChild(fl, 0, 64, true);
    QCheckBox *fW = new QCheckBox(sv->viewport());
    fW->setChecked(stream->fillOnEmpty());
    connect(fW, SIGNAL(stateChanged(int)), this, SLOT(slotFillOnEmpty(QButton::ToggleState)));
    sv->addChild(fW, 90, 64, true);

    l = new QLabel(tr("Decode wait:"), sv->viewport());
    sv->addChild(l, 0, 96, true);
    QSpinBox *s = new QSpinBox(sv->viewport());
    s->setRange(0, 999);
    s->setSuffix("s");
    s->setValue(stream->decodeWait());
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotDecodeWaitChanged(int)));
    sv->addChild(s, 90, 96, true);

    QLabel *recDir = new QLabel(tr("Record dir:"), sv->viewport());
    sv->addChild(recDir, 0, 128, true);
    recordDirLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(recordDirLE, 90, 160, true);
    hb = new QHBox(sv->viewport());
    ZPushButton *b = new ZPushButton("FMMS_Personal_Folder_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectRecordDirectory()));
    sv->addChild(hb, 90, 128, true);

    QLabel *ofl = new QLabel(tr("One file:"), sv->viewport());
    sv->addChild(ofl, 0, 192, true);
    QCheckBox *oneFileW = new QCheckBox(sv->viewport());
    connect(oneFileW, SIGNAL(stateChanged(int)), this, SLOT(slotRecordOneFile(QButton::ToggleState)));
    oneFileW->setChecked(recordOneFile);
    sv->addChild(oneFileW, 90, 192, true);

    QLabel *proxyL = new QLabel(tr("Proxy:"), sv->viewport());
    sv->addChild(proxyL, 0, 224, true);
    proxyLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(proxyLE, 90, 224, true);

    QLabel *proxyPortsL = new QLabel(tr("Allowed ports:"), sv->viewport());
    sv->addChild(proxyPortsL, 0, 256, true);
    proxyPortsLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(proxyPortsLE, 90, 256, true);

    l = new QLabel(tr("Username:"), sv->viewport());
    sv->addChild(l, 0, 288, true);
    proxyUsernameLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(proxyUsernameLE, 90, 288, true);

    l = new QLabel(tr("Password:"), sv->viewport());
    sv->addChild(l, 0, 320, true);
    proxyPasswordLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    proxyPasswordLE->setEchoMode(ZMultiLineEdit::Password);
    sv->addChild(proxyPasswordLE, 90, 320, true);

    l = new QLabel(tr("Type:"), sv->viewport());
    sv->addChild(l, 0, 352, true);
    proxyTypeCB = new QComboBox(sv->viewport());
    proxyTypeCB->insertItem("http");
    proxyTypeCB->insertItem("socks4", -1);
    proxyTypeCB->insertItem("socks4a", -1);
    proxyTypeCB->insertItem("socks5", -1);
    sv->addChild(proxyTypeCB, 90, 352, true);

    l = new QLabel(tr("Retries:"), sv->viewport());
    sv->addChild(l, 0, 384, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(0, 999);
    s->setValue(streamRetries);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotStreamRetriesChanged(int)));
    sv->addChild(s, 90, 384, true);

    l = new QLabel(tr("Connect timeout:"), sv->viewport());
    sv->addChild(l, 0, 416, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(1, 999);
    s->setSuffix("s");
    s->setValue(stream->connectTimeout());
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotStreamConnectTimeoutChanged(int)));
    sv->addChild(s, 90, 416, true);

    bl->addWidget(sv);
    prefsNetworkCst = new QFrame(this);
    ZSetLayout(prefsNetworkCst, ZGlobal::getCstR());
    ZPushButton *okBtn = new ZPushButton(prefsNetworkCst, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    ZSetLayout(okBtn, ZGlobal::getCst2a_1R());
    connect(okBtn, SIGNAL(clicked()), this, SLOT(slotPrefsNetworkFinalize()));

    ZPushButton *cancelBtn = new ZPushButton(prefsNetworkCst, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    ZSetLayout(cancelBtn, ZGlobal::getCst2a_2R());
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(slotPrefsNetworkCancel()));

    prefsNetworkDialog->show();
    prefsNetworkCst->show();

    /* Segfault if done before show(). */
    recordDirLE->setText(recordDirectory);
    proxyLE->setText(stream->proxy());
    proxyUsernameLE->setText(stream->proxyUsername());
    proxyPasswordLE->setText(stream->proxyPassword());

    if (stream->proxyPorts().count())
	proxyPortsLE->setText(stream->proxyPorts().join(QChar(' ')));

    for (int i = 0; i < proxyTypeCB->count(); i++) {
	if (proxyTypeCB->text(i) == type) {
	    proxyTypeCB->setCurrentItem(i);
	    break;
	}
    }
}

void EZXRadio::slotDecodeWaitChanged(int v)
{
    stream->setDecodeWait(v);
}

void EZXRadio::slotReadIntervalChanged(int v)
{
    stream->setReadInterval(v);
}

void EZXRadio::slotDecodeIntervalChanged(int v)
{
    stream->setDecodeInterval(v);
}

void EZXRadio::slotScanIntervalChanged(int v)
{
    scanInterval = v;
    stream->setScanEnabled(stream->isScanning(), scanInterval);
}

void EZXRadio::slotVolumeChanged(int v)
{
    defaultVolume = v;
}

void EZXRadio::slotMarqueeIntervalChanged(int v)
{
    marqueeInterval = v;
    resetMarquee(enableMarquee ? QButton::On : QButton::Off);
}

void EZXRadio::slotMarqueeSleepIntervalChanged(int v)
{
    marqueeSleepInterval = v;
    resetMarquee(enableMarquee ? QButton::On : QButton::Off);
}

void EZXRadio::slotPlaylistJumpChanged(int v)
{
    playListJump = v;
}

void EZXRadio::slotToggleFavorites(QButton::ToggleState s)
{
    if (s == QButton::NoChange)
	return;

    favorites = s == 2 ? true : false;
}

void EZXRadio::slotPrefsInterface()
{
    if (prefsIfaceDialog) {
	prefsIfaceDialog->show();
	prefsIfaceCst->show();
	return;
    }

    prefsIfaceDialog = new QFrame(this);
    ZSetLayout(prefsIfaceDialog, ZGlobal::getContentR());
    ZScrollView *sv = new ZScrollView(prefsIfaceDialog);
    sv->enableClipper(true);
    QBoxLayout *bl = new QBoxLayout(prefsIfaceDialog, QBoxLayout::TopToBottom);

    QLabel *l = new QLabel(tr("Cycle playlist:"), sv->viewport());
    sv->addChild(l, 0, 0, true);
    QCheckBox *cyclicPlayListW = new QCheckBox(sv->viewport());
    cyclicPlayListW->setChecked(cyclePlayList);
    connect(cyclicPlayListW, SIGNAL(stateChanged(int)), this, SLOT(slotCyclePlayList(QButton::ToggleState)));
    sv->addChild(cyclicPlayListW, 90, 0, true);

    QLabel *apl = new QLabel(tr("Autoplay:"), sv->viewport());
    sv->addChild(apl, 0, 32, true);
    QCheckBox *autoPlayW = new QCheckBox(sv->viewport());
    autoPlayW->setChecked(autoPlay);
    connect(autoPlayW, SIGNAL(stateChanged(int)), this, SLOT(slotAutoPlay(QButton::ToggleState)));
    sv->addChild(autoPlayW, 90, 32, true);

    QLabel *ml = new QLabel(tr("Marquee:"), sv->viewport());
    sv->addChild(ml, 0, 64, true);
    QCheckBox *mW = new QCheckBox(sv->viewport());
    mW->setChecked(enableMarquee);
    connect(mW, SIGNAL(stateChanged(int)), this, SLOT(slotMarquee(QButton::ToggleState)));
    sv->addChild(mW, 90, 64, true);

    QLabel *sl = new QLabel(tr("Skip bad:"), sv->viewport());
    sv->addChild(sl, 0, 96, true);
    QCheckBox *sW = new QCheckBox(sv->viewport());
    sW->setChecked(stream->skipBad());
    connect(sW, SIGNAL(stateChanged(int)), this, SLOT(slotSkipBad(QButton::ToggleState)));
    sv->addChild(sW, 90, 96, true);

    QLabel *volumeL = new QLabel(tr("Volume:"), sv->viewport());
    sv->addChild(volumeL, 0, 128, true);
    QSpinBox *volumeSL = new QSpinBox(sv->viewport());
    volumeSL->setRange(0, 100);
    volumeSL->setValue(defaultVolume);
    connect(volumeSL, SIGNAL(valueChanged(int)), this, SLOT(slotVolumeChanged(int)));
    sv->addChild(volumeSL, 90, 128, true);

    QLabel *l2 = new QLabel(tr("Scan interval:"), sv->viewport());
    sv->addChild(l2, 0, 160, true);
    QSpinBox *s = new QSpinBox(sv->viewport());
    s->setRange(1, 999);
    s->setValue(scanInterval);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotScanIntervalChanged(int)));
    sv->addChild(s, 90, 160, true);

    l = new QLabel(tr("Marquee interval:"), sv->viewport());
    sv->addChild(l, 0, 192, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(1, 9999);
    s->setSuffix("ms");
    s->setValue(marqueeInterval);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotMarqueeIntervalChanged(int)));
    sv->addChild(s, 90, 192, true);

    l = new QLabel(tr("Marquee sleep interval:"), sv->viewport());
    sv->addChild(l, 0, 224, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(1, 9999);
    s->setSuffix("ms");
    s->setValue(marqueeSleepInterval);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotMarqueeSleepIntervalChanged(int)));
    sv->addChild(s, 90, 224, true);

    l = new QLabel(tr("Playlist jump:"), sv->viewport());
    sv->addChild(l, 0, 256, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(1, 9999);
    s->setValue(playListJump);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotPlaylistJumpChanged(int)));
    sv->addChild(s, 90, 256, true);

    l = new QLabel(tr("Single pls:"), sv->viewport());
    sv->addChild(l, 0, 288, true);
    QCheckBox *c = new QCheckBox(sv->viewport());
    c->setChecked(playListSingle);
    connect(c, SIGNAL(stateChanged(int)), this, SLOT(slotPlayListSingle(QButton::ToggleState)));
    sv->addChild(c, 90, 288, true);

    l = new QLabel(tr("Favorites:"), sv->viewport());
    sv->addChild(l, 0, 320, true);
    QCheckBox *cb = new QCheckBox(sv->viewport());
    cb->setChecked(favorites);
    connect(cb, SIGNAL(stateChanged(int)), this, SLOT(slotToggleFavorites(QButton::ToggleState)));
    sv->addChild(cb, 90, 320, true);


    bl->addWidget(sv);
    prefsIfaceCst = new QFrame(this);
    ZSetLayout(prefsIfaceCst, ZGlobal::getCstR());
    ZPushButton *okBtn = new ZPushButton(prefsIfaceCst, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    ZSetLayout(okBtn, ZGlobal::getCst2a_1R());
    connect(okBtn, SIGNAL(clicked()), this, SLOT(slotPrefsInterfaceFinalize()));

    ZPushButton *cancelBtn = new ZPushButton(prefsIfaceCst, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    ZSetLayout(cancelBtn, ZGlobal::getCst2a_2R());
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(slotPrefsInterfaceCancel()));

    prefsIfaceDialog->show();
    prefsIfaceCst->show();
}

void EZXRadio::showError(QString e)
{
    UTIL_Dialog *Dialog = new UTIL_Dialog(UTIL_Dialog::DTSmallSize,true, this,
	    0,1,0);
    Dialog->setHasTitle(true);
    Dialog->setDlgTitle(tr("Connection error"));

    QVBox *cw = new QVBox(Dialog);
    Dialog->setDlgContent(cw);
    QVBox *vb = (QVBox*)Dialog->getDlgContent();
    QLabel *l = new QLabel(e, vb);
    l->setAlignment(Qt::AlignLeft|Qt::AlignVCenter|Qt::WordBreak);

    QHBox *hb = new QHBox(Dialog);
    Dialog->setDlgCst(hb);
    ZPushButton *okBtn = new ZPushButton(hb, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    connect( okBtn, SIGNAL( clicked() ), Dialog, SLOT(accept()));
    Dialog->show();
    Dialog->exec();
    delete Dialog;	
}

void EZXRadio::doPlayListShuffle(bool scanMode)
{
    bool reset;
    int n = stream->randomPlayListItem(&reset);

    if (reset && repeatB->type() != EZXRadioRepeat::All && 
	    stream->isPlaying()) {
#ifdef WITH_TTS
	if (useTTS)
	    stream->playTTS(tr("End of playlist"));
	else
#endif
	stream->playSound(eofSound);
	stream->stop();
    }
    else {
	if (stream->isPlaying()) {
	    if (reset) {
#ifdef WITH_TTS
		if (useTTS)
		    stream->playTTS(tr("Repeating all"));
		else
#endif
		stream->playSound(repeatAllSound);
	    }

	    if (scanMode) {
#ifdef WITH_TTS
		if (useTTS)
		    stream->playTTS(tr("Scanning"));
		else
#endif
		stream->playSound(scanSound);
	    }
	}

	recall = stream->playListAt();
	stream->playListAt((unsigned)n);
	lastPlayListItem = stream->current();
    }
}

void EZXRadio::doPlayListPrev(bool eof)
{
    bool n;
    bool b = eof ? false : browseMode;

    do {
	n = stream->playListPrev(b);
    } while (n && !browseMode && stream->skipBad() &&
	    stream->current()->isBad());

    if (!n) {
	if (cyclePlayList || browseMode) {
	    n = stream->playListAt(stream->playListTotal()-1, b);

	    if (n && !browseMode && stream->skipBad() &&
		    stream->current()->isBad())
		stream->playListPrev();
	}
    }

    lastPlayListItem = stream->current(b);
}

void EZXRadio::resetPlayListItemData()
{
    if ((browseMode && stream->current() != stream->current(browseMode)) ||
	    !stream->current())
	return;

    EZXRadioPlayListItemData *d = (EZXRadioPlayListItemData *)stream->current()->data();

    if (!d)
	return;

    d->resetRetries();
}

void EZXRadio::doPlayListNext(bool eof, bool scanMode)
{
    bool n;

    if ((eof || scanMode) && repeatB->type() == EZXRadioRepeat::Current) {
	if (!stream->isBad()) {
#ifdef WITH_TTS
	    if (useTTS)
		stream->playTTS(tr("Repeating"));
	    else
#endif
	    stream->playSound(repeatSound);
	    stream->restart();
	}
	else {
#ifdef WITH_TTS
	    if (useTTS)
		stream->playTTS(tr("End of playlist"));
	    else
#endif
	    stream->playSound(eofSound);
	    stream->stop();
	}

	return;
    }

    if ((!browseMode || scanMode) && shuffleB->isShuffled()) {
	doPlayListShuffle(scanMode);
	return;
    }
		
    do {
	n = stream->playListNext(eof || scanMode ? false : browseMode);
    } while (n && !browseMode && stream->skipBad() &&
	    stream->current()->isBad());

    if (eof || scanMode) {
	bool r = false;

       	if (!n) {
	    if ((repeatB->type() == EZXRadioRepeat::All &&
		    stream->isPlaying()) || (!stream->isPlaying() &&
		    cyclePlayList)) {
		n = stream->playListAt((unsigned)0, false);

		if (!n || (stream->skipBad() && stream->current()->isBad()))
		    n = stream->playListNext();

		if (n) {
		    r = true;
#ifdef WITH_TTS
		    if (useTTS)
			stream->playTTS(tr("Repeating all"));
		    else
#endif
		    stream->playSound(repeatAllSound);
		}
	    }
	    else
		stream->stop();
	}
	else if (stream->skipBad() && stream->current()->isBad())
	    n = stream->playListNext();

	if (n && scanMode && !r) {
#ifdef WITH_TTS
	    if (useTTS)
		stream->playTTS(tr("Scanning"));
	    else
#endif
	    stream->playSound(scanSound);
	}
	else if (!n) {
#ifdef WITH_TTS
	    if (useTTS)
		stream->playTTS(tr("End of playlist"));
	    else
#endif
	    stream->playSound(eofSound);
	}

	lastPlayListItem = stream->current();
	return;
    }

    if (!n && !browseMode) {
       if (cyclePlayList) {
	   n = stream->playListAt((unsigned)0, false);

	   if (n && stream->skipBad() && stream->current()->isBad())
	       n = stream->playListNext();
       }
       else
	   stream->stop();
    }

    lastPlayListItem = stream->current(browseMode);
}

void EZXRadio::updateStatusLabel()
{
    if (browseMode || stream->isLocal() || !stream->isPlaying()) {
	status_l->setText(linkId != -1 ?
		tr("link available") : tr("not connected"));
	return;
    }

    if (powerSaving)
	return;

    int i = stream->getInputBufferUsed()/1024;

    if (stream->getInputBufferUsed()%1024)
	i++;

    status_l->setText(QString("%1 %2k/%3k").arg(stream->isBuffering() || stream->isPaused() ? tr("buffering") : tr("buffered")).arg(i).arg(stream->getInputBufferSize()/1024));
}

QString EZXRadio::playListSortString()
{
    bool r;
    QString s = QString::null;

    switch (stream->sortType(&r)) {
	case RadioPlayList::SortBitRate:
	    s = tr("Bitrate");
	    break;
	case RadioPlayList::SortGenre:
	    s = tr("Genre");
	    break;
	case RadioPlayList::SortId:
	    s = tr("Id");
	    break;
	case RadioPlayList::SortTitle:
	    s = tr("Title");
	    break;
	case RadioPlayList::SortUrl:
	    s = tr("Url");
	    break;
	case RadioPlayList::SortStreamTitle:
	    s = tr("Stream Title");
	    break;
    }

    if (r)
	s.prepend(tr("Rev. "));

    s.prepend("-[ Sorting by ");
    s.append(" ]-");
    return s;
}

bool EZXRadio::shoutCastRetry(ShoutCastGenre *g)
{
    if (!g || !g->data())
	return false;

    ShoutCastData *d = (ShoutCastData *)g->data();
    unsigned n = ++d->retry;
    std::cerr << "SC RETRIES=" << std::dec << n << "/" << shoutCastRetries << "\n";

    if (n >= shoutCastRetries)
	return false;

    if (!sc->fetchGenre(g))
	return false;

    if (!stream->isPlaying()) {
#ifdef WITH_TTS
	if (useTTS)
	    stream->playTTS(tr("Retrying"));
	else
#endif
	stream->playSound(retrySound);
    }

    shoutCastL->setActive(true);
    return true;
}

QString EZXRadio::playListTagString()
{
    QString s = QString::null;

    switch (title_l->at()) {
	case 0:
	    s = tr("Title");
	    break;
	case 1:
	    s = tr("Stream Title");
	    break;
	case 2:
	    s = tr("Artist");
	    break;
	case 3:
	    s = tr("Album");
	    break;
	case 4:
	    s = tr("Genre");
	    break;
	case 5:
	    s = tr("Bitrate");
	    break;
	case 6:
	    s = tr("ICY Url");
	    break;
	case 7:
	    s = tr("Url");
	    break;
    }

    s.prepend(tr("-[ Tag "));
    return s.append(" ]-");
}

ShoutCastGenre *EZXRadio::findGenrePlayList(RadioPlayList *pl)
{
    QList<ShoutCastGenre> list = sc->genres();

    for (unsigned i = 0; i < list.count(); i++) {
	ShoutCastGenre *g = list.at(i);
	ShoutCastData *d = (ShoutCastData *)g->data();

	if (!d)
	    continue;

	if (d->playList == pl)
	    return g;
    }

    return NULL;
}

#ifdef WITH_TTS
void EZXRadio::playTitle(bool force)
{
    RadioPlayListItem *p = stream->current(browseMode);

    if (!useTTS)
	return;

    if ((!stream->isBuffering() && !stream->isLocal() && !browseMode &&
	    stream->isPlaying() && stream->current()->title() !=
	    title_l->text()) || force)
	stream->playTTS(p->title(), true);
}
#endif

void EZXRadio::customEvent(QCustomEvent *ev)
{
    ZPushButton *b = cst->getMidBtn();

    if (ev->type() < QEvent::User)
	return;

    streamState = ev->type();
    RadioPlayListItem *p = stream->current(browseMode);
    ShoutCastError *sce;
    EZXRadioPlayListItemData *d;
    ShoutCastData *sdata;
    RadioPlayList *pl;
    int n;

    switch (ev->type()) {
	case EZXRadioShoutCast:
	    sdata = (ShoutCastData *)ev->data();

	    if (!sdata->playList)
		break;

	    pl = stream->playList();
	    n = title_l->at();
	    doSetPlayList(sdata->playList, false);
	    title_l->at(n);

	    if (sdata->state == ShoutCast::Init) {
		if (!findGenrePlayList(pl))
		    delete pl;

		stream->playListAt(sdata->playListIndex);
	    }
	    else if (sdata->state == ShoutCast::Done)
		stream->playListAt(sdata->playListIndex);

	    break;
	case ShoutCastErrorEvent:
	    if (!stream->fetches())
		shoutCastL->off();

	    sce = (ShoutCastError *)ev->data();
	    std::cerr << "SC ERROR " << sce->error() << "\n";

	    if (!shoutCastRetry(sce->genre()))
		showError("SHOUTcast: " + sce->error());

	    delete sce;
	    break;
	case StreamScan:
	    doPlayListNext(false, true);
	    resetPlayListItemData();
	    break;
	case StreamScanFormat:
	    info_l->setText(tr("Reading file, please wait ..."));
	    info2_l->setText("");
	    break;
	case StreamScanFormatComplete:
	    if (stream->isBad(browseMode)) {
		qApp->postEvent(this, EVENT(StreamError));
		break;
	    }

	    if (lastPlayListItem == stream->current(browseMode) &&
		    stream->isBad(browseMode) && stream->skipBad()) {
		doPlayListNext(true);
		resetPlayListItemData();
		break;
	    }

	    if (browseMode) {
		qApp->postEvent(this, EVENT(StreamPlaying));
		qApp->postEvent(this, EVENT(StreamFormat));
	    }

	    break;
	case StreamError:
	    qApp->postEvent(this, EVENT(StreamFormat));

	    if (!stream->current()->isBad())
		break;

	    d = (EZXRadioPlayListItemData *)stream->current()->data();

	    if (!d) {
		d = new EZXRadioPlayListItemData();
		stream->current()->setData(d);
	    }

	    if (!stream->isLocal()) {
		unsigned n = d->retry();

		std::cerr << "RETRY " << std::dec << n << "/" << std::dec <<
		    streamRetries << "\n";

		if (n < streamRetries) {
#ifdef WITH_TTS
		    if (useTTS)
			stream->playTTS(tr("Retrying"));
		    else
#endif
		    stream->playSound(retrySound);
		    stream->restart();
		    break;
		}
	    }

	    resetPlayListItemData();

	    if (stream->isPlaying()) {
#ifdef WITH_TTS
		if (useTTS)
		    stream->playTTS(tr("Stream error"));
		else
#endif
		stream->playSound(streamErrorSound);
	    }

	    if (stream->isPlaying() && stream->skipBad()) {
		doPlayListNext(true, stream->isScanning());
		resetPlayListItemData();
		break;
	    }

	    stream->stop();
	    break;
	case StreamPlayList:
	    stream->stop();

	    if (!updateShoutCastGenre((RadioPlayList *)ev->data()))
		doSetPlayList((RadioPlayList *)ev->data());

	    startStream();
	    break;
	case StreamEof:
	    doPlayListNext(true);
	    resetPlayListItemData();
	    break;
	case EZXRadioLink:
	    status_l->setText(tr("waiting for link"));
	    break;
	case EZXRadioSort:
	    playing_l->setText(playListSortString());
	    break;
	case EZXRadioTag:
	    playing_l->setText(playListTagString());
	    break;
	case StreamPlaying:
	    if (stream->playListAt() != -1) {
		playing_l->setText(QString(tr("-[ %1 %2/%3 ]-")).arg(browseMode ? tr("Browsing") : tr("Playing")).arg(stream->playListAt(browseMode)+1).arg(stream->playListTotal()));
		int n = title_l->at();
		title_l->resetText();
#ifdef WITH_TTS
		playTitle();
#endif

		/* The order is important to maintain the tag display. */
		title_l->addText(p->title(), true);
		title_l->addText(p->streamTitle());
		title_l->addText(p->artist());
		title_l->addText(p->album());
		title_l->addText(p->genre());
		title_l->addText(p->bitRate() ? QString("%1").arg(p->bitRate()) : 0);
		title_l->addText(p->icyUrl());
		title_l->addText(p->url());

		if (!title_l->at(n))
		    title_l->at(0);
	    }
	    else {
		playing_l->setText(QString(tr("-[ %1 ]-").arg(browseMode ? tr("Browsing") : tr("Playing"))));
		title_l->setText(tr("not available"));
		info_l->setText(tr("not available"));
		info2_l->setText("");
	    }
	    break;
	case StreamRecord:
	    slotRecord(true);
	    break;
	case StreamInit:
	    updateStatusLabel();
	    b->setText(tr("Play"));
	    qApp->postEvent(this, EVENT(StreamPlaying));
	    break;
	case StreamConnecting:
	    if (!stream->isLocal()) {
		if (linkId == -1) {
		    stream->stop();
		    doLinkConnect();
		    break;
		}

		connectingSoundTimer->start(1000, true);
		status_l->setText(tr("connecting"));
	    }

	    b->setText(tr("Stop"));
	    break;
	case StreamConnected:
	    if (connectingSoundId && connectingSoundId == stream->soundId() &&
		    !browseMode)
		stream->stopSound();

	    updateStatusLabel();
	    break;
	case StreamFormat:
	    if (!p) {
		info_l->setText(tr("not available"));
		break;
	    }

	    if (!stream->current(browseMode)->frameInfo() ||
		    stream->current(browseMode)->isBad()) {
		if (stream->current(browseMode)->isBad())
		    info_l->setText(stream->error(-1, browseMode));
		else
		    info_l->setText(tr("not available"));

		info2_l->setText("");
		break;
	    }

	    info_l->setText(QString("%1, %2 Hz").arg(stream->frameVersion(browseMode)).arg(stream->frameRate(browseMode)));
	    info2_l->setText(QString("%1 kbit/s, %2").arg(stream->frameBitRate(browseMode)).arg(stream->frameMode(browseMode)));
	    break;
	default:
	    std::cerr << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl;
	    break;
    }
}

void EZXRadio::slotMarqueeMousePressEvent(EZXRadioMarquee *w)
{
    if (w == title_l || w == info_l)
	w->stop();
    else if (w == status_l && !stream->isLocal())
	stream->flushFillBuffer();
}

void EZXRadio::slotMarqueeMouseReleaseEvent(EZXRadioMarquee *w)
{
    if (w == title_l || w == info_l)
	w->start();
}

void EZXRadio::slotMarqueeMouseDoubleClickEvent(EZXRadioMarquee *w)
{
    if (w == title_l)
	cyclePlayListTag();
}

bool EZXRadio::doLinkConnect(bool force)
{
    startup = false;

    if (linkId != -1)
	return false;

    sessionLink = networkProfile;

    if (!sessionLink || force) {
	if (getProfileNamebySelect(sessionLink)) {
	    sessionLink = QString::null;
	    return false;
	}
    }

    int n = sessionMgr->openLink(sessionLink);

    if (n < 0)
	return false;

    linkId = n;
    qApp->postEvent(this, EVENT(EZXRadioLink));
    return true;
}

void EZXRadio::slotStartStopStream(bool doStop)
{
    if (doStop && stream->isPlaying()) {
	stream->stop();
	return;
    }

    if (linkId == -1 && !stream->isLocal()) {
	doLinkConnect();
	return;
    }

    startStream();
}

bool EZXRadio::startStream()
{
    sc->unfetchAll();
    stream->resetPlayed();

    if (currentGenre) {
	ShoutCastData *d = (ShoutCastData *)currentGenre->data();
	d->selected = stream->current();
    }

    resetPlayListItemData();
    return stream->start();
}

void EZXRadio::slotSelectRetrySoundFile()
{
    selectLEFile(retrySoundFileLE);
}

void EZXRadio::slotSelectEofSoundFile()
{
    selectLEFile(eofSoundFileLE);
}

void EZXRadio::slotSelectConnectingSoundFile()
{
    selectLEFile(connectingSoundFileLE);
}

void EZXRadio::slotConnectingLoopChanged(QButton::ToggleState state)
{
    if (state == QButton::NoChange)
	return;

    connectingSoundLoop = state == 2 ? true : false;
}

void EZXRadio::slotConnectingIntervalChanged(int n)
{
    connectingSoundInterval = n;
}

void EZXRadio::slotSelectRepeatSoundFile()
{
    selectLEFile(repeatSoundFileLE);
}

void EZXRadio::slotSelectRepeatAllSoundFile()
{
    selectLEFile(repeatAllSoundFileLE);
}

void EZXRadio::selectLEFile(ZMultiLineEdit *w)
{
    int n = w->text().findRev('/');
    QString d = QString::null;
    bool canceled;

    if (n != -1)
	d = w->text().left(n);

    QString f = openFileCommon(tr("Open filename:"), "*",
	    FileBrowser::FilesOnly, &canceled, d);

    if (canceled)
	return;

    w->setText(f);
}

void EZXRadio::slotPrefsSoundFinalize()
{
    prefsSoundDialog->hide();
    prefsSoundCst->hide();
    streamErrorSound = errorSoundFileLE->text();
    scanSound = scanSoundFileLE->text();
    bufferEmptySound = emptyBufferSoundFileLE->text();
    repeatSound = repeatSoundFileLE->text();
    repeatAllSound = repeatAllSoundFileLE->text();
    eofSound = eofSoundFileLE->text();
    connectingSound = connectingSoundFileLE->text();
    retrySound = retrySoundFileLE->text();
    config->writeFile(rcFile);
}

void EZXRadio::slotPrefsSoundCancel()
{
    prefsSoundDialog->hide();
    prefsSoundCst->hide();
    emptyBufferSoundFileLE->setText(bufferEmptySound);
    errorSoundFileLE->setText(streamErrorSound);
    scanSoundFileLE->setText(scanSound);
    repeatSoundFileLE->setText(repeatSound);
    repeatAllSoundFileLE->setText(repeatAllSound);
    connectingSoundFileLE->setText(connectingSound);
    retrySoundFileLE->setText(retrySound);
}

void EZXRadio::slotSoundVolumeChanged(int v)
{
    soundVolume = v;
    stream->setSoundVolume(v);
}

void EZXRadio::slotUseTTSChanged(QButton::ToggleState state)
{
#ifdef WITH_TTS
    if (state == QButton::NoChange)
	return;

    useTTS = state == 2 ? true : false;
#else
    (void)state;
#endif
}

void EZXRadio::slotPrefsSound()
{
    if (prefsSoundDialog) {
	prefsSoundDialog->show();
	prefsSoundCst->show();
	return;
    }

    prefsSoundDialog = new QFrame(this);
    ZSetLayout(prefsSoundDialog, ZGlobal::getContentR());
    ZScrollView *sv = new ZScrollView(prefsSoundDialog);
    sv->enableClipper(true);
    QBoxLayout *bl = new QBoxLayout(prefsSoundDialog, QBoxLayout::TopToBottom);

    QLabel *l = new QLabel(tr("Error sound:"), sv->viewport());
    sv->addChild(l, 0, 0, true);
    errorSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(errorSoundFileLE, 90, 32, true);
    QHBox *hb = new QHBox(sv->viewport());
    ZPushButton *b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectErrorSoundFile()));
    sv->addChild(hb, 90, 0, true);

    l = new QLabel(tr("Scan sound:"), sv->viewport());
    sv->addChild(l, 0, 64, true);
    scanSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(scanSoundFileLE, 90, 96, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectScanSoundFile()));
    sv->addChild(hb, 90, 64, true);

    l = new QLabel(tr("Empty sound file:"), sv->viewport());
    sv->addChild(l, 0, 128, true);
    emptyBufferSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(emptyBufferSoundFileLE, 90, 160, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectEmptyBufferSoundFile()));
    sv->addChild(hb, 90, 128, true);

    l = new QLabel(tr("Loop:"), sv->viewport());
    sv->addChild(l, 0, 192, true);
    QCheckBox *c = new QCheckBox(sv->viewport());
    c->setChecked(bufferEmptySoundLoop);
    connect(c, SIGNAL(stateChanged(int)), this, SLOT(slotBufferEmptyLoopChanged(QButton::ToggleState)));
    sv->addChild(c, 90, 192, true);

    l = new QLabel(tr("Loop interval:"), sv->viewport());
    sv->addChild(l, 0, 224, true);
    QSpinBox *s = new QSpinBox(sv->viewport());
    s->setRange(0, 999);
    s->setSuffix("s");
    s->setValue(bufferEmptySoundInterval);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotBufferEmptyIntervalChanged(int)));
    sv->addChild(s, 90, 224, true);

    l = new QLabel(tr("Repeat:"), sv->viewport());
    sv->addChild(l, 0, 256, true);
    repeatSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(repeatSoundFileLE, 90, 288, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectRepeatSoundFile()));
    sv->addChild(hb, 90, 256, true);

    l = new QLabel(tr("Repeat all:"), sv->viewport());
    sv->addChild(l, 0, 320, true);
    repeatAllSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(repeatAllSoundFileLE, 90, 352, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectRepeatAllSoundFile()));
    sv->addChild(hb, 90, 320, true);

    l = new QLabel(tr("EOF:"), sv->viewport());
    sv->addChild(l, 0, 384, true);
    eofSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(eofSoundFileLE, 90, 416, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectEofSoundFile()));
    sv->addChild(hb, 90, 384, true);

    l = new QLabel(tr("Connecting:"), sv->viewport());
    sv->addChild(l, 0, 448, true);
    connectingSoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(connectingSoundFileLE, 90, 480, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectConnectingSoundFile()));
    sv->addChild(hb, 90, 448, true);

    l = new QLabel(tr("Loop:"), sv->viewport());
    sv->addChild(l, 0, 512, true);
    c = new QCheckBox(sv->viewport());
    c->setChecked(connectingSoundLoop);
    connect(c, SIGNAL(stateChanged(int)), this, SLOT(slotConnectingLoopChanged(QButton::ToggleState)));
    sv->addChild(c, 90, 512, true);

    l = new QLabel(tr("Loop interval:"), sv->viewport());
    sv->addChild(l, 0, 544, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(0, 999);
    s->setSuffix("s");
    s->setValue(connectingSoundInterval);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotConnectingIntervalChanged(int)));
    sv->addChild(s, 90, 544, true);

    l = new QLabel(tr("Volume:"), sv->viewport());
    sv->addChild(l, 0, 576, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(0, 100);
    s->setValue(stream->soundVolume());
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotSoundVolumeChanged(int)));
    sv->addChild(s, 90, 576, true);

    l = new QLabel(tr("Retry:"), sv->viewport());
    sv->addChild(l, 0, 598, true);
    retrySoundFileLE = new ZMultiLineEdit(sv->viewport(), true, 1);
    sv->addChild(retrySoundFileLE, 90, 630, true);
    hb = new QHBox(sv->viewport());
    b = new ZPushButton("FMMS_Personal_File_S", hb);
    b->setMaximumSize(24, 24);
    connect(b, SIGNAL(clicked()), this, SLOT(slotSelectRetrySoundFile()));
    sv->addChild(hb, 90, 598, true);

#ifdef WITH_TTS
    l = new QLabel(tr("TTS:"), sv->viewport());
    sv->addChild(l, 0, 662, true);
    c = new QCheckBox(sv->viewport());
    c->setChecked(useTTS);
    connect(c, SIGNAL(stateChanged(int)), this, SLOT(slotUseTTSChanged(QButton::ToggleState)));
    sv->addChild(c, 90, 662, true);
#endif

    bl->addWidget(sv);
    prefsSoundCst = new QFrame(this);
    ZSetLayout(prefsSoundCst, ZGlobal::getCstR());
    ZPushButton *okBtn = new ZPushButton(prefsSoundCst, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    ZSetLayout(okBtn, ZGlobal::getCst2a_1R());
    connect(okBtn, SIGNAL(clicked()), this, SLOT(slotPrefsSoundFinalize()));

    ZPushButton *cancelBtn = new ZPushButton(prefsSoundCst, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    ZSetLayout(cancelBtn, ZGlobal::getCst2a_2R());
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(slotPrefsSoundCancel()));

    prefsSoundDialog->show();
    prefsSoundCst->show();

    /* Segfault if done before show(). */
    errorSoundFileLE->setText(streamErrorSound);
    scanSoundFileLE->setText(scanSound);
    emptyBufferSoundFileLE->setText(bufferEmptySound);
    repeatSoundFileLE->setText(repeatSound);
    repeatAllSoundFileLE->setText(repeatAllSound);
    eofSoundFileLE->setText(eofSound);
    connectingSoundFileLE->setText(connectingSound);
    retrySoundFileLE->setText(retrySound);
}

void EZXRadio::slotPrefsAdvancedFinalize()
{
    prefsAdvancedDialog->hide();
    prefsAdvancedCst->hide();
    config->writeFile(rcFile);
}

void EZXRadio::slotPrefsAdvancedCancel()
{
    prefsAdvancedDialog->hide();
    prefsAdvancedCst->hide();
}

void EZXRadio::slotPrefsAdvanced()
{
    if (prefsAdvancedDialog) {
	prefsAdvancedDialog->show();
	prefsAdvancedCst->show();
	return;
    }

    prefsAdvancedDialog = new QFrame(this);
    ZSetLayout(prefsAdvancedDialog, ZGlobal::getContentR());
    ZScrollView *sv = new ZScrollView(prefsAdvancedDialog);
    sv->enableClipper(true);
    QBoxLayout *bl = new QBoxLayout(prefsAdvancedDialog, QBoxLayout::TopToBottom);

    QLabel *l = new QLabel(tr("Inbuf:"), sv->viewport());
    sv->addChild(l, 0, 0, true);
    QSpinBox *s = new QSpinBox(sv->viewport());
    s->setRange(1, 9999);
    s->setSuffix("k");
    s->setValue(stream->getInputBufferSize()/1024);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotInbufSizeChanged(int)));
    sv->addChild(s, 90, 0, true);

    l = new QLabel(tr("Outbuf:"), sv->viewport());
    sv->addChild(l, 0, 32, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(1, 9999);
    s->setSuffix("k");
    s->setValue(stream->getOutputBufferSize()/1024);
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotOutbufSizeChanged(int)));
    sv->addChild(s, 90, 32, true);

    l = new QLabel(tr("read interval:"), sv->viewport());
    sv->addChild(l, 0, 64, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(50, 5000);
    s->setSuffix("ms");
    s->setValue(stream->readInterval());
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotReadIntervalChanged(int)));
    sv->addChild(s, 90, 64, true);

    l = new QLabel(tr("decode interval:"), sv->viewport());
    sv->addChild(l, 0, 96, true);
    s = new QSpinBox(sv->viewport());
    s->setRange(0, 1000);
    s->setSuffix("ms");
    s->setValue(stream->decodeInterval());
    connect(s, SIGNAL(valueChanged(int)), this, SLOT(slotDecodeIntervalChanged(int)));
    sv->addChild(s, 90, 96, true);

    bl->addWidget(sv);
    prefsAdvancedCst = new QFrame(this);
    ZSetLayout(prefsAdvancedCst, ZGlobal::getCstR());
    ZPushButton *okBtn = new ZPushButton(prefsAdvancedCst, 0, -1, -1);
    okBtn->setText(tr("OK"));	
    ZSetLayout(okBtn, ZGlobal::getCst2a_1R());
    connect(okBtn, SIGNAL(clicked()), this, SLOT(slotPrefsAdvancedFinalize()));

    ZPushButton *cancelBtn = new ZPushButton(prefsAdvancedCst, 0, -1, -1);
    cancelBtn->setText(tr("Cancel"));	
    ZSetLayout(cancelBtn, ZGlobal::getCst2a_2R());
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(slotPrefsAdvancedCancel()));

    prefsAdvancedDialog->show();
    prefsAdvancedCst->show();
}
