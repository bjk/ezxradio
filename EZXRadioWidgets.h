/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __EZXRADIOWIDGETS__
#define __EZXRADIOWIDGETS__

#include <ZApplication.h>
#include <qlabel.h>
#include <qtimer.h>
#include <qlistbox.h>
#include <ezxutillistbox.h>
#include <qlistbox.h>
#include "shoutCast.h"

class EZXRadioListBox : public QListBox
{
    Q_OBJECT

    public:
	EZXRadioListBox(int keyUp = 0, int keyDown = 0, int keySelect = 0);
	void setKeys(int keyUp, int keyDown, int keySelect);
	~EZXRadioListBox() {};

    private slots:
	void keyPressEvent(QKeyEvent *);

    private:
	int keyUp;
	int keyDown;
	int keySelect;
};

class EZXRadioVolume : public QLabel
{
    Q_OBJECT

    public:
	EZXRadioVolume(QWidget *, int volume = 0);
	~EZXRadioVolume() {};
	void setVolume(int);
	void setMuted(bool);
	bool isMuted();

    signals:
	void muteChanged(bool);

    protected:
	virtual void mousePressEvent(QMouseEvent*);

    private:
	void doMute(bool);

	bool _isMuted;
	int volume;
};

class EZXRadioRepeat : public QLabel
{
    Q_OBJECT

    public:
	enum {
	    None, Current, All, MAX
	};

	EZXRadioRepeat(QWidget *parent, int type = None);
	~EZXRadioRepeat() {};
	void setType(int);
	int type(void);
	void next();

    signals:
	void repeatChanged(int);

    protected:
	virtual void mousePressEvent(QMouseEvent*);

    private:
	QLabel *l;
	int _type;
};

class EZXRadioBlink : public QLabel
{
    Q_OBJECT

    public:
	EZXRadioBlink(QWidget *parent, const QString &data,
		unsigned interval = 500, bool pixmap = true, bool _emit = false);
	~EZXRadioBlink() {};
	void stop();
	bool isActive();
	void setActive(bool);
	void setInterval(unsigned);
	unsigned interval();
	bool isPixmap();
	void setText(const QString &);
	void off();

    signals:
	void blinkChanged(bool);

    protected:
	virtual void mousePressEvent(QMouseEvent*);

    private slots:
	void slotTimer();

    private:
	void changePixmap(void);

	QString baseDir;
	QTimer *timer;
	bool which;
	bool active;
	QString pixmap;
	unsigned _interval;
	bool _isPixmap;
	bool _emit;
};

class EZXRadioShuffle : public QLabel
{
    Q_OBJECT

    public:
	EZXRadioShuffle(QWidget *parent, bool shuffle = false);
	~EZXRadioShuffle() {};
	void setShuffled(bool);
	bool isShuffled();

    signals:
	void shuffleChanged(bool);

    protected:
	virtual void mousePressEvent(QMouseEvent*);

    private:
	void changePixmap(void);

	bool _shuffle;
	QString baseDir;
};

class GenreBrowser : public QListBox
{
    Q_OBJECT

    public:
	GenreBrowser(QWidget *, int keyUp = 0, int keyDown = 0,
		int keySelect = 0);
	
	int genre()
	{
	    return currentItem();
	};

	void setGenreList(const QList<ShoutCastGenre>);
	
    private slots:
	void keyPressEvent(QKeyEvent *);
	void slotSetGenres(QList<ShoutCastGenre>);
	
    private:	
	void refreshList();

	QList<ShoutCastGenre> _list;
	int keyUp;
	int keyDown;
	int keySelect;
};

#endif
