#!/bin/sh
MYFILE=`basename $0`
MYPATH=`echo $0 | sed -e 's/'$MYFILE'//g'`
cd $MYPATH
export QTDIR=/usr/lib/ezx
export PATH=$PATH:/ezxlocal/bin:$MYPATH/bin
export LD_LIBRARY_PATH=/lib:/usr/lib:/usr/lib/ezx/lib:$MYPATH/lib
export HOME=$MYPATH

# For file associations. Autoplay.
if [ $# -gt 0 ]; then
    exec ./EZXRadio -a $@
fi

exec ./EZXRadio $@
