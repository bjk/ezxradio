/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __STREAM_H__
#define __STREAM_H__

#include <ZApplication.h>
#include <qthread.h>
#include <qwidget.h>
#include <qtimer.h>
#include <vector>
#include <iostream>
#include <curl/curl.h>
#include "playList.h"
#include "sound.h"
#include "module.h"

/* Remote stream connect timeout in seconds. */
#define DEFAULT_CONNECT_TIMEOUT 180

/* The number of supported audio hardware formats. For use with module.h. */
#define MAX_DSP_FORMATS	128

/* Audio hold buffer size. */
#define INBUFSIZE	262144

/* Decode buffer size. */
#define OUTBUFSIZE	32768

/* Give up looking for a supported module after this many bytes. */
#define PARSE_MAX	589824

/* To reduce the amount of writes to flash memory. When reached, dump the
 * buffer to file. */
#define RECORD_BUFSIZE INBUFSIZE

#ifdef DEBUG
#define MUTEX_LOCK(m)	std::cerr << "LOCK: " << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl; \
		       (m)->lock()
#define MUTEX_UNLOCK(m)	std::cerr << "UNLOCK: " << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl; \
		       (m)->unlock()
#else
#define MUTEX_LOCK(m) (m)->lock()
#define MUTEX_UNLOCK(m) (m)->unlock()
#endif

#define EVENT(n) new QEvent(QEvent::Type(n))

/* Initial state. */
#define StreamInit QEvent::User+1

/* When connecting to a stream. */
#define StreamConnecting QEvent::User+2

/* After each iteration of file/stream read or decode. */
#define StreamConnected QEvent::User+3

/* When a new file format has been detected as determined by the module. */
#define StreamFormat QEvent::User+4

/* After a playlist change or when a remote stream has updated the current
 * tracks contents. */
#define StreamPlaying QEvent::User+5

/* The decode thread has started. startRecord() can now be called. */
#define StreamRecord QEvent::User+6

/* After the end-of-file has been reached. */
#define StreamEof QEvent::User+7

/* When starting a local file format scan. For use with scanFormat(). */
#define StreamScanFormat QEvent::User+8

/* Useful to determine what to do after an invalid file was scanned. */
#define StreamScanFormatComplete QEvent::User+9

/* For use with setScanEnabled() when the interval is reached. */
#define StreamScan QEvent::User+10

/* Some error occurred. The error string can be had by calling getError(). */
#define StreamError QEvent::User+11

/* When a remote playlist has been retrieved. There is data associated with
 * this event. It is a new RadioPlayList* and must be freed by you. */
#define StreamPlayList QEvent::User+12

/* If you have user events of your own then they should be incremented from
 * this as the base. */
#define StreamEventMax QEvent::User+13

class RadioStream;

class IcyMetaDataItem
{
    public:
	IcyMetaDataItem(const QString data, ssize_t offset)
	{
	    d = data;
	    o = offset;
	};

	QString data(void)
	{
	    return d;
	};

	ssize_t offset(void)
	{
	    return o;
	};

	void setOffset(size_t offset)
	{
	    o = offset;
	};

    private:
	QString d;
	ssize_t o;
};

class RadioStreamDecodeThread : public QObject, public QThread
{
    public:
	RadioStreamDecodeThread(RadioStream *s)
	{
	    stream = s;
	    soundId = 0;
	};

	virtual void run();

    private:
	bool updateMetaData(size_t len);
	bool decode(const unsigned char *, size_t len);

	RadioStream *stream;
	unsigned soundId;
};

class RadioStreamReadThread : public QThread
{
    public:
	RadioStreamReadThread(RadioStream *s)
	{
	    stream = s;
	};

	virtual void run();

    private:
	RadioStream *stream;
};

class RadioStreamRecordThread : public QThread
{
    public:
	RadioStreamRecordThread(RadioStream *s)
	{
	    stream = s;
	};

	virtual void run();

    private:
	RadioStream *stream;
};

class RadioStreamScanThread : public QObject, public QThread
{
    Q_OBJECT

    public:
	RadioStreamScanThread(RadioStream *s)
	{
	    stream = s;
	};

	virtual void run();

    private:
	RadioStream *stream;
};

template <class T> void freeVector(T &v)
{
    if (v.capacity() != 0)
	T().swap(v);
}

/* To be passed to a new RadioStream object. */
class RadioStreamDsp
{
    public:
	typedef int (*dspOpenFunc)(void *);
	typedef int (*dspCloseFunc)(void *);
	typedef int (*dspSyncFunc)(void *, bool);
	typedef size_t (*dspWriteFunc)(void *, void *, size_t);
	typedef int (*dspFormatFunc)(void *, int, long, int);
	typedef int (*dspSetVolumeFunc)(void *, int);
	typedef int (*dspFormatsFunc)(void *, decodeModuleFormats *, size_t);
	typedef int (*dspVolumeFunc)(void *);

	RadioStreamDsp(dspOpenFunc o, dspCloseFunc c, dspSetVolumeFunc v,
		dspFormatFunc f, dspWriteFunc w, dspSyncFunc s,
		dspFormatsFunc z, dspVolumeFunc g, void *data)
	{
	    _refCount = 0;
	    _open = o;
	    _close = c;
	    _write = w;
	    _setVolume = v;
	    _setFormat = f;
	    _sync = s;
	    _formats = z;
	    _volume = g;
	    _data = data;
	};

	int volume()
	{
	    return _volume(_data);
	};

	int open(bool reopen = false)
	{
	    if (_refCount) {
		_refCount++;
		return 0;
	    }

	    int n = _open(_data);

	    if (!n)
		_refCount++;

	    return n;
	};

	int close(bool force = false)
	{
	    if (--_refCount && !force)
		return 0;

	    int n = _close(_data);

	    if (force || n)
		_refCount = 0;

	    return n;
	};

	int setVolume(int v)
	{
	    return _setVolume(_data, v);
	};

	int setFormat(int ch, long rate, int fmt)
	{
	    return _setFormat(_data, ch, rate, fmt);
	};

	size_t write(void *data, size_t len)
	{
	    return _write(_data, data, len);
	};

	int sync(bool now = true)
	{
	    return _sync(_data, now);
	};

	int formats(decodeModuleFormats *d, size_t len)
	{
	    return _formats(_data, d, len);
	};

	unsigned refCount()
	{
	    return _refCount;
	};

    private:
	dspOpenFunc _open;
	dspCloseFunc _close;
	dspSetVolumeFunc _setVolume;
	dspVolumeFunc _volume;
	dspFormatFunc _setFormat;
	dspWriteFunc _write;
	dspSyncFunc _sync;
	dspFormatsFunc _formats;
	unsigned _refCount;
	void *_data;
};

class RadioStreamFetchError
{
    public:
	RadioStreamFetchError(unsigned id, const QString &str)
	{
	    _id = id;
	    _error = str;
	};

	unsigned id()
	{
	    return _id;
	};

	const QString &error()
	{
	    return _error;
	};

    private:
	unsigned _id;
	QString _error;
};

/* Functions that take a browseMode parameter and when true normally only
 * update a pointer to a playlist entry or return the browsed entry's pointer
 * data. It might be better to let the app take care of that though.
 */
class RadioStream : public QObject
{
    Q_OBJECT

    public:
	/* The verbose parameter is also passed to modules. */
	RadioStream(QObject *parent, RadioStreamDsp *dsp, int verbose);
	~RadioStream();

	/* The proxy to use when connecting to a remote stream. */
	enum ProxyType {
	    Http, Socks4, Socks4a, Socks5
	};

	void setProxy(const QString &);
	QString &proxy(void);
	void setProxyUsername(const QString &);
	QString &proxyUsername(void);
	void setProxyPassword(const QString &);
	QString &proxyPassword(void);
	void setProxyType(ProxyType);
	ProxyType proxyType();

	/* When specified and a proxy is also set, then only the specified
	 * ports will use the proxy. */
	void setProxyPorts(const QStringList &);
	QStringList &proxyPorts(void);

	void setConnectTimeout(long seconds);
	long connectTimeout();

	bool start();
	void stop(void);
	bool restart();
	bool isPlaying();
	/* When true then the decode thread will wait. Note that the read
	 * thread still runs and that the dsp is closed when true and reopened
	 * when false. */
	void setPaused(bool);
	bool isPaused();
	void setInputBufferSize(size_t size);
	size_t getInputBufferSize(void);
	size_t getInputBufferUsed(void);
	void setOutputBufferSize(size_t size);
	size_t getOutputBufferSize(void);
	/* Whether the URL is a local file or remote stream. */
	bool isLocal(bool browseMode = false);
	/* Whether the decode thread is waiting for more data or not. This
	 * works in conjuction with setFillOnEmpty(). */
	bool isBuffering(void);
	void setFillOnEmpty(bool);
	bool fillOnEmpty(void);
	/* The fill buffer is set to half of the input buffer size. When
	 * filling and you want to play what is buffered then call this. */
	void flushFillBuffer();

	/* The number of seconds to wait when the decode thread starts before
	 * considering setFillOnEmpty(). */
	void setDecodeWait(unsigned);
	unsigned decodeWait();

	/* Set to 0 to mute. */
	void setVolume(int value);
	int volume(void);

	/* Usually these are called after the StreamFormat event has been
	 * sent. */
	QString frameVersion(bool browseMode = false);
	long frameRate(bool browseMode = false);
	int frameBitRate(bool browseMode = false);
	QString frameMode(bool browseMode = false);

	/* Recording should only be done after the StreamRecord event has
	 * been sent. */
	int startRecord(const QString &filename);
	QString &recordFilename(void);
	void stopRecord(void);
	bool isRecording(void);
	void setRecordBufferSize(size_t size);
	size_t recordBufferSize(void);
	/* Manually flush the record buffer. It is done automatically when it
	 * becomes full. */
	void flushRecordBuffer(void);

	/* These playlist functions will send event StreamPlaylist and
	 * possibly StreamFormat when successful. */
	void setPlayList(RadioPlayList *playList, bool rm = true);
	RadioPlayList *playList(void);
	bool playListNext(bool browseMode = false);
	bool playListPrev(bool browseMode = false);
	bool playListAt(unsigned index, bool browseMode = false);
	int playListAt(bool browseMode = false);
	bool playListAt(RadioPlayListItem *, bool browseMode = false);
	bool removeCurrentPlayListEntry(bool browseMode = false);
	bool removePlayListEntry(unsigned index);
	bool addPlayListItem(RadioPlayListItem *);
	unsigned playListTotal();

	/* Searches the playlist for the specified string and sets the found
	 * item as the current one. */
	enum SearchType {
	    Id, Title, Artist, Album, Url, StreamTitle
	};

	bool search(const QString &str, SearchType type, bool browseMode = false);

	/* When theres an error (access or file format) with the current
	 * playlist item and this is set then playListNext() and
	 * playListPrev() will skip over it. */
	void setSkipBad(bool);
	bool skipBad();
	bool isBad(bool browseMode = false);

	/* Saves the current playlist to the specified file. Returns the errno
	 * on failure or 0 on success. */
	int savePlayList(const QString &filename);

	/* Resets the played status of all playlist entries. */
	void resetPlayed();

	/* Returned an index to a random playlist item. The reset parameter is
	 * set when all of the items have been played and were reset. */
	int randomPlayListItem(bool *reset);

	/* Returns the current item in the playlist. */
	RadioPlayListItem *current(bool browseMode = false);

	/* Play a local file. Whether the file is supported depends on
	 * installed modules. The 'cancel' parameter determines whether the
	 * next call to playSound will cancel the currently played sound or
	 * queue it until the current sound is finished. The 'loop' parameter
	 * will play the file continueously every 'interval' seconds. Returns
	 * a unique sound ID or 0 on error.
	 */
	unsigned playSound(const QString &filename, bool cancel = false,
		bool loop = false, int interval = 0);

	/* Text-To-Speech output. Like playSound but the first argument is the
	 * string to speak.
	 */
	unsigned playTTS(const QString &str, bool cancel = false,
		bool loop = false, int interval = 0);

	/* Returns the current sound ID or 0 if no sound is playing. */
	unsigned soundId();

	/* Stop a previously played sound from playSound(). */
	void stopSound();

	/* Removed a queued soundId. If the specified soundId is playing, then
	 * it will be stopped. */
	bool removeSoundId(unsigned);

	/* Returns true if the specified soundId was found in the queue. */
	bool hasSoundId(unsigned);

	/* Sets the volume for sounds. This is not the volume for streams. */
	void setSoundVolume(int);
	int soundVolume();

	/* Return the error of the specified or current playList index. */
	QString error(int n = -1, bool browseMode = false);

	/* The number of microseconds to wait before each iteration of the
	 * readThread. */
	void setReadInterval(unsigned);
	unsigned readInterval();

	/* The number of microseconds to wait before each iteration of the
	 * decodeThread. */
	void setDecodeInterval(unsigned);
	unsigned decodeInterval();

	/* When true and the decode thread is running and not buffering, a
	 * counter is incremented and the StreamScan event is sent after the
	 * counter exceeds the specified interval. */
	/* FIXME: better to have the app do this. */
	void setScanEnabled(bool, unsigned interval = 15);
	bool isScanning();

	/* This will try and find a supported module for the local file. It
	 * returns true when either the scanFormatThread is already running or
	 * has been started. */
	bool scanFormat(RadioPlayListItem *, bool browseMode = false);

	/* Sort the playlist according to the specified sort type. Returns
	 * false if the type is invalid. */
	bool sortPlayList(int type = 0, bool reverse = false);
	int sortType(bool *reverse);

	/* This will add a new curl handle and return a unique ID. The
	 * streamFetch signal will be emitted with the id and the data. The
	 * data will be NULL when EOF is reached - either from an error or
	 * from a successful transmission. */
	unsigned fetch(const QString &url);

	/* Removes the specified fetchId from the fetch queue and any data
	 * associated with it. */
	bool unfetch(unsigned id);

	/* Returns the number of active transfers. */
	unsigned fetches();

	/* Returns the last error of the specified fetchId. This will also
	 * do an unfetch() of the specified id. Future references to the
	 * passed id will be invalid. */
	const QString fetchError(unsigned id);

	/* Returns true if the specified fetchId is in the list. */
	bool fetchId(unsigned id);

    signals:
	void streamStart();
	void streamStop();
	void streamPaused(bool);
	void streamBufferEmpty(bool);
	void streamFetch(unsigned id, const QByteArray *data);

    private:
	friend class RadioStreamReadThread;
	friend class RadioStreamDecodeThread;
	friend class RadioStreamScanThread;
	friend class RadioStreamSoundThread;
	friend class RadioStreamRecordThread;
	friend class RadioStreamSound;
	friend class CurlWrite;
	friend class RadioStreamFetch;
	RadioPlayListItem *playListCurrent;
	RadioPlayListItem *playListBrowsing;
	RadioPlayListItem *playListScanning;
	void setDspFormat(int, long, int);
	void updateInfo(RadioPlayListItem *, decodeModule *);
	void closeDecModule(decodeModule **);
	decodeModule *findModuleFromFilename(RadioPlayListItem *,
		const QString &);
	void sendPlayingEvent(RadioPlayListItem *);
	void parseMetaTags(RadioPlayListItem *);
	bool updatePlayList(bool browseMode = false);
	bool initStream(void);
	void cleanup(void);
	int openDsp(bool);
	void closeDsp(bool force = false);
	int dspSync(bool now = true);
	size_t dspWrite(void *, size_t);
	decodeModule *findModule(RadioPlayListItem *, const unsigned char *, size_t, bool *match);
	decodeModule *findModuleFromData(RadioPlayListItem *, const unsigned char *, size_t, bool *match);
	int getDspFormats();
	void sendStreamEvents(RadioPlayListItem *);
	bool playListNextPrev(bool, bool browseMode = false);
	void setError(const QString &);
	static size_t curlWriteCb(void *, size_t, size_t, void *);
	int parseInput(const QString &str);
	bool testInputBuf(size_t);
	bool addCurlHandle(CURL *, const QString &, bool = false);
	void setFetchError(unsigned, const QString);
	bool parsePlayList(const unsigned char *, size_t, RadioPlayList::Type);

	enum {
	    UrlTypeUnknown,
	    UrlTypeBroken,
	    UrlTypeBrokenDone,
	    UrlTypePls,
	    UrlTypePlsDone,
	    UrlTypeM3u,
	    UrlTypeM3uDone,
	    UrlTypeIcy,
	    UrlTypeFound,
	    UrlTypeNeedMore,
	    UrlTypeError
	};

	QMutex *readMutex;
	QMutex *recordMutex;
	std::vector<unsigned char> inbuf;
	std::vector<unsigned char> metaintBuf;
	ssize_t metaintLen;
	size_t metaintBufLen;
	size_t total;
	size_t inbufSize;
	size_t newInbufSize;
	size_t outbufSize;
	std::vector<unsigned char> recordBuf;
	size_t recordBufLen;
	size_t _recordBufferSize;
	QString icyHttpData;
	typedef QList<IcyMetaDataItem> IcyMetaData;
	IcyMetaData icyData;
	int urlType;
	bool eof;
	size_t received;
	long icyMetaInt;
	bool init;
	RadioStreamDecodeThread decodeThread;
	RadioStreamReadThread readThread;
	RadioStreamRecordThread recordThread;
	RadioStreamScanThread scanFormatThread;
	QString recordDirectory;
	int fd;
	struct curl_slist *curlHeaders;
	int _volume;
	RadioPlayList *_playList;
	bool restarting;
	int origVolume;
	CURL *curlHandle;
	bool _fillOnEmpty;
	decodeModuleFormats dspFormats[MAX_DSP_FORMATS];
	int dspFormatLen;
	bool _skipBad;
	CURLM *curlMHandle;
	CURLMcode readRc;
	size_t lastReadSize;
	QMutex *decodeMutex;
	QMutex *playListMutex;
	QMutex *dspMutex;
	QMutex *fetchMutex;
	int recordFd;
	ProxyType _proxyType;
	QString _proxy;
	QStringList _proxyPorts;
	QString _proxyUsername;
	QString _proxyPassword;
	QString _recordFilename;
	bool paused;
	QTimer *readTimer;
	int verbose;
	decodeModule *decModule;
	bool doFill;
	bool doFlushFillBuffer;
	QTimer *decodeStartTimer;
	RadioStreamSound *playSoundW;
	QObject *parentO;
	unsigned _decodeWait;
	unsigned _decodeInterval;
	unsigned _readInterval;
	unsigned scanElapsed;
	unsigned scanInterval;
	QTimer *scanTimer;
	bool scanBrowse;
	RadioStreamDsp *dsp;
	QList<RadioStreamFetch> fetchList;
	QList<RadioStreamFetchError> fetchErrorList;
	QTimer *fetchTimer;
	int _soundVolume;
	int fileFd;
	long _connectTimeout;

    private slots:
	void slotRead(void);
	void slotFetch(void);
	void slotScanTimer();
};

class CurlWrite
{
    public:
	CurlWrite(RadioStream *, unsigned char *, size_t len);
	~CurlWrite();
	size_t process(bool isLocal = false);
	bool updateInbufSize();
	void updateRecordBuf();

    private:
	RadioStream *stream;
	unsigned char *ptr;
	size_t len;
	unsigned char *optr;
};

#endif
