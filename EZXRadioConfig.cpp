/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include "EZXRadioConfig.h"
#include <qfile.h>

EZXRadioConfig::EZXRadioConfig(EZXRadio *w)
{
    filename = QString::null;
    parent = w;
    memset(hardKeyConfig, 0, sizeof(hardKeyConfig));
}

static const char *bool2str(bool var)
{
    return var ? "true" : "false";
}

bool EZXRadioConfig::strToBool(QString &str, bool init, unsigned line)
{
    if (str.lower() == "true" || str == "1" || str.lower() == "yes")
	return true;

    if (str.lower() == "false" || str == "0" || str.lower() == "no")
	return false;

    std::cerr << filename.data() << ": parse error: line " << std::dec << line << ", using default" << std::endl;
    return init;
}

void EZXRadioConfig::writeHardKey(QTextStream &t, int which, QString s)
{
    if (!hardKeyConfig[which].keyN) {
	t << '#' << s.data() << " =\n";
	return;
    }

    for (int i = 0; i < hardKeyConfig[which].keyN; i++) {
	if (hardKeyConfig[which].key[i].mod)
	    t << s.data() << " = ^" << hardKeyConfig[which].key[i].key << "\n";
	else
	    t << s.data() << " = " << hardKeyConfig[which].key[i].key << "\n";
    }
}

bool EZXRadioConfig::writeFile(const QString &file)
{
    QFile f(file);

    if (!f.open(IO_WriteOnly)) {
	std::cerr << file.data() << ": " << tr("could not open file for writing.") << std::endl;
	return false;
    }

    filename = file;
    QTextStream t;
    t.setDevice(&f);
    t << "# Default network profile to use for remote streams.\n";

    if (parent->networkProfile)
	t << "networkProfile = " << parent->networkProfile.data() << "\n\n";
    else
	t << "#networkProfile = provider\n\n";

    t << "# Autoload the favorites playlist on startup.\n";
    t << "loadFavorites = " << bool2str(parent->favorites) << "\n\n";
    t << "# Default volume level.\n";
    t << "volume = " << parent->defaultVolume << "\n\n";
    t << "# The volume level for sounds (not the stream volume).\n";
    t << "soundsVolume = " << parent->soundVolume << "\n\n";
    t << "# The number of seconds to wait before selecting the next track in scan mode.\n";
    t << "scanInterval = " << parent->scanInterval << "\n\n";
    t << "# A sound to play when scanning the next item.\n";

    if (parent->scanSound)
	t << "scanSound = " << parent->scanSound.data() << "\n\n";
    else
	t << "#scanSound = /path/to/some/file.mp3\n\n";

    t << "# Network connect timeout\n";
    t << "connectTimeout = " << parent->stream->connectTimeout() << "\n\n";
    t << "# Audio hold buffer size.\n";
    t << "inBufSize = " << parent->stream->getInputBufferSize()/1024 << "\n\n";
    t << "# Decode buffer size.\n";
    t << "outBufSize = " << parent->stream->getOutputBufferSize()/1024 << "\n\n";
    t << "# Record buffer size. Will be written to file when reached.\n";
    t << "recordBufSize = " << parent->stream->recordBufferSize()/1024 << "\n\n";
    t << "# Where to store recorded files.\n";
    t << "recordDirectory = " << parent->recordDirectory.data() << "\n\n";
    t << "# Whether to start a new file or append to the existing one.\n";
    t << "recordOneFile = " << bool2str(parent->recordOneFile) << "\n\n";
    t << "# Proxy to use for network streams.\n";

    if (!parent->stream->proxy().isEmpty())
	t << "proxy = " << parent->stream->proxy().data() << "\n\n";
    else
	t << "#proxy = 123.123.123.123:8080\n\n";

    t << "# If specified, only these ports will use the proxy.\n";

    if (parent->stream->proxyPorts().count())
	t << "allowedProxyPorts = " <<
	    parent->stream->proxyPorts().join(QChar(' ')).data() << "\n\n";
    else
	t << "#allowedProxyPorts = 80 8080\n\n";

    t << "# Proxy username.\n";

    if (!parent->stream->proxyUsername().isEmpty())
	t << "proxyUsername = " << parent->stream->proxyUsername().data() << "\n\n";
    else
	t << "#proxyUsername = username\n\n";

    t << "# Proxy password.\n";

    if (!parent->stream->proxyPassword().isEmpty())
	t << "proxyPassword = " << parent->stream->proxyPassword().data() << "\n\n";
    else
	t << "#proxyPassword = password\n\n";

    t << "# Proxy type: http, socks4, socks4a, socks5.\n";
    QString s = parent->proxyTypeToString(parent->stream->proxyType());
    t << "proxyType = " << s.data() << "\n\n";
    t << "# The number of times to retry a remote stream.\n";
    t << "streamRetries = " << parent->streamRetries << "\n\n";
    t << "# The number of milliseconds to wait at each read thread iteration.\n";
    t << "readInterval = " << parent->stream->readInterval() << "\n\n";
    t << "# The number of milliseconds to wait at each decode iteration.\n";
    t << "decodeInterval = " << parent->stream->decodeInterval() << "\n\n";
    t <<
	"# When set and the hold buffer becomes empty then the decoding will stop until\n"
	"# the hold buffer reached half of it's maximum.\n";
    t << "fillOnEmpty = " << bool2str(parent->stream->fillOnEmpty()) << "\n\n";
    t << "# A sound file to play when the hold buffer becomes empty.\n";

#ifdef WITH_TTS
    t << "# Use TTS for stream events rather than sound files.\n";
    t << "useTTS = " << bool2str(parent->useTTS) << "\n\n";
#endif

    if (!parent->bufferEmptySound.isEmpty())
	t << "bufferEmptySound = " << parent->bufferEmptySound.data() << "\n\n";
    else
	t << "#bufferEmptySound = /path/to/filename\n\n";

    t << "# Whether to loop the bufferEmptySound.\n";
    t << "bufferEmptySoundLoop = " << bool2str(parent->bufferEmptySoundLoop) << "\n\n";
    t << "# The interval to loop the bufferEmptySound in seconds.\n";
    t << "bufferEmptySoundInterval = " << parent->bufferEmptySoundInterval << "\n\n";
    t << "# The initial number of seconds to wait before fillOnEmpty is considered.\n";
    t << "decodeWait = " << parent->stream->decodeWait() << "\n\n";
    t << "# A sound file to play when a stream error occurs.\n";

    if (!parent->streamErrorSound.isEmpty())
	t << "streamErrorSound = " << parent->streamErrorSound.data() << "\n\n";
    else
	t << "#streamErrorSound = /path/to/some/filename\n\n";

    t << "# A sound file to play when retrying a remote stream.\n";

    if (!parent->retrySound.isEmpty())
	t << "retrySound = " << parent->retrySound.data() << "\n\n";
    else
	t << "#retrySound = /path/to/some/filename\n\n";

    t << "# A sound file to play when repeating a single playlist item.\n";
    if (!parent->repeatSound.isEmpty())
	t << "repeatSound = " << parent->repeatSound.data() << "\n\n";
    else
	t << "#repeatSound = /path/to/some/filename\n\n";

    t << "# A sound file to play when repeating all playlist items.\n";

    if (!parent->repeatAllSound.isEmpty())
	t << "repeatAllSound = " << parent->repeatAllSound.data() << "\n\n";
    else
	t << "#repeatAllSound = /path/to/some/filename\n\n";

    t << "# A sound file to play when there are no more playlist items.\n";

    if (!parent->eofSound.isEmpty())
	t << "eofSound = " << parent->eofSound.data() << "\n\n";
    else
	t << "#eofSound = /path/to/some/filename\n\n";

    t << "# A sound file to play while connecting.\n";

    if (!parent->connectingSound.isEmpty())
	t << "connectingSound = " << parent->connectingSound.data() << "\n\n";
    else
	t << "#connectingSound = /path/to/some/filename\n\n";

    t << "# Whether to loop the connectingSound.\n";
    t << "connectingSoundLoop = " << bool2str(parent->connectingSoundLoop) << "\n\n";
    t << "# The interval to loop the connectingSound in seconds.\n";
    t << "connectingSoundInterval = " << parent->connectingSoundInterval << "\n\n";
    t << "# The interval to wait when scrolling a single character in milliseconds.\n";
    t << "marqueeInterval = " << parent->marqueeInterval << "\n\n";
    t << "# The interval to wait at the beginning and end of the marquee string.\n";
    t << "marqueeSleepInterval = " << parent->marqueeSleepInterval << "\n\n";
    t << "# Whether the next and previous playlist keys wrap around.\n";
    t << "cyclicPlayList = " << bool2str(parent->cyclePlayList) << "\n\n";
    t << "# Whether to automatically start playing on file load.\n";
    t << "autoPlay = " << bool2str(parent->autoPlay) << "\n\n";
    t << "# Enable or disable the scrolling title.\n";
    t << "enableMarquee = " << bool2str(parent->enableMarquee) << "\n\n";
    t << "# Whether to automatically skip over files with errors.\n";
    t << "skipBadFiles = " << bool2str(parent->stream->skipBad()) << "\n\n";
    t << "# For use with keyPlayListNextJump and keyPlayListPrevJump.\n";
    t << "playListJump = " << parent->playListJump << "\n\n";
    t << "# The number of times to retry a SHOUTcast fetch.\n";
    t << "shoutCastRetries = " << parent->shoutCastRetries << "\n\n";
    t << "# The maximum number of items to fetch for each genre.\n";
    t << "shoutCastMaxItems = " << parent->shoutCastMaxItems << "\n\n";

    QStringList l;

    for (unsigned i = 1; i < parent->shoutCastSearchesLB->count(); i++)
	l << parent->shoutCastSearchesLB->text(i);

    t << "# Recent SHOUTcast searches.\n";

    if (l.count())
	t << "shoutCastSearches = " << l.join("|").data() << "\n\n";
    else
	t << "#shoutCastSearches = " << "\n\n";

    t << "# Whether to load only the first remote playlist item.\n";
    t << "playListSingle = " << bool2str(parent->playListSingle) << "\n\n";
    t << "# The default font size in points.\n";
    t << "defaultFontSize = " << parent->defaultFontSize << "\n\n";
    t << "# The default font color.\n";
    t << "defaultColor = " << parent->defaultColor.data() << "\n\n";
    t << "# The section header font size.\n";
    t << "headerFontSize = " << parent->headerFontSize << "\n\n";
    t << "# The section header font color.\n";
    t << "headerColor = " << parent->headerColor.data() << "\n\n";
    t << "# The meta tag font size.\n";
    t << "tagFontSize = " << parent->tagFontSize << "\n\n";
    t << "# The meta tag font color.\n";
    t << "tagColor = " << parent->tagColor.data() << "\n\n";
    t << "# The stream format info font size.\n";
    t << "formatFontSize = " << parent->formatFontSize << "\n\n";
    t << "# The stream format info font color.\n";
    t << "formatColor = " << parent->formatColor.data() << "\n\n";
    t << "# The network info font size.\n";
    t << "statusFontSize = " << parent->statusFontSize << "\n\n";
    t << "# The network info font color.\n";
    t << "statusColor = " << parent->statusColor.data() << "\n\n";
    t <<
	"# Key bindings. Some bindings are only available in browse mode. They are:\n"
	"# keySavePlayList, keyDeletePlayListItem, keyRemovePlayListItem,\n"
	"# keyAddPlayListItem, keyOpenFile and keyOpenFolder.\n"
	"#\n"
	"# A keyModifier keycode begins with the ^ character. This allows for twice as\n"
	"# many bindings as there are physical keys. There are no default bindings.\n";

    for (int i = 0; i < MaxKeys; i++) {
	switch (i) {
	    case KeyOpenFavorites:
		writeHardKey(t, i, "keyOpenFavorites");
		break;
	    case KeyAddFavorite:
		writeHardKey(t, i, "keyAddFavorite");
		break;
	    case KeyMute:
		writeHardKey(t, i, "keyMute");
		break;
	    case KeyVolumeUp:
		writeHardKey(t, i, "keyVolumeUp");
		break;
	    case KeyVolumeDown:
		writeHardKey(t, i, "keyVolumeDown");
		break;
	    case KeyPlaylistNext:
		writeHardKey(t, i, "keyPlayListNext");
		break;
	    case KeyPlaylistNextJump:
		writeHardKey(t, i, "keyPlayListNextJump");
		break;
	    case KeyPlaylistPrev:
		writeHardKey(t, i, "keyPlayListPrev");
		break;
	    case KeyPlaylistPrevJump:
		writeHardKey(t, i, "keyPlayListPrevJump");
		break;
	    case KeyPause:
		writeHardKey(t, i, "keyPause");
		break;
	    case KeyPlaystop:
		writeHardKey(t, i, "keyPlayStop");
		break;
	    case KeyBrowse:
		writeHardKey(t, i, "keyBrowse");
		break;
	    case KeyOpenfolder:
		writeHardKey(t, i, "keyOpenFolder");
		break;
	    case KeyOpenfile:
		writeHardKey(t, i, "keyOpenFile");
		break;
	    case KeyPrefsIface:
		writeHardKey(t, i, "keyPrefsInterface");
		break;
	    case KeyPrefsNetwork:
		writeHardKey(t, i, "keyPrefsNetwork");
		break;
	    case KeyShuffle:
		writeHardKey(t, i, "keyShuffle");
		break;
	    case KeyRepeat:
		writeHardKey(t, i, "keyRepeat");
		break;
	    case KeyScan:
		writeHardKey(t, i, "keyScan");
		break;
	    case KeyScanNext:
		writeHardKey(t, i, "keyScanNext");
		break;
	    case KeyRecord:
		writeHardKey(t, i, "keyRecord");
		break;
	    case KeyQuit:
		writeHardKey(t, i, "keyQuit");
		break;
	    case KeyPlaylistSave:
		writeHardKey(t, i, "keySavePlayList");
		break;
	    case KeyPlaylistDelete:
		writeHardKey(t, i, "keyDeletePlayListItem");
		break;
	    case KeyPlaylistRemove:
		writeHardKey(t, i, "keyRemovePlayListItem");
		break;
	    case KeyPlaylistAdd:
		writeHardKey(t, i, "keyAddPlayListItem");
		break;
	    case KeyLeft:
		writeHardKey(t, i, "keyLeft");
		break;
	    case KeyRight:
		writeHardKey(t, i, "keyRight");
		break;
	    case KeyUp:
		writeHardKey(t, i, "keyUp");
		break;
	    case KeyDown:
		writeHardKey(t, i, "keyDown");
		break;
	    case KeySelect:
		writeHardKey(t, i, "keySelect");
		break;
	    case KeyPlaylistTag:
		writeHardKey(t, i, "keyPlayListTag");
		break;
	    case KeyRecall:
		writeHardKey(t, i, "keyRecall");
		break;
	    case KeySort:
		writeHardKey(t, i, "keySort");
		break;
	    case KeySortReverse:
		writeHardKey(t, i, "keySortReverse");
		break;
	    case KeyModifier:
		writeHardKey(t, i, "keyModifier");
		break;
	    case KeyCopyTag:
		writeHardKey(t, i, "keyCopyTag");
		break;
	    case KeyMenu:
		writeHardKey(t, i, "keyMenu");
		break;
#ifdef WITH_TTS
	    case KeyTTS:
		writeHardKey(t, i, "keyTTS");
		break;
#endif
	    default:
		break;
	}
    }

    f.close();
    return true;
}

void EZXRadioConfig::parseHardKey(EZXRadioKey which, QString &key)
{
    if (hardKeyConfig[which].keyN >= MAX_BINDINGS)
	return;

    const char *p = key.data();
    bool mod = false;

    if (*p == '^') {
	mod = true;
	p++;
    }

    hardKeyConfig[which].key[hardKeyConfig[which].keyN].key = atoi(p);
    hardKeyConfig[which].key[hardKeyConfig[which].keyN++].mod = mod;
}

bool EZXRadioConfig::readFile(const QString &file)
{
    QFile f(file);

    if (!f.open(IO_ReadOnly)) {
	std::cerr << file.data() << ": " << tr("could not open file for reading.") << std::endl;
	return false;
    }

    filename = file;
    QTextStream t;
    t.setDevice(&f);
    unsigned lineN = 0;
    QString line;

    while ((line = t.readLine()) != QString::null) {
	++lineN;

	if (line.at(0) == '#' || line.isEmpty())
	    continue;

	line.stripWhiteSpace();
	int n = line.find('=');

	if (n == -1) {
	    std::cerr << file.data() << ": parse error: line " << std::dec << lineN << std::endl;
	    continue;
	}
	
	QString name = line.left(n).stripWhiteSpace();
	QString val = line.mid(n+1).stripWhiteSpace();

	if (name.lower() == QString("inBufSize").lower())
	    parent->stream->setInputBufferSize(val.toInt()*1024);
	else if (name.lower() == QString("connectTimeout").lower())
	    parent->stream->setConnectTimeout(val.toLong());
	else if (name.lower() == QString("marqueeInterval").lower())
	    parent->marqueeInterval = val.toInt();
	else if (name.lower() == QString("playListJump").lower())
	    parent->playListJump = val.toInt();
	else if (name.lower() == QString("marqueeSleepInterval").lower())
	    parent->marqueeSleepInterval = val.toInt();
	else if (name.lower() == QString("networkProfile").lower())
	    parent->networkProfile = val;
	else if (name.lower() == QString("retrySound").lower())
	    parent->retrySound = val;
	else if (name.lower() == QString("streamErrorSound").lower())
	    parent->streamErrorSound = val;
	else if (name.lower() == QString("repeatSound").lower())
	    parent->repeatSound = val;
	else if (name.lower() == QString("repeatAllSound").lower())
	    parent->repeatAllSound = val;
	else if (name.lower() == QString("scanSound").lower())
	    parent->scanSound = val;
	else if (name.lower() == QString("eofSound").lower())
	    parent->eofSound = val;
	else if (name.lower() == QString("connectingSound").lower())
	    parent->connectingSound = val;
	else if (name.lower() == QString("connectingSoundLoop").lower())
	    parent->connectingSoundLoop = strToBool(val, parent->connectingSoundLoop, lineN);
	else if (name.lower() == QString("connectingSoundInterval").lower())
	    parent->connectingSoundInterval = val.toInt();
	else if (name.lower() == QString("readInterval").lower())
	    parent->stream->setReadInterval(val.toInt());
	else if (name.lower() == QString("decodeInterval").lower())
	    parent->stream->setDecodeInterval(val.toInt());
	else if (name.lower() == QString("scanInterval").lower())
	    parent->scanInterval = val.toInt();
	else if (name.lower() == QString("volume").lower())
	    parent->defaultVolume = val.toInt();
	else if (name.lower() == QString("soundsVolume").lower())
	    parent->soundVolume = val.toInt();
	else if (name.lower() == QString("decodeWait").lower())
	    parent->stream->setDecodeWait(val.toInt());
	else if (name.lower() == QString("outBufSize").lower())
	    parent->stream->setOutputBufferSize(val.toInt()*1024);
	else if (name.lower() == QString("recordBufSize").lower())
	    parent->stream->setRecordBufferSize(val.toInt()*1024);
	else if (name.lower() == QString("recordDirectory").lower())
	    parent->recordDirectory = val;
	else if (name.lower() == QString("proxy").lower())
	    parent->stream->setProxy(val);
	else if (name.lower() == QString("allowedProxyPorts").lower()) {
	    QStringList l = parseProxyPorts(val);
	    parent->stream->setProxyPorts(l);
	}
	else if (name.lower() == QString("proxyUsername").lower())
	    parent->stream->setProxyUsername(val);
	else if (name.lower() == QString("proxyPassword").lower())
	    parent->stream->setProxyPassword(val);
	else if (name.lower() == QString("proxyType").lower())
	    parent->stream->setProxyType(parent->proxyTypeToEnum(val));
	else if (name.lower() == QString("cyclicPlayList").lower())
	    parent->cyclePlayList = strToBool(val, parent->cyclePlayList, lineN);
	else if (name.lower() == QString("loadFavorites").lower())
	    parent->favorites = strToBool(val, parent->favorites, lineN);
	else if (name.lower() == QString("autoPlay").lower())
	    parent->autoPlay = strToBool(val, parent->autoPlay, lineN);
	else if (name.lower() == QString("recordOneFile").lower())
	    parent->recordOneFile = strToBool(val, parent->recordOneFile, lineN);
	else if (name.lower() == QString("enableMarquee").lower())
	    parent->enableMarquee = strToBool(val, parent->enableMarquee, lineN);
	else if (name.lower() == QString("skipBadFiles").lower())
	    parent->stream->setSkipBad(strToBool(val, parent->stream->skipBad(), lineN));
#ifdef WITH_TTS
	else if (name.lower() == QString("useTTS").lower())
	    parent->useTTS = (strToBool(val, parent->useTTS, lineN));
#endif
	else if (name.lower() == QString("fillOnEmpty").lower())
	    parent->stream->setFillOnEmpty(strToBool(val, parent->stream->fillOnEmpty(), lineN));
	else if (name.lower() == QString("bufferEmptySound").lower())
	    parent->bufferEmptySound = val;
	else if (name.lower() == QString("bufferEmptySoundLoop").lower())
	    parent->bufferEmptySoundLoop = strToBool(val, parent->bufferEmptySoundLoop, lineN);
	else if (name.lower() == QString("bufferEmptySoundInterval").lower())
	    parent->bufferEmptySoundInterval = val.toInt();
	else if (name.lower() == QString("streamRetries").lower())
	    parent->streamRetries = val.toInt();
	else if (name.lower() == QString("shoutCastRetries").lower())
	    parent->shoutCastRetries = val.toInt();
	else if (name.lower() == QString("shoutCastMaxItems").lower())
	    parent->shoutCastMaxItems = val.toInt();
	else if (name.lower() == QString("shoutCastSearches").lower()) {
	    QStringList l = QStringList::split(QChar('|'), val);
	    parent->shoutCastSearchesLB->insertStringList(l);
	}
	else if (name.lower() == QString("playListSingle").lower())
	    parent->playListSingle = strToBool(val, parent->playListSingle, lineN);
	else if (name.lower() == QString("keyMute").lower())
	    parseHardKey(KeyMute, val);
	else if (name.lower() == QString("keyOpenFavorites").lower())
	    parseHardKey(KeyOpenFavorites, val);
	else if (name.lower() == QString("keyAddFavorite").lower())
	    parseHardKey(KeyAddFavorite, val);
	else if (name.lower() == QString("keyVolumeUp").lower())
	    parseHardKey(KeyVolumeUp, val);
	else if (name.lower() == QString("keyVolumeDown").lower())
	    parseHardKey(KeyVolumeDown, val);
	else if (name.lower() == QString("keyPlayListNext").lower())
	    parseHardKey(KeyPlaylistNext, val);
	else if (name.lower() == QString("keyPlayListNextJump").lower())
	    parseHardKey(KeyPlaylistNextJump, val);
	else if (name.lower() == QString("keyPlayListPrev").lower())
	    parseHardKey(KeyPlaylistPrev, val);
	else if (name.lower() == QString("keyPlayListPrevJump").lower())
	    parseHardKey(KeyPlaylistPrevJump, val);
	else if (name.lower() == QString("keyPause").lower())
	    parseHardKey(KeyPause, val);
	else if (name.lower() == QString("keyBrowse").lower())
	    parseHardKey(KeyBrowse, val);
	else if (name.lower() == QString("keyPlayStop").lower())
	    parseHardKey(KeyPlaystop, val);
	else if (name.lower() == QString("keyOpenFile").lower())
	    parseHardKey(KeyOpenfile, val);
	else if (name.lower() == QString("keyOpenFolder").lower())
	    parseHardKey(KeyOpenfolder, val);
	else if (name.lower() == QString("keyShuffle").lower())
	    parseHardKey(KeyShuffle, val);
	else if (name.lower() == QString("keyPrefsInterface").lower())
	    parseHardKey(KeyPrefsIface, val);
	else if (name.lower() == QString("keyPrefsNetwork").lower())
	    parseHardKey(KeyPrefsNetwork, val);
	else if (name.lower() == QString("keyRepeat").lower())
	    parseHardKey(KeyRepeat, val);
	else if (name.lower() == QString("keyScan").lower())
	    parseHardKey(KeyScan, val);
	else if (name.lower() == QString("keyScanNext").lower())
	    parseHardKey(KeyScanNext, val);
	else if (name.lower() == QString("keyRecord").lower())
	    parseHardKey(KeyRecord, val);
	else if (name.lower() == QString("keyQuit").lower())
	    parseHardKey(KeyQuit, val);
	else if (name.lower() == QString("keyDeletePlayListItem").lower())
	    parseHardKey(KeyPlaylistDelete, val);
	else if (name.lower() == QString("keyRemovePlayListItem").lower())
	    parseHardKey(KeyPlaylistRemove, val);
	else if (name.lower() == QString("keyAddPlayListItem").lower())
	    parseHardKey(KeyPlaylistAdd, val);
	else if (name.lower() == QString("keySavePlayList").lower())
	    parseHardKey(KeyPlaylistSave, val);
	else if (name.lower() == QString("keyLeft").lower())
	    parseHardKey(KeyLeft, val);
	else if (name.lower() == QString("keyRight").lower())
	    parseHardKey(KeyRight, val);
	else if (name.lower() == QString("keyUp").lower())
	    parseHardKey(KeyUp, val);
	else if (name.lower() == QString("keyDown").lower())
	    parseHardKey(KeyDown, val);
	else if (name.lower() == QString("keySelect").lower())
	    parseHardKey(KeySelect, val);
	else if (name.lower() == QString("keyPlayListTag").lower())
	    parseHardKey(KeyPlaylistTag, val);
	else if (name.lower() == QString("keyRecall").lower())
	    parseHardKey(KeyRecall, val);
	else if (name.lower() == QString("keySort").lower())
	    parseHardKey(KeySort, val);
	else if (name.lower() == QString("keySortReverse").lower())
	    parseHardKey(KeySortReverse, val);
	else if (name.lower() == QString("keyModifier").lower())
	    parseHardKey(KeyModifier, val);
	else if (name.lower() == QString("keyCopyTag").lower())
	    parseHardKey(KeyCopyTag, val);
	else if (name.lower() == QString("keyMenu").lower())
	    parseHardKey(KeyMenu, val);
#ifdef WITH_TTS
	else if (name.lower() == QString("keyTTS").lower())
	    parseHardKey(KeyTTS, val);
#endif
	else if (name.lower() == QString("headerFontSize").lower())
	    parent->headerFontSize = val.toInt();
	else if (name.lower() == QString("headerColor").lower())
	    parent->headerColor = val;
	else if (name.lower() == QString("tagFontSize").lower())
	    parent->tagFontSize = val.toInt();
	else if (name.lower() == QString("tagColor").lower())
	    parent->tagColor = val;
	else if (name.lower() == QString("formatFontSize").lower())
	    parent->formatFontSize = val.toInt();
	else if (name.lower() == QString("formatColor").lower())
	    parent->formatColor = val;
	else if (name.lower() == QString("statusFontSize").lower())
	    parent->statusFontSize = val.toInt();
	else if (name.lower() == QString("statusColor").lower())
	    parent->statusColor= val;
	else if (name.lower() == QString("defaultFontSize").lower())
	    parent->defaultFontSize = val.toInt();
	else if (name.lower() == QString("defaultColor").lower())
	    parent->defaultColor = val;
	else
	    std::cerr << file.data() << ": parse error: line " << std::dec << lineN << std::endl;
    }

    f.close();
    return true;
}

bool EZXRadioConfig::matchBinding(EZXRadioKey which, int key, bool mod)
{
    for (int i = 0; i < MAX_BINDINGS; i++) {
	if (hardKeyConfig[which].key[i].key == key) {
	    if (hardKeyConfig[which].key[i].key == key &&
		    hardKeyConfig[which].key[i].mod == mod)
		return true;
	}
    }

    return false;
}

bool EZXRadioConfig::hasBinding(int key, bool mod)
{
    for (int b = 0; b < MaxKeys; b++) {
	for (int k = 0; k < MAX_BINDINGS; k++) {
	    if (hardKeyConfig[b].key[k].key == key &&
		    hardKeyConfig[b].key[k].mod == mod)
		return true;
	}
    }

    return false;
}

int EZXRadioConfig::hardKey(EZXRadioKey which)
{
    return hardKeyConfig[which].key[0].key;
}

QStringList EZXRadioConfig::parseProxyPorts(const QString &str)
{
    bool ok = true;
    QStringList l = QStringList::split(' ', str);

    for (QStringList::Iterator it = l.begin(); it != l.end(); ++it) {
	int len = (*it).length();
	int p = (*it).toInt(&ok, 10);

	if (p <= 0 || p > 65535) {
	    ok = false;
	    break;
	}

	for (int i = 0; i < len; i++) {
	    QChar c = (*it).at(i);

	    if (!c.isDigit()) {
		ok = false;
		break;
	    }
	}
    }

    if (!ok)
	l.clear();

    return l;
}
