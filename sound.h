/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __SOUND_H__
#define __SOUND_H__

#ifdef WITH_TTS
#include <espeak/speak_lib.h>
#endif
#include "stream.h"

class RadioStreamSound;
class RadioStreamSoundItem;
class RadioStreamSoundThread : public QThread
{
    public:
	RadioStreamSoundThread(RadioStreamSound *s)
	{
	    sound = s;
	};

	virtual void run();

    private:
	bool doPlay(RadioStreamSoundItem *);

	RadioStreamSound *sound;
};

class RadioStreamSoundItem
{
    public:
	RadioStreamSoundItem(const QString &d, bool c, bool l, unsigned i, bool t)
	{
	    static unsigned __soundId;

	    _tts = t;
	    _data = d;
	    _cancel = c;
	    _loop = l;
	    _interval = i;
	    _soundId = ++__soundId;
	};

	bool tts()
	{
	    return _tts;
	};

	unsigned soundId()
	{
	    return _soundId;
	};

	const QString &data()
	{
	    return _data;
	};

	bool cancel()
	{
	    return _cancel;
	};

	bool loop()
	{
	    return _loop;
	};

	unsigned interval()
	{
	    return _interval;
	};

    private:
	QString _data;
	bool _cancel;
	bool _loop;
	unsigned _interval;
	unsigned _soundId;
	bool _tts;
};

class RadioStream;
class RadioStreamSound : public QObject
{
    Q_OBJECT

    public:
	RadioStreamSound(RadioStream *);
	~RadioStreamSound() {};
	unsigned play(const QString &, bool cancel = false, bool loop = false,
		unsigned interval = 0, bool tts = false);
	void stop();
	unsigned soundId();
	bool hasSoundId(unsigned);
	bool removeId(unsigned);

    private slots:
	void slotStreamPaused(bool);

    private:
	friend class RadioStreamSoundThread;
#ifdef WITH_TTS
	static int espeakCallback(short *wav, int n_samples, espeak_EVENT *ev);
#endif
	RadioStreamSoundThread thread;
	RadioStream *stream;
	QList<RadioStreamSoundItem> fileList;
	bool playing;
	bool _playing;
	bool paused;
	QMutex *mutex;
};

#endif
