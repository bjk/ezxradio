/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __SHOUTCAST_H__
#define __SHOUTCAST_H__

#include "stream.h"

#define ShoutCastErrorEvent StreamEventMax+100

class ShoutCastGenreItem
{
    public:
	ShoutCastGenreItem(const QString &title, const QString &genre,
		const QString &mimeType, unsigned long id, unsigned bitRate,
		const QString &playing, unsigned listeners, const QString &url)
	{
	    _title = title;
	    _genre = genre;
	    _mimeType = mimeType;
	    _playing = playing;
	    _id = id;
	    _listeners = listeners;
	    _bitRate = bitRate;
	    _url = url;
	};

	const QString &url()
	{
	    return _url;
	};

	const QString &title()
	{
	    return _title;
	};

	const QString &genre()
	{
	    return _genre;
	};

	const QString &mimeType()
	{
	    return _mimeType;
	};

	const QString &playing()
	{
	    return _playing;
	};

	unsigned long id()
	{
	    return _id;
	};

	unsigned bitRate()
	{
	    return _bitRate;
	};

	unsigned listeners()
	{
	    return _listeners;
	};

    private:
	QString _title;
	QString _genre;
	QString _mimeType;
	QString _playing;
	QString _url;
	unsigned long _id;
	unsigned _bitRate;
	unsigned _listeners;
};

class ShoutCastGenreItemList : public QList<ShoutCastGenreItem>
{
    public:
	ShoutCastGenreItemList() {};
};

class ShoutCastGenre;
class ShoutCast : public QObject
{
    Q_OBJECT

    public:
	ShoutCast(QObject *, RadioStream *);
	~ShoutCast();

	/* Refresh the genre index. */
	void refreshGenres();

	/* Stop the refresh of the genre index. */
	void cancelRefreshGenres();

	/* Fetch the specified genre. */
	bool fetchGenre(ShoutCastGenre *);

	/* Cancel the fetch of the specified genre index. */
	bool unfetchGenre(ShoutCastGenre *);

	/* Cancel all active fetches. */
	void unfetchAll();

	/* Returns the list of genres. */
	const QList<ShoutCastGenre> genres();

	/* For genre index caching. It appends each line in the specified file
	 * to the genre list. */
	bool parseGenreIndexFile(const QString &filename);

	/* Find the first genre in the list whose data pointer matches the
	 * parameter. */
	ShoutCastGenre *findGenre(void *data);

	/* Search shoutcast using the specified string. */
	bool search(QString &str);

	/* For use with the signals. */
	enum ShoutCastState {
	    Init, // Initial call. There may be items in the genreList.
	    More, // Continueing the transfer; genreList has been updated.
	    Cancel, // The transfer was canceled.
	    Error, // The transfer was canceled by an error.
	    Done // The transfer has been completed.
	};

    signals:
	/* Emitted when fetching the genre index. */
	void shoutCastGenres(QList<ShoutCastGenre>, ShoutCast::ShoutCastState);

	/* Emitted during the transfer of a genre. */
	void shoutCastFetch(ShoutCastGenre *, ShoutCast::ShoutCastState);

    private slots:
	void slotStreamFetch(unsigned, const QByteArray *);

    private:
	bool parseGenreIndex(const QByteArray *);
	bool parseGenre(ShoutCastGenre *, const QByteArray *);
	bool parseStation(ShoutCastGenre *, const QString &);
	void postError(ShoutCastGenre *, const QString error);
	void resetGenre(ShoutCastGenre *);

	QObject *parent;
	RadioStream *stream;
	QList<ShoutCastGenre> genreList;
	unsigned cacheId;
	bool cacheIsValid;
	QString genreAttic;
	size_t total;
	ShoutCastGenre *searchGenre;
	QString searchQuery;
	QMutex *mutex;
};

class ShoutCastGenre
{
    public:
	ShoutCastGenre(QString name)
	{
	    _state = ShoutCast::Init;
	    _name = name;
	    _fetchId = 0;
	    items.setAutoDelete(true);
	    tunein = QString::null;
	    attic = QString::null;
	    valid = false;
	    total = 0;
	    _data = NULL;
	};

	/* The name or title of this genre. */
	const QString &name()
	{
	    return _name;
	};

	/* The transfer ID when retrieving genre items from the server. */
	unsigned fetchId()
	{
	    return _fetchId;
	}

	/* User data. It's up to you to free it. */
	void setData(void *data)
	{
	    _data = data;
	};

	void *data()
	{
	    return _data;
	};

	/* Call this to cancel the fetch. */
	void cancel()
	{
	    _state = ShoutCast::Cancel;
	};

	ShoutCast::ShoutCastState state()
	{
	    return _state;
	};

	/* The items in this genre. */
	ShoutCastGenreItemList items;

    private:
	friend class ShoutCast;

	void setFetchId(unsigned id)
	{
	    _fetchId = id;
	};

	void setState(ShoutCast::ShoutCastState s)
	{
	    _state = s;
	};

	unsigned _fetchId;
	QString _name;
	bool valid;
	QString tunein;
	QString attic;
	size_t total;
	void *_data;
	ShoutCast::ShoutCastState _state;
};

class ShoutCastError
{
    public:
	ShoutCastError(ShoutCastGenre *g, QString str)
	{
	    _genre = g;
	    _error = str;
	};

	/* The error string. */
	const QString &error()
	{
	    return _error;
	};

	/* The genre index that this error came from. Returns NULL if
	 * refreshing the genre list. */
	ShoutCastGenre *genre()
	{
	    return _genre;
	};

    private:
	ShoutCastGenre *_genre;
	QString _error;
};

#endif
