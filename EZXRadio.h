/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef  __EZXRADIO_H__
#define __EZXRADIO_H__

#include <ZMainWidget.h>
#include <ZMultiLineEdit.h>
#include <ZDataSessionManager.h>
#include <ezxutilcst.h>
#include <ezxutildialog.h>
#include <qlabel.h>
#include <qslider.h>
#include <qlist.h>
#include <qlistbox.h>
#include <qcombobox.h>
#include "EZXRadioMarquee.h"
#include "EZXRadioWidgets.h"
#include "stream.h"
#include "shoutCast.h"
#include "filebrowser.h"
#include "EZXRadioConfig.h"

#define VERSION	"0.4"

#define EZXRadioLink		StreamEventMax+1
#define EZXRadioSort		StreamEventMax+2
#define EZXRadioShoutCast	StreamEventMax+3
#define EZXRadioTag		StreamEventMax+4

extern "C" int UTIL_GetPhoneInCall(void);
extern "C" int UTIL_GetIncomingCallStatus(void);
extern "C" int UTIL_GetPhoneCallIconIndex(void);
extern "C" int UTIL_GetPhoneApplicationStatus(void);
extern "C" int UTIL_GetPhoneSystemStatus(void);
extern "C" void UTIL_SetUSBChargingStatus(int b);

class EZXRadio : public ZMainWidget
{
    Q_OBJECT

    public:
	EZXRadio(int argc, char **argv);

    private slots:
	void slotAutoPlayStart();
	void slotStartStopStream(bool doStop = false);
	void slotOpenURL(void);
	void slotOpenFile(void);
	void slotOpenFolder(void);
	void slotPrefsInterface(void);
	void slotPrefsNetwork(void);
	void slotPrefsSound(void);
	void slotPrefsAdvanced(void);
	void slotPrefsShoutCast(void);
	void slotAboutDialog(void);
	void slotAdjustVolume(int value);
	void slotLinkConnected(int id, ZLinkInfo &info);
	void slotLinkOpenFailed(int id, unsigned short a, unsigned short b);
	void slotLinkBroken(int id, ZLinkInfo &info);
	void slotRecord(bool);
	void slotMute(bool);
	void slotShuffle(bool);
	void slotScan(bool);
	void slotCyclePlayList(QButton::ToggleState);
	void slotAutoPlay(QButton::ToggleState);
	void slotRecordOneFile(QButton::ToggleState);
	void slotMarquee(QButton::ToggleState);
	void slotSkipBad(QButton::ToggleState);
	void slotFillOnEmpty(QButton::ToggleState);
	void slotQuit(void);
	void slotStreamConnectTimeoutChanged(int);
	void slotInbufSizeChanged(int);
	void slotOutbufSizeChanged(int);
	void slotRecordBufSizeChanged(int);
	void slotPrefsInterfaceFinalize(void);
	void slotPrefsInterfaceCancel(void);
	void slotPrefsNetworkFinalize(void);
	void slotPrefsNetworkCancel(void);
	void slotPrefsSoundFinalize(void);
	void slotPrefsSoundCancel(void);
	void slotPrefsAdvancedFinalize(void);
	void slotPrefsAdvancedCancel(void);
	void slotPrefsShoutCastFinalize(void);
	void slotPrefsShoutCastCancel(void);
	void slotKbStateChanged(bool);
	void slotSelectRecordDirectory(void);
	void slotCallHandler(void);
	void slotRemovePlayListItem(void);
	void slotDeletePlayListFile(void);
	void slotAppendToPlayList();
	void slotSavePlayList();
	void slotSaveAsPlayList();
	void slotSelectSaveDirectory();
	void slotSaveTextChanged();
	void slotSelectEmptyBufferSoundFile();
	void slotSelectErrorSoundFile();
	void slotSelectScanSoundFile();
	void slotSelectRepeatSoundFile();
	void slotSelectRepeatAllSoundFile();
	void slotSelectEofSoundFile();
	void slotSelectConnectingSoundFile();
	void slotSelectRetrySoundFile();
	void slotMarqueeMousePressEvent(EZXRadioMarquee *);
	void slotMarqueeMouseReleaseEvent(EZXRadioMarquee *);
	void slotMarqueeMouseDoubleClickEvent(EZXRadioMarquee *);
	void slotStreamStop();
	void slotStreamStart();
	void slotStreamPaused(bool);
	void slotPaused(bool);
	void slotGenres();
	void slotRefreshGenres();
	void slotOpenDoc(const QString &);
	void slotScanIntervalChanged(int);
	void slotDecodeWaitChanged(int);
	void slotVolumeChanged(int);
	void slotReadIntervalChanged(int);
	void slotDecodeIntervalChanged(int);
	void slotBufferEmptyLoopChanged(QButton::ToggleState);
	void slotBufferEmptyIntervalChanged(int);
	void slotStreamBufferEmpty(bool);
	void slotSelectNetworkProfile();
	void slotLinkDisconnect();
	void slotLinkConnect();
	void slotFlushFillBuffer();
	void slotMarqueeIntervalChanged(int);
	void slotMarqueeSleepIntervalChanged(int);
	void slotPlaylistJumpChanged(int);
	void slotConnectingSoundTimer();
	void slotConnectingLoopChanged(QButton::ToggleState);
	void slotConnectingIntervalChanged(int);
	void slotSortTagTimer();
	void slotKeyModTimer();
	void slotShoutCastGenres(QList<ShoutCastGenre>, ShoutCast::ShoutCastState);
	void slotShoutCastFetch(ShoutCastGenre *, ShoutCast::ShoutCastState);
	void slotShoutCastBlink(bool);
	void slotRefreshGenre();
	void slotShoutCastSearch();
	void slotShoutCastSearchResults();
	void shoutCastSearchSelected(int);
	void slotSearchTitle();
	void slotSearchAlbum();
	void slotSearchArtist();
	void slotSearchUrl();
	void slotSearchId();
	void slotSearchStreamTitle();
	void slotSoundVolumeChanged(int);
	void slotShoutCastRetriesChanged(int);
	void slotShoutCastMaxItemsChanged(int);
	void slotClearShoutCastSearches();
	void slotPlayListSingle(QButton::ToggleState);
	void slotOpenFavorites();
	void slotAppendToFavorites();
	void slotSaveAsFavorites();
	void slotKeyModBlink(bool);
	void slotStreamRetriesChanged(int);
	void slotToggleFavorites(QButton::ToggleState);
	void slotUseTTSChanged(QButton::ToggleState);

    private:
	friend class EZXRadioConfig;
	bool openURLFinalize(QString, bool);
	QString openFileCommon(QString, QString, int, bool *, QString = 0);
	void showError(QString);
	void resetMarquee(QButton::ToggleState);
	void doPlayListShuffle(bool scanMode = false);
	void doSetPlayList(RadioPlayList *, bool rm = true);
	void updateStatusLabel();
	void cyclePlayListTag();
	void setWidgetColor(QWidget *, QColor);
	void setWidgetFontSize(QWidget *, int);
	bool doLinkConnect(bool force = false);
	bool startStream(void);
	void keyPressEvent(QKeyEvent *e);
	void customEvent(QCustomEvent *);
	void doPlayListNext(bool eof, bool scanMode = false);
	void doPlayListPrev(bool eof = false);
	void focusInEvent(QFocusEvent *);
	void focusOutEvent(QFocusEvent *);
	void selectLEFile(ZMultiLineEdit *);
	void recursiveAdd(RadioPlayList *, const QString &, bool);
	QString playListSortString();
	QString playListTagString();
	static int openDsp(void *);
	static int closeDsp(void *);
	static int setDspVolume(void *, int);
	static int dspVolume(void *);
	static int setDspFormat(void *, int, long, int);
	static int syncDsp(void *, bool);
	static size_t writeDsp(void *, void *, size_t);
	static int setDspFormats(void *, decodeModuleFormats *, size_t);
	bool updateShoutCastGenre(RadioPlayList * = NULL);
	bool shoutCastRetry(ShoutCastGenre *);
	QString lineEdit(QString, QString &);
	void searchCommon(QString, QString &, RadioStream::SearchType);
	void shoutCastConnectFinalize(int which = -1);
	void resetShoutCastData(ShoutCastGenre *);
	void resetPlayListItemData();
	void setCurrentGenre(unsigned, bool = false, bool = false);
	ShoutCastGenre *findGenrePlayList(RadioPlayList *);
	void updateGenrePlayListIndex();
	QString proxyTypeToString(RadioStream::ProxyType);
	RadioStream::ProxyType proxyTypeToEnum(const QString &);
#ifdef WITH_TTS
	void playTitle(bool force = false);
#endif

	enum  {
	    ShoutCastSearchCmd, ShoutCastGenreCmd, ShoutCastGenresCmd
	};

	EZXRadioConfig *config;
	RadioStream *stream;
	EZXRadioRepeat *repeatB;
	EZXRadioBlink *recordB;
	EZXRadioShuffle *shuffleB;
	EZXRadioBlink *scanB;
	EZXRadioBlink *pausedL;
	EZXRadioBlink *shoutCastL;
	EZXRadioBlink *keyModL;
	unsigned scanInterval;
	QPopupMenu *cstpopup;
	UTIL_CST *cst;
	EZXRadioMarquee *title_l;
	QLabel *playing_l;
	QLabel *format_l;
	EZXRadioMarquee *info_l;
	QLabel *info2_l;
	QLabel *statusTitle_l;
	EZXRadioMarquee *status_l;
	QSlider *volume_s;
	EZXRadioVolume *volumeL;
	int defaultVolume;
	int soundVolume;
	ZDataSessionManager *sessionMgr;
	int linkId;
	QFrame *prefsIfaceDialog;
	QFrame *prefsIfaceCst;
	QFrame *prefsNetworkDialog;
	QFrame *prefsNetworkCst;
	QFrame *prefsSoundDialog;
	QFrame *prefsSoundCst;
	QFrame *prefsAdvancedDialog;
	QFrame *prefsAdvancedCst;
	QFrame *prefsShoutCastDialog;
	QFrame *prefsShoutCastCst;
	QString openUrlLast;
	FileBrowser *fileBrowser;
	QString lastDir;
	QComboBox *proxyTypeCB;
	ZMultiLineEdit *proxyLE;
	ZMultiLineEdit *proxyPortsLE;
	ZMultiLineEdit *proxyUsernameLE;
	ZMultiLineEdit *proxyPasswordLE;
	ZMultiLineEdit *recordDirLE;
	ZMultiLineEdit *emptyBufferSoundFileLE;
	QString rcFile;
	bool recordOneFile;
	bool pausedInCall;
	bool inCall;
	bool enableMarquee;
	int backLightFd;
	bool autoPlay;
	bool powerSaving;
	QString recordDirectory;
	int streamState;
	RadioPlayListItem *lastPlayListItem;
	int headerFontSize;
	QString headerColor;
	int tagFontSize;
	QString tagColor;
	int formatFontSize;
	QString formatColor;
	int statusFontSize;
	QString statusColor;
	int defaultFontSize;
	QString defaultColor;
	bool browseMode;
	int recall;
	bool cyclePlayList;
	int sortType;
	bool reverseSort;
	QTimer *sortTagTimer;
	QTimer *keyModTimer;
	bool keyMod;
	QString bufferEmptySound;
	bool bufferEmptySoundLoop;
	int bufferEmptySoundInterval;
	QString streamErrorSound;
	ZMultiLineEdit *errorSoundFileLE;
	QString sessionLink;
	bool startup;
	QString networkProfile;
	ZPushButton *networkProfileB;
	QString scanSound;
	ZMultiLineEdit *scanSoundFileLE;
	int marqueeInterval;
	int marqueeSleepInterval;
	int playListJump;
	QString repeatSound;
	ZMultiLineEdit *repeatSoundFileLE;
	QString repeatAllSound;
	ZMultiLineEdit *repeatAllSoundFileLE;
	QString eofSound;
	ZMultiLineEdit *eofSoundFileLE;
	QString connectingSound;
	ZMultiLineEdit *connectingSoundFileLE;
	bool connectingSoundLoop;
	int connectingSoundInterval;
	unsigned connectingSoundId;
	QTimer *connectingSoundTimer;
	QString retrySound;
	ZMultiLineEdit *retrySoundFileLE;
	int dspFd;
	int mixerFd;
	ShoutCast *sc;
	UTIL_Dialog *genreDialog;
	QString searchQuery;
	QString searchShoutCastQuery;
	UTIL_Dialog *lineEditDialog;
	ShoutCastGenre *currentGenre;
	int shoutCastCommand;
	unsigned shoutCastRetries;
	unsigned shoutCastMaxItems;
	bool playListSingle;
	QString favoritesFile;
	unsigned streamRetries;
	QString saveDirectory;
	QLabel *saveDirectoryL;
	ZMultiLineEdit *saveFilenameLE;
	ZPushButton *saveConfirmB;
	bool wantRefreshGenres;
	EZXRadioListBox *shoutCastSearchesLB;
	QComboBox *shoutCastSearchesCB;
	bool favorites;
#ifdef WITH_TTS
	bool useTTS;
#endif
};

class ShoutCastData
{
    friend class EZXRadio;

    ShoutCastData(unsigned m = 0)
    {
	maxItems = m;
	retry = 0;
	selected = NULL;
	playList = NULL;
	playListIndex = 0;
	state = ShoutCast::Init;
    };

    unsigned retry;
    unsigned maxItems;
    RadioPlayListItem *selected;
    RadioPlayList *playList;
    unsigned playListIndex;
    ShoutCast::ShoutCastState state;
};

class EZXRadioPlayListItemData : public RadioPlayListItemData
{
    public:
	EZXRadioPlayListItemData() {
	    _retry = 0;
	};

	~EZXRadioPlayListItemData()
	{
	};

	unsigned retry()
	{
	    ++_retry;
	    return _retry;
	};

	void resetRetries()
	{
	    _retry = 0;
	};

    private:
	unsigned _retry;
};

#endif
