/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <linux/soundcard.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif
#include <mpg123.h>
#include "module.h"

#ifndef AFMT_S16_NE
# ifdef OSS_BIG_ENDIAN
#  define AFMT_S16_NE AFMT_S16_BE
# else
#  define AFMT_S16_NE AFMT_S16_LE
# endif
#endif

#ifndef AFMT_U16_NE
# ifdef OSS_BIG_ENDIAN
#  define AFMT_U16_NE AFMT_U16_BE
# else
#  define AFMT_U16_NE AFMT_U16_LE
# endif
#endif

#define BUFSIZE		16*1024
#define FILL_LIMIT	256*1024

const char *content_types[] = {
    "audio/mpeg",
    "audio/x-mp3",
    NULL
};

struct mpg_handle_s {
    mpg123_handle *m;
    decodeModuleFrameInfo info;
    struct mpg123_frameinfo fi;
    int n;
    struct mpg_handle_s *next;
};

static int verbose;
static int nHandles;
static struct mpg_handle_s *handles;

static int updateInfo(struct mpg_handle_s *h);
void finish(int index);

static struct mpg_handle_s *findHandle(int n)
{
    struct mpg_handle_s *p;

    for (p = handles; p; p = p->next) {
	if (p->n == n)
	    break;
    }

    return p;
}

int init(int verbosity, const char **types[])
{
    mpg123_init();
    verbose = verbosity;
    *types = content_types;
    return DEC_MOD_OK;
}

int valid(int index, const unsigned char *data, size_t len)
{
    struct mpg_handle_s *h = findHandle(index);
    int done;
    int n;

    if (!h)
	return DEC_MOD_ERR;

    n = mpg123_decode(h->m, data, len, NULL, 0, &done);

    if (n == MPG123_NEW_FORMAT)
	updateInfo(h);
    else if (n == MPG123_NEED_MORE || n == MPG123_OK)
	return DEC_MOD_NEED_MORE;

    return n == MPG123_NEW_FORMAT ? DEC_MOD_NEWINFO : DEC_MOD_ERR;
}

static int addHandle(struct mpg_handle_s *h)
{
    struct mpg_handle_s *p;

    h->n = nHandles;

    for (p = handles; p; p = p->next) {
	if (!p->next) {
	    p->next = h;
	    break;
	}
    }

    if (!p)
	handles = h;

    return nHandles++;
}

int openFeed(const decodeModuleFormats *formats, int fsize)
{
    struct mpg_handle_s *h = (struct mpg_handle_s *)malloc(sizeof(struct mpg_handle_s));
    int i;

    if (!h)
	return DEC_MOD_ERR;

    memset(h, 0, sizeof(struct mpg_handle_s));
    h->m = mpg123_new(NULL, NULL);
    mpg123_param(h->m, MPG123_VERBOSE, verbose, 0);

    if (fsize) {
	mpg123_format_none(h->m);

	for (i = 0; i < fsize; i++) {
	    int c, f = 0;

	    switch (formats[i].channels) {
		case 1:
		    c = MPG123_MONO;
		    break;
		case 2:
		    c = MPG123_STEREO;
		    break;
		default:
		    c = MPG123_MONO|MPG123_STEREO;
		    break;
	    }

	    switch (formats[i].format) {
		case AFMT_S16_NE:
		    f = MPG123_ENC_SIGNED_16;
		    break;
		case AFMT_U8:
		    f = MPG123_ENC_UNSIGNED_8;
		    break;
		case AFMT_S8:
		    f = MPG123_ENC_SIGNED_8;
		    break;
		case AFMT_MU_LAW:
		    f = MPG123_ENC_ULAW_8;
		    break;
		case AFMT_A_LAW:
		    f = MPG123_ENC_ALAW_8;
		    break;
		case AFMT_U16_NE:
		    f = MPG123_ENC_UNSIGNED_16;
		    break;
	    }

	    mpg123_format(h->m, formats[i].rate, c, f);
	}
    }

    if (mpg123_open_feed(h->m)) {
	mpg123_close(h->m);
	mpg123_delete(h->m);
	return DEC_MOD_ERR;
    }

    return addHandle(h);
}

int scan(const char *filename)
{
    int fd = open(filename, O_RDONLY);
    int index = -1;
    unsigned char *buf = NULL;
    int rc = DEC_MOD_ERR;
    size_t total = 0;
    struct mpg_handle_s *h;
    struct stat st;

    if (fd == -1)
	return DEC_MOD_ERR;

    buf = (unsigned char *)malloc(BUFSIZE);

    if (!buf)
	goto done;

    index = openFeed(NULL, 0);
    h = findHandle(index);
    stat(filename, &st);
    mpg123_set_filesize(h->m, st.st_size);

again:
    do {
	size_t len = read(fd, buf, BUFSIZE);

	if (len <= 0) {
	    rc = MPG123_ERR;
	    goto done;
	}

	rc = valid(index, buf, len);
	total += len;
    } while (rc == DEC_MOD_NEED_MORE && total <= FILL_LIMIT);

    if (rc == DEC_MOD_NEWINFO && (!h->info.infoTypes & DEC_INFO_META))
	goto again;

done:
    close(fd);

    if (buf)
	free(buf);

    if (rc != DEC_MOD_NEWINFO) {
	finish(index);
	return DEC_MOD_ERR;
    }

    return index;
}

#ifndef AFMT_U16_NE
# ifdef OSS_BIG_ENDIAN
#  define AFMT_U16_NE AFMT_U16_BE
# else
#  define AFMT_U16_NE AFMT_U16_LE
# endif
#endif

static int updateInfo(struct mpg_handle_s *h)
{
    int fmts = 0;
    int n;
    char *v = NULL;
    char *l = NULL;
    char *vbr = NULL;

    memset(&h->info, 0, sizeof(h->info));
    n = mpg123_getformat(h->m, &h->info.frameRate, &h->info.channels,
	    &h->info.format);

    if (!n) {
	switch (h->info.format) {
	    case MPG123_ENC_SIGNED_16:
	    default:
		fmts = AFMT_S16_NE;
		break;
	    case MPG123_ENC_UNSIGNED_8:
		fmts = AFMT_U8;
		break;
	    case MPG123_ENC_SIGNED_8:
		fmts = AFMT_S8;
		break;
	    case MPG123_ENC_ULAW_8:
		fmts = AFMT_MU_LAW;
		break;
	    case MPG123_ENC_ALAW_8:
		fmts = AFMT_A_LAW;
		break;
	    case MPG123_ENC_UNSIGNED_16:
		fmts = AFMT_U16_NE;
		break;
	}

	h->info.format = fmts;
	h->info.infoTypes |= DEC_INFO_FORMAT;
    }

    n = mpg123_info(h->m, &h->fi);

    if (!n) {
	switch (h->fi.version) {
	    case 0:
		v = "1.0";
		break;
	    case 1:
		v = "2.0";
		break;
	    case 2:
		v = "2.5";
		break;
	    default:
		v = "x.x";
		break;
	}

	switch (h->fi.layer) {
	    case 0:
		l = "unknown";
		break;
	    case 1:
		l = "I";
		break;
	    case 2:
		l = "II";
		break;
	    case 3:
		l = "III";
		break;
	}

	switch (h->fi.vbr) {
	    case 0:
		vbr = "";
		break;
	    case 1:
		vbr = " VBR";
		break;
	    case 2:
		vbr = " ABR";
		break;
	    default:
		vbr = "";
		break;
	}

	snprintf(h->info.version, sizeof(h->info.version),
		"MPEG %s, Layer %s%s", v, l, vbr);
	h->info.frameRate = h->fi.rate;
	h->info.mode = h->fi.mode;
	h->info.bitRate = h->fi.bitrate;
	h->info.infoTypes |= DEC_INFO_FORMAT;
    }

    n = mpg123_meta_check(h->m);

    if (n & MPG123_NEW_ID3) {
	mpg123_id3v1 *v1 = NULL;
	mpg123_id3v2 *v2 = NULL;

	n = mpg123_id3(h->m, &v1, &v2);

	if (v1 && v1->title[0])
	    h->info.title = v1->title;

	if (v1 && v1->artist[0])
	    h->info.artist = v1->artist;

	if (v1 && v1->album[0])
	    h->info.album = v1->album;

	if (v2 && v2->title && v2->title->fill)
	    h->info.title = v2->title->p;

	if (v2 && v2->artist && v2->artist->fill)
	    h->info.artist = v2->artist->p;

	if (v2 && v2->album && v2->album->fill)
	    h->info.album = v2->album->p;
    }

    if (n & (MPG123_NEW_ICY|MPG123_NEW_ID3))
	h->info.infoTypes |= DEC_INFO_META;

    return 0;
}

int feed(int index, const void *data, size_t len)
{
    struct mpg_handle_s *h = findHandle(index);

    if (!h)
	return DEC_MOD_ERR;

    return mpg123_feed(h->m, data, len) != MPG123_OK ? DEC_MOD_ERR : DEC_MOD_OK;
}

int decodeFrame(int index, void **data, size_t *len)
{
    int n;
    struct mpg_handle_s *h = findHandle(index);

    if (!h)
	return DEC_MOD_ERR;

    n = mpg123_decode_frame(h->m, NULL, (unsigned char **)data, len);

    switch (n) {
	case MPG123_NEW_FORMAT:
	    updateInfo(h);
	    return DEC_MOD_NEWINFO;
	case MPG123_DONE:
	case MPG123_OK:
	    return DEC_MOD_OK;
	case MPG123_NEED_MORE:
	    return DEC_MOD_NEED_MORE;
	default:
	    break;
    }

    return DEC_MOD_ERR;
}

void finish(int index)
{
    struct mpg_handle_s *h = findHandle(index), *p, *last;

    if (!h)
	return;

    mpg123_close(h->m);
    mpg123_delete(h->m);

    for (last = p = handles; p; p = p->next) {
	if (p->n == h->n) {
	    if (p == handles && p->next)
		handles = p->next;
	    else if (p == handles && !p->next)
		handles = NULL;
	    else
		last->next = p->next;

	    free(p);
	    break;
	}

	last = p;
    }
}

decodeModuleFrameInfo *getInfo(int index)
{
    struct mpg_handle_s *h = findHandle(index);
    return !h ? NULL : &h->info;
}
