/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __EZXRADIOMARQUEE__
#define __EZXRADIOMARQUEE__

#include <ZApplication.h>
#include <qlabel.h>
#include <qtimer.h>
#include <qlist.h>

class MarqueeString
{
    public:
	MarqueeString(const QString &str)
	{
	    s = str;
	};

	QString text(void)
	{
	    return s;
	};

    private:
	QString s;
};

class EZXRadioMarquee : public QLabel
{
    Q_OBJECT

    public:
	EZXRadioMarquee(const QString &str, QWidget *parent,
		int interval = 150, int sInterval = 1000);
	~EZXRadioMarquee();
	void setInterval(int value);
	int interval(void);
	void setSleepInterval(int value);
	int sleepInterval(void);
	void start(void);
	void stop(void);
	bool isActive();
	void addText(const QString &str, bool set = false);
	void resetText(void);
	void next(void);
	void prev(void);
	int at(void);
	bool at(int);
	void setText(const QString &str);

    private slots:
	void updateLabel(void);
	void sleep(void);
	void startTimer(void);

    protected:
	virtual void mousePressEvent(QMouseEvent*);
	virtual void mouseReleaseEvent(QMouseEvent*);
	virtual void mouseDoubleClickEvent(QMouseEvent*);

    signals:
	void MarqueeMousePressEvent(EZXRadioMarquee *);
	void MarqueeMouseReleaseEvent(EZXRadioMarquee *);
	void MarqueeMouseDoubleClickEvent(EZXRadioMarquee *);

    private:
	QString orig;
	QTimer *timer;
	int _interval;
	int _sleepInterval;
	unsigned pos;
	typedef QList<MarqueeString> MarqueeList;
	MarqueeList list;
};

#endif
