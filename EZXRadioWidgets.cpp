/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <qdir.h>
#include <ezxres.h>
#include "EZXRadioWidgets.h"

EZXRadioListBox::EZXRadioListBox(int up, int down, int select)
    : QListBox()
{
    keyUp = up;
    keyDown = down;
    keySelect = select;
}

void EZXRadioListBox::setKeys(int up, int down, int select)
{
    keyUp = up;
    keyDown = down;
    keySelect = select;
}

void EZXRadioListBox::keyPressEvent(QKeyEvent *k)
{
    if (currentItem() == 0 && k->key() == keyUp)
	setSelected(count()-1, true);
    else if (currentItem() == (int)count()-1 && k->key() == keyDown)
	setSelected(0, true);
    else if (k->key() == keyUp)
	setSelected(currentItem()-1, true);
    else if (k->key() == keyDown)
	setSelected(currentItem()+1, true);
    else if (k->key() == keySelect)
	emit selected(currentItem());
    else
	k->ignore();

    centerCurrentItem();
}

EZXRadioVolume::EZXRadioVolume(QWidget *parent, int v) :
    QLabel(QString("%1").arg(v), parent)
{
    _isMuted = false;
    volume = v;
}

void EZXRadioVolume::setVolume(int v)
{
    v = v > 100 ? 100 : v;
    v = v < 0 ? 0 : v;
    volume = v;

    if (!_isMuted)
	setText(QString("%1").arg(volume));
}

void EZXRadioVolume::doMute(bool v)
{
    QString baseDir = QDir::homeDirPath() + "/.ezxradio";

    _isMuted = v;

    if (_isMuted)
	setPixmap(QPixmap(baseDir + "/mute.xpm"));
    else
	setText(QString("%1").arg(volume));

    emit muteChanged(_isMuted);
}

void EZXRadioVolume::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    setMuted(!_isMuted);
}

void EZXRadioVolume::setMuted(bool v)
{
    doMute(v);
}

bool EZXRadioVolume::isMuted()
{
    return _isMuted;
}

EZXRadioRepeat::EZXRadioRepeat(QWidget *parent, int t) : QLabel("", parent)
{
    setType(t);
};

void EZXRadioRepeat::setType(int t)
{
    QString baseDir = QDir::homeDirPath() + "/.ezxradio";

    _type = t;

    switch (_type) {
	case None:
	    setPixmap(QPixmap(baseDir + "/repeatNone.xpm"));
	    break;
	case Current:
	    setPixmap(QPixmap(baseDir + "/repeatCurrent.xpm"));
	    break;
	case All:
	    setPixmap(QPixmap(baseDir + "/repeat.xpm"));
	    break;
	default:
	    break;
    }

    emit repeatChanged(_type);
};

int EZXRadioRepeat::type(void)
{
    return _type;
}

void EZXRadioRepeat::next()
{
    setType(_type+1 == MAX ? 0 : _type+1);
};

void EZXRadioRepeat::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    next();
}

EZXRadioBlink::EZXRadioBlink(QWidget *parent, const QString &p, unsigned i,
	bool image, bool doEmit) : QLabel("", parent)
{
    baseDir = QDir::homeDirPath() + "/.ezxradio";
    timer = new QTimer(this);
    pixmap = p;
    _interval = i;
    _isPixmap = image;
    _emit = doEmit;
    connect(timer, SIGNAL(timeout()), this, SLOT(slotTimer()));
    which = active = false;
    changePixmap();
}

void EZXRadioBlink::slotTimer()
{
    changePixmap();

    if (_emit)
	emit blinkChanged(timer->isActive());
}

unsigned EZXRadioBlink::interval()
{
    return _interval;
}

void EZXRadioBlink::setInterval(unsigned i)
{
    _interval = i;
}

void EZXRadioBlink::changePixmap()
{
    if (!which) {
	if (isPixmap())
	    setPixmap(QPixmap(baseDir + "/" + pixmap));
	else
	    QLabel::setText(pixmap);
    }
    else
	QLabel::setText("");

    which = !which;
}

void EZXRadioBlink::stop()
{
    timer->stop();
    which = active = false;
    changePixmap();
}

void EZXRadioBlink::off()
{
    stop();
    QLabel::setText("");
}

bool EZXRadioBlink::isActive()
{
    return active;
}

void EZXRadioBlink::setActive(bool v)
{
    if ((!v && !active) || (active && v))
	return;

    if (active && !v) {
	timer->stop();
	which = true;
    }
    else {
	timer->start(interval());
	which = false;
    }
    
    active = v;
    changePixmap();

    // Failsafe since we cant wait for the timer callback to finish.
    if (!active)
	changePixmap();

    emit blinkChanged(timer->isActive());
}

void EZXRadioBlink::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    setActive(!isActive());
}

bool EZXRadioBlink::isPixmap()
{
    return _isPixmap;
}

void EZXRadioBlink::setText(const QString &str)
{
    if (isPixmap())
	return;

    pixmap = str;
    which = !which;
    changePixmap();
}

EZXRadioShuffle::EZXRadioShuffle(QWidget *parent, bool s) : QLabel("", parent)
{
    baseDir = QDir::homeDirPath() + "/.ezxradio";
    _shuffle = s;
    changePixmap();
}

void EZXRadioShuffle::changePixmap()
{
    if (_shuffle)
	setPixmap(QPixmap(baseDir + "/shuffle.xpm"));
    else
	setPixmap(QPixmap(baseDir + "/noShuffle.xpm"));

    emit shuffleChanged(_shuffle);
}

void EZXRadioShuffle::setShuffled(bool v)
{
    _shuffle = v;
    changePixmap();
}

bool EZXRadioShuffle::isShuffled()
{
    return _shuffle;
}

void EZXRadioShuffle::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    _shuffle = !_shuffle;
    changePixmap();
}

GenreBrowser::GenreBrowser(QWidget *parent, int ku, int kd, int ks) :
    QListBox(parent)
{
    keyUp = ku;
    keyDown = kd;
    keySelect = ks;
    connect(this, SIGNAL(highlighted(int)), this, SLOT(itemHighlighted(int)));
    connect(this, SIGNAL(selected(int)), this, SLOT(itemSelected(int)));
    connect(this, SIGNAL(clicked(QListBoxItem *)), this, SLOT(itemClicked(QListBoxItem *)));
}
    
void GenreBrowser::setGenreList(const QList<ShoutCastGenre> list)
{
    _list = list;
    refreshList();
}

void GenreBrowser::refreshList()
{
    RES_ICON_Reader ir;
    clear();

    for (unsigned i = 0; i < _list.count(); i++) {
	ShoutCastGenre *g = _list.at(i);

	insertItem(ir.getIcon("FMMS_Personal_Folder_S.gif", 1), g->name());
    }

    setSelected(0, true);
}
    
void GenreBrowser::keyPressEvent(QKeyEvent *k)
{
    if (currentItem() == 0 && k->key() == keyUp)
	setSelected(count()-1, true);
    else if (currentItem() == (int)count()-1 && k->key() == keyDown)
	setSelected(0, true);
    else if (k->key() == keyUp)
	setSelected(currentItem()-1, true);
    else if (k->key() == keyDown)
	setSelected(currentItem()+1, true);
#if 0
    else if (k->key() == keySelect)
	emit itemSelected(currentItem());
#endif
    else
	k->ignore();

    centerCurrentItem();
}

void GenreBrowser::slotSetGenres(QList<ShoutCastGenre> list)
{
    _list = list;
    refreshList();
}
