/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __MODULE_H__
#define __MODULE_H__

#ifdef __cplusplus
extern "C" {
#endif

#define DEC_INFO_FORMAT	0x01
#define DEC_INFO_META	0x02

#define DEC_MODE_STEREO		0
#define DEC_MODE_JSTEREO	1
#define DEC_MODE_DUAL		2
#define DEC_MODE_MONO		3

typedef struct {
    char *title;
    char *artist;
    char *album;
    long frameRate;
    size_t frameSize;
    int bitRate;
    int channels;
    int mode;
    int format;
    char version[128];
    int infoTypes;
} decodeModuleFrameInfo;

typedef enum {
    DEC_MOD_ERR = -1,
    DEC_MOD_OK,
    DEC_MOD_NEWINFO,
    DEC_MOD_NEED_MORE
} decodeModuleRc;

typedef struct {
    int format;
    int channels;
    long rate;
} decodeModuleFormats;

typedef struct {
    int (*init)(int verbose, const char **content_types[]);
    int (*valid)(int index, const void *data, size_t len);
    int (*scan)(const char *filename);
    int (*feed)(int index, const void *data, size_t len);
    int (*decodeFrame)(int index, void **data, size_t *len);
    decodeModuleFrameInfo *(*getInfo)(int index);
    int (*openFeed)(const decodeModuleFormats *, int size);
    void (*finish)(int index);

    void *handle;
    decodeModuleFrameInfo *info;
    int index;
} decodeModule;

#ifdef __cplusplus
}
#endif

#endif
