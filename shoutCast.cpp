/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <qfile.h>
#include "shoutCast.h"

#define ShoutCastGenreUrl "http://www.shoutcast.com/sbin/newxml.phtml"
#define ShoutCastBaseUrl "http://www.shoutcast.com"

ShoutCast::ShoutCast(QObject *p, RadioStream *s)
{
    parent = p;
    stream = s;
    connect(stream, SIGNAL(streamFetch(unsigned, const QByteArray *)), this,
	    SLOT(slotStreamFetch(unsigned, const QByteArray *)));
    cacheId = 0;
    cacheIsValid = false;
    genreAttic = QString::null;
    total = 0;
    genreList.setAutoDelete(true);
    searchGenre = new ShoutCastGenre(tr("Search results"));
    searchQuery = QString::null;
    genreList.insert(0, searchGenre);
    mutex = new QMutex(true);
}

ShoutCast::~ShoutCast()
{
    stream->unfetch(cacheId);
}

bool ShoutCast::parseGenreIndexFile(const QString &filename)
{
    QFile f(filename);

    if (!f.open(IO_ReadOnly)) {
	std::cerr << filename.data() << ": could open file for reading.\n";
	return false;
    }

    QTextStream t;
    t.setDevice(&f);
    QString line;
    MUTEX_LOCK(mutex);

    while ((line = t.readLine()) != QString::null) {
	bool dup = false;

	for (unsigned i = 0; i < genreList.count(); i++) {
	    ShoutCastGenre *g = genreList.at(i);

	    if (g->name() == line) {
		dup = true;
		break;
	    }
	}

	if (!dup)
	    genreList.append(new ShoutCastGenre(line));
    }

    MUTEX_UNLOCK(mutex);
    f.close();
    return true;
}

void ShoutCast::refreshGenres()
{
    MUTEX_LOCK(mutex);
    stream->unfetch(cacheId);
    unfetchAll();
    cacheIsValid = false;
    total = 0;
    cacheId = stream->fetch(ShoutCastGenreUrl);
    MUTEX_UNLOCK(mutex);
}

void ShoutCast::postError(ShoutCastGenre *g, const QString str)
{
    QCustomEvent *ev = new QCustomEvent(QEvent::Type(ShoutCastErrorEvent));
    ShoutCastError *e = new ShoutCastError(g, str);
    ev->setData(e);
    QApplication::postEvent(parent, ev);
}

void ShoutCast::slotStreamFetch(unsigned id, const QByteArray *data)
{
    QString e;
    MUTEX_LOCK(mutex);

    if (id == cacheId) {
	if (data) {
	    if (!parseGenreIndex(data)) {
		stream->fetchError(id);
		postError(NULL, tr("Error while fetching genres. Unknown data."));
		emit shoutCastGenres(genreList, Error);
	    }
	}
	else {
	    genreAttic = QString::null;
	    e = QString(stream->fetchError(id));

	    if (!e.isEmpty()) {
		postError(NULL, e);
		emit shoutCastGenres(genreList, Error);
	    }
	    else 
		emit shoutCastGenres(genreList, Done);
	}

	MUTEX_UNLOCK(mutex);
	return;
    }

    for (unsigned i = 0; i < genreList.count(); i++) {
	ShoutCastGenre *g = genreList.at(i);

	if (g->fetchId() == id) {
	    if (data && !parseGenre(g, data)) {
		stream->fetchError(id);
		postError(g, tr("Error while parsing genre data."));
		emit shoutCastFetch(g, Error);
	    }
	    else if (!data) {
		g->attic = QString::null;
		e = QString(stream->fetchError(id));

		if (!e.isEmpty()) {
		    postError(g, e);
		    emit shoutCastFetch(g, Error);
		}
		else
		    emit shoutCastFetch(g, Done);
	    }
	    else {
		ShoutCastState s = g->state();
		emit shoutCastFetch(g, g->state());

		if (s == g->state())
		    g->setState(More);
	    }

	    if (g->state() == Cancel)
		unfetchGenre(g);

	    MUTEX_UNLOCK(mutex);
	    return;
	}
    }

    std::cerr << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl;
    MUTEX_UNLOCK(mutex);
}

const QList<ShoutCastGenre> ShoutCast::genres()
{
    return genreList;
}

void ShoutCast::cancelRefreshGenres()
{
    stream->unfetch(cacheId);
}

bool ShoutCast::parseGenreIndex(const QByteArray *data)
{
    if (!cacheIsValid) {
	if (data->size() >= 6 && !memcmp(data->data(), "<?xml ", 6)) {
	    QString s = data->data();

	    if (s.find("<genrelist>") != -1) {
		cacheIsValid = true;
		genreList.clear();
		resetGenre(searchGenre);
		searchGenre = new ShoutCastGenre(tr("Search results"));
		genreList.insert(0, searchGenre);
		emit shoutCastGenres(genreList, Init);
	    }
	}

	total += data->size();
    }

    if (!cacheIsValid) {
	if (total > 256)
	    return false;

	genreAttic += data->data();
	return true;
    }

    QString s;

    if (genreAttic) {
	s = genreAttic + data->data();
	genreAttic = QString::null;
    }
    else s = data->data();

    int n;

    do {
	n = s.find("name=\"");

	if (n != -1 && s.length() > 6) {
	    int l = s.mid(n+6).find("\"");

	    if (l == -1) {
		genreAttic = s;
		break;
	    }

	    s = s.mid(n+6);
	    genreList.append(new ShoutCastGenre(s.left(l)));
	}
	else {
	    n = -1;
	    genreAttic = s;
	}
    } while (n != -1);

    return true;
}

bool ShoutCast::fetchGenre(ShoutCastGenre *g)
{
    if (!g)
	return false;

    QString url;

    if (g == searchGenre) {
	if (searchQuery.isEmpty())
	    return false;

	QString str = searchQuery;

	for (unsigned i = 0; i < str.length(); i++) {
	    QChar c = str.at(i);

	    if (c == ' ')
		str = str.replace(i, 1, "+");
	}

	url = QString("%1?search=%2").arg(ShoutCastGenreUrl).arg(str);
    }
    else
	url = QString("%1?genre=%2").arg(ShoutCastGenreUrl).arg(g->name());

    resetGenre(g);
    g->setFetchId(stream->fetch(url));
    return true;
}

bool ShoutCast::unfetchGenre(ShoutCastGenre *g)
{
    if (!g || !g->fetchId())
	return false;

    g->setState(Cancel);
    bool rc = stream->unfetch(g->fetchId());
    emit shoutCastFetch(g, g->state());
    return rc;
}

bool ShoutCast::parseStation(ShoutCastGenre *g, const QString &str)
{
    QString title = QString::null;
    QString mime = QString::null;
    unsigned long id = 0;
    unsigned bitrate = 0;
    QString playing = QString::null;
    unsigned listeners = 0;
    bool inQuote = false;
    bool inName = true;
    const char *p;
    QString buf = QString::null;
    QString parsedName = QString::null;
    QString parsedValue = QString::null;
    QString genre = QString::null;

    /* "<station " */
    p = str.data() + 9;

    for (; *p; p++) {
	if (*p == '=' && inName) {
	    inName = false;
	    parsedName = buf.lower();
	    buf = QString::null;
	    continue;
	}

	if ((*p == ' ' || *p == '>') && !inQuote) {
	    parsedValue = buf;
	    inName = true;
	    buf = QString::null;

	    if (!QString::compare(parsedName, "name"))
		title = parsedValue;
	    else if (!QString::compare(parsedName, "mt"))
		mime = parsedValue;
	    else if (!QString::compare(parsedName, "id"))
		id = parsedValue.toInt();
	    else if (!QString::compare(parsedName, "br"))
		bitrate = parsedValue.toUInt();
	    else if (!QString::compare(parsedName, "ct"))
		playing = parsedValue;
	    else if (!QString::compare(parsedName, "genre"))
		genre = parsedValue;
	    else if (!QString::compare(parsedName, "lc"))
		listeners = parsedValue.toULong();

	    continue;
	}

	if (*p == '\"') {
	    inQuote = !inQuote;
	    continue;
	}

	buf.append(*p);
    }

    QString url = QString("%1%2?id=%3").arg(ShoutCastBaseUrl).arg(g->tunein).arg(id);
    g->items.append(new ShoutCastGenreItem(title, genre, mime, id, bitrate,
		playing, listeners, url));
    return true;
}

bool ShoutCast::parseGenre(ShoutCastGenre *genre, const QByteArray *data)
{
    if (!genre->valid) {
	if (data->size() >= 6 && !memcmp(data->data(), "<?xml ", 6)) {
	    QString s = data->data();

	    if (s.find(QRegExp(".*<tunein base=\"[^\"]*\".*")) != -1) {
		genre->valid = true;
		genre->items.clear();
	    }
	}

	genre->total += data->size();
    }

    if (!genre->valid) {
	if (genre->total > 512)
	    return false;

	genre->attic += data->data();
	return true;
    }

    QString s;

    if (genre->attic)
	s = genre->attic + data->data();
    else
	s = data->data();

    int n;
    int l;
    int end;

    if (!genre->tunein) {
	n = s.find("<tunein base=\"");
	s = s.mid(n+14);
	l = s.find("\"");
	genre->tunein = s.left(l);
	s = s.mid(l+3);
    }

    for (;;) {
	end = s.find("/>");

	if (end == -1) {
	    end = s.find("</station>");

	    if (end == -1)
		break;

	    end += 9;
	}
	else
	    end += 2;

	n = s.find("<station ");
	QString t = s.mid(n, end);
	s = s.mid(end);

	if (!parseStation(genre, t))
	    return false;
    }

    genre->attic = s;
    return true;
}

ShoutCastGenre *ShoutCast::findGenre(void *data)
{
    MUTEX_LOCK(mutex);

    for (unsigned i = 0; i < genreList.count(); i++) {
	ShoutCastGenre *g = genreList.at(i);

	if (g->data() && g->data() == data) {
	    MUTEX_UNLOCK(mutex);
	    return g;
	}
    }

    MUTEX_UNLOCK(mutex);
    return NULL;
}

void ShoutCast::unfetchAll()
{
    MUTEX_LOCK(mutex);

    for (unsigned i = 0; i < genreList.count(); i++) {
	ShoutCastGenre *g = genreList.at(i);
	unfetchGenre(g);
    }

    MUTEX_UNLOCK(mutex);
}

void ShoutCast::resetGenre(ShoutCastGenre *g)
{
    stream->unfetch(g->fetchId());
    g->attic = QString::null;
    g->valid = false;
    g->tunein = QString::null;
    g->total = 0;
    g->setState(Init);
}

bool ShoutCast::search(QString &str)
{
    searchQuery = str;
    return fetchGenre(searchGenre);
}
