TEMPLATE	= app
CONFIG		= qt warn_on release thread
HEADERS		= EZXRadio.h EZXRadioMarquee.h stream.h playList.h \
		  filebrowser.h module.h EZXRadioWidgets.h sound.h \
		  EZXRadioConfig.h dsp.h fetch.h shoutCast.h
SOURCES		= EZXRadio.cpp \
		  EZXRadioMarquee.cpp \
		  main.cpp \
		  playList.cpp \
		  stream.cpp \
		  filebrowser.cpp \
		  EZXRadioWidgets.cpp \
		  sound.cpp \
		  EZXRadioConfig.cpp \
		  fetch.cpp \
		  shoutCast.cpp
DEFINES		=
INCLUDEPATH	=
LIBS            = -lcurl -lezxnapi -lezxqtnapi

# Text to speech support. Requires libespeak and its data files.
DEFINES		+= WITH_TTS
LIBS		+= -lespeak
