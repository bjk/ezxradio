/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include "fetch.h"

RadioStreamFetch::RadioStreamFetch(RadioStream *p, const QString &url)
{
    static unsigned __id;

    _id = ++__id;
    _canceled = false;
    parent = p;
    _handle = curl_easy_init();
    curl_easy_setopt(_handle, CURLOPT_WRITEFUNCTION, curlWriteCb);
    curl_easy_setopt(_handle, CURLOPT_WRITEDATA, this);
    parent->addCurlHandle(_handle, url);
    std::cerr << "FETCH INIT id=" << std::dec << _id << "\n";
}

void RadioStreamFetch::cancel()
{
    std::cerr << "FETCH CANCEL id=" << std::dec << _id << "\n";
    _canceled = true;
}

bool RadioStreamFetch::canceled()
{
    return _canceled;
}

size_t RadioStreamFetch::curlWriteCb(void *ptr, size_t size, size_t nmemb,
	void *data)
{
    RadioStreamFetch *f = (RadioStreamFetch *)data;
    size_t len = size*nmemb;

    if (f->canceled())
	return len;

    MUTEX_LOCK(f->parent->fetchMutex);
    QByteArray b;
    b.setRawData((const char *)ptr, len);
    emit f->parent->streamFetch(f->id(), &b);
    b.resetRawData((const char *)ptr, len);
    MUTEX_UNLOCK(f->parent->fetchMutex);
    return len;
}

RadioStreamFetch::~RadioStreamFetch()
{
    MUTEX_LOCK(parent->fetchMutex);
    std::cerr << "FETCH REMOVE id=" << std::dec << _id << "\n";
    curl_multi_remove_handle(parent->curlMHandle, _handle);
    curl_easy_cleanup(_handle);
    MUTEX_UNLOCK(parent->fetchMutex);
}

unsigned RadioStreamFetch::id()
{
    return _id;
}

const CURL *RadioStreamFetch::handle()
{
    return _handle;
}
