/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef __PLAYLIST_H__
#define __PLAYLIST_H__

#include <qlist.h>
#include <qstring.h>
#include <qtextstream.h>
#include <qobject.h>
#include "module.h"

class RadioPlayListRandomItem
{
    public:
	RadioPlayListRandomItem(unsigned index)
	{
	    i = index;
	};

	unsigned value()
	{
	    return i;
	};

    private:
	unsigned i;
};

/* To be subclassed and passed as the data parameter to RadioPlayListItem. */
class RadioPlayListItemData
{
    public:
	RadioPlayListItemData() {};
	virtual ~RadioPlayListItemData() {};
};
	
class RadioPlayListItem
{
    public:
	RadioPlayListItem(const QString &url,
		const QString &title = QString::null,
		const QString &artist = 0, const QString &album = 0,
		const QString &genre = 0, const QString &streamTitle = 0,
		int bitRate = 0, void *data = NULL)
	{
	    static unsigned __id;

	    _bitRate = bitRate;
	    _url = url;
	    _title = title;
	    _streamTitle = streamTitle;
	    _album = album;
	    _artist = artist;
	    _genre = genre;
	    _bad= false;
	    memset(&info, 0, sizeof(info));
	    infoInUse = false;
	    _version = QString::null;
	    _icyData = QString::null;
	    _icyUrl = QString::null;
	    _played = 0;
	    _error = QString::null;
	    _data = data;
	    _id = ++__id;
	    _ct = QString::null;
	};

	enum {
	    ProtocolUnknown, ProtocolFtp, ProtocolHttp, ProtocolFile
	};

	~RadioPlayListItem()
	{
	    delete (RadioPlayListItemData *)_data;
	};

	void setData(void *data)
	{
	    _data = data;
	};

	void *data()
	{
	    return _data;
	};

	void setBitRate(int b)
	{
	    _bitRate = b;
	};

	int bitRate()
	{
	    return _bitRate;
	};

	unsigned id()
	{
	    return _id;
	};

	void setError(const QString &s)
	{
	    _error = s;
	};

	QString &error()
	{
	    return _error;
	};

	bool played()
	{
	    return _played;
	};

	void setPlayed(bool v)
	{
	    _played = v;
	};

	void setIcyUrl(const QString &u)
	{
	    _icyUrl = u;
	};

	QString &icyUrl()
	{
	    return _icyUrl;
	};

	void setIcyData(const QString &data)
	{
	    _icyData = data;
	};

	QString &icyData()
	{
	    return _icyData;
	};

	void setIcyHttpData(const QString &data)
	{
	    _icyHttpData = data;
	};

	QString &icyHttpData()
	{
	    return _icyHttpData;
	};

	void setFrameInfo(decodeModuleFrameInfo *i)
	{
	    if (!i) {
		_version = QString::null;
		infoInUse = false;
		memset(&info, 0, sizeof(info));
		return;
	    }

	    memcpy(&info, i, sizeof(info));
	    infoInUse = true;
	    _version = QString(i->version);

	    if (info.title)
		setTitle(QString(info.title));

	    if (info.artist)
		setArtist(QString(info.artist));

	    if (info.album)
		setAlbum(QString(info.album));
	};

	decodeModuleFrameInfo *frameInfo()
	{
	    return !infoInUse ? NULL : &info;
	};

	QString &frameVersion()
	{
	    return _version;
	};

	void setBad(bool val)
	{
	    _bad = val;
	};

	bool isBad()
	{
	    return _bad;
	};

	QString &url(void)
	{
	    return _url;
	};

	QString &title(void)
	{
	    return _title;
	};

	void setStreamTitle(const QString &title)
	{
	    _streamTitle = title;
	};

	QString &streamTitle(void)
	{
	    return _streamTitle;
	};

	QString &album(void)
	{
	    return _album;
	};

	QString &artist(void)
	{
	    return _artist;
	};

	QString &genre(void)
	{
	    return _genre;
	};

	int protocol()
	{
	    return parseProtocol(_url);
	};

	int protocol(const QString &url)
	{
	    return parseProtocol(url);
	};

	bool isLocal()
	{
	    return parseProtocol(_url) == ProtocolFile;
	}

	void setUrl(const QString &url)
	{
	    _url = url;
	};

	void setTitle(const QString &title)
	{
	    _title = title;
	};

	void setAlbum(const QString &album)
	{
	    _album = album;
	};

	void setArtist(const QString &artist)
	{
	    _artist = artist;
	};

	void setGenre(const QString &genre)
	{
	    _genre = genre;
	};

	RadioPlayListItem *dup()
	{
	    return new RadioPlayListItem(url(), title(), artist(), album(),
		    genre(), streamTitle(), bitRate(), data());
	}

	void setContentType(const QString &ct)
	{
	    _ct = ct;
	};

	QString contentType()
	{
	    return _ct;
	};

    private:
	int parseProtocol(const QString &url)
	{
	    if (url.left(7) == "ftps://" || url.left(6) == "ftp://")
		return ProtocolFtp;

	    if (url.left(7) == "http://" || url.left(8) == "https://")
		return ProtocolFtp;

	    if (url.left(7) == "file://")
		return ProtocolFile;

	    return ProtocolUnknown;
	};

	QString _url;
	QString _title;
	QString _streamTitle;
	QString _artist;
	QString _album;
	QString _genre;
	bool _bad;
	decodeModuleFrameInfo info;
	bool infoInUse;
	QString _version;
	QString _icyData; // Stream tags.
	QString _icyHttpData; // Initial ICY HTTP header.
	QString _icyUrl; // From the ICY header. Not the connection URL.
	bool _played;
	QString _error;
	unsigned _id;
	int _bitRate;
	void *_data;
	QString _ct;
};

class RadioPlayListList : public QList<RadioPlayListItem>
{
    private:
	friend class RadioPlayList;

	RadioPlayListList() : QList<RadioPlayListItem> ()
	{
	    _sortType = 0;
	    revSort = false;
	};

	void setSortType(int type, bool reverse = false)
	{
	    _sortType = type;
	    revSort = reverse;
	};

	int sortType(bool *r)
	{
	    *r = revSort;
	    return _sortType;
	};

	int _sortType;
	bool revSort;

    protected:
	virtual int compareItems(QCollection::Item, QCollection::Item);
};

class RadioPlayList : public QObject
{
    Q_OBJECT

    public:
	typedef enum Type {
	    PlayListBroken, PlayListPLS, PlayListM3U
	};

	enum {
	    SortId, SortTitle, SortStreamTitle, SortBitRate, SortGenre,
	    SortUrl, SortMax
	};

	RadioPlayList();
	virtual ~RadioPlayList();

	bool parse(const QString &filename, Type, bool reset = false);
	bool parse(const unsigned char *buf, size_t len, Type type);
	bool parseUrl(const QString &str, bool reset = false);
	void append(const QString &url, const QString title = 0);
	void append(RadioPlayListItem *);
	int writeToFile(const QString &);
	bool setSortType(int type, bool reverse = false);
	int sortType(bool *);

	RadioPlayListList playList;
    
    private:
	void doParse(QTextStream &t, Type, bool reset);
};

#endif
