/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "module.h"

struct dummy_handle_s {
    int n;
    decodeModuleFrameInfo info;
    struct dummy_handle_s *next;
};

static int verbose;
static int nHandles;
static struct dummy_handle_s *handles;

int init(int verbosity)
{
    verbose = verbosity;
    return DEC_MOD_OK;
}

static struct dummy_handle_s *findHandle(int n)
{
    struct dummy_handle_s *p;

    for (p = handles; p; p = p->next) {
	if (p->n == n)
	    break;
    }

    return p;
}

static void updateInfo(struct dummy_handle_s *h)
{
    strcpy(h->info.version, "dummy module");
    h->info.infoTypes = DEC_INFO_FORMAT;
}

int valid(int index, const unsigned char *data, size_t len)
{
    struct dummy_handle_s *h = findHandle(index);

    if (h)
	updateInfo(h);

    return h ? DEC_MOD_NEWINFO : DEC_MOD_ERR;
}

static int addHandle(struct dummy_handle_s *h)
{
    struct dummy_handle_s *p;

    h->n = nHandles;

    for (p = handles; p; p = p->next) {
	if (!p->next) {
	    p->next = h;
	    break;
	}
    }

    if (!p)
	handles = h;

    return nHandles++;
}

int openFeed(const decodeModuleFormats *formats, int fsize)
{
    struct dummy_handle_s *h = (struct dummy_handle_s *)malloc(sizeof(struct dummy_handle_s));

    if (!h)
	return DEC_MOD_ERR;

    memset(h, 0, sizeof(struct dummy_handle_s));
    return addHandle(h);
}

int scan(const char *filename)
{
    struct dummy_handle_s *h;
    int index = openFeed(NULL, 0);

    h = findHandle(index);
    updateInfo(h);
    return index;
}

int feed(int index, const void *data, size_t len)
{
    struct dummy_handle_s *h = findHandle(index);

    return h ? DEC_MOD_OK : DEC_MOD_ERR;
}

int decodeFrame(int index, void *data, size_t *len)
{
    struct dummy_handle_s *h = findHandle(index);

    *len = 0;
    return h ? DEC_MOD_NEED_MORE : DEC_MOD_ERR;
}

void finish(int index)
{
    struct dummy_handle_s *h = findHandle(index), *p, *last;

    if (!h)
	return;

    for (last = p = handles; p; p = p->next) {
	if (p->n == h->n) {
	    if (p == handles && p->next)
		handles = p->next;
	    else if (p == handles && !p->next)
		handles = NULL;
	    else
		last->next = p->next;

	    free(p);
	    break;
	}

	last = p;
    }
}

decodeModuleFrameInfo *getInfo(int index)
{
    struct dummy_handle_s *h = findHandle(index);
    return !h ? NULL : &h->info;
}
