/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA

    Most of the audio hardware capability finding code was ripped from
    libmpg123.
*/
#ifndef __DSP_H__
#define __DSP_H__

#include <linux/soundcard.h>

#ifndef AFMT_S16_NE
# ifdef OSS_BIG_ENDIAN
#  define AFMT_S16_NE AFMT_S16_BE
# else
#  define AFMT_S16_NE AFMT_S16_LE
# endif
#endif

#ifndef AFMT_U16_NE
# ifdef OSS_BIG_ENDIAN
#  define AFMT_U16_NE AFMT_U16_BE
# else
#  define AFMT_U16_NE AFMT_U16_LE
# endif
#endif

static int set_rate_oss(int fd, int rate)
{
    int dsp_rate;
    int ret = 0;

    if(rate >= 0) {
	dsp_rate = rate;
	ret = ioctl(fd, SNDCTL_DSP_SPEED,&dsp_rate);
    }
    return ret;
}

static int set_channels_oss(int fd, int channels)
{
    int chan = channels - 1;
    int ret;

    if(channels < 0) return 0;

    ret = ioctl(fd, SNDCTL_DSP_STEREO, &chan);
    if(chan != (channels-1)) return -1;

    return ret;
}

static int set_format_oss(int fd, int format)
{
    int sf,ret;

    if(format == -1) return 0;
    sf = format;
    ret = ioctl(fd, SNDCTL_DSP_SETFMT, &sf);
    if(sf != format) return -1;

    return ret;
}

int EZXRadio::setDspFormats(void *data, decodeModuleFormats *dspFormats,
	size_t max)
{
    EZXRadio *ezx = (EZXRadio *)data;
    int n = 0;
    int f, r;
    static const long rates[9] = /* only the standard rates */
    {
	8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000
    };
    static const int formats[6] = {
	AFMT_S16_NE, AFMT_U8, AFMT_S8, AFMT_MU_LAW, AFMT_A_LAW, AFMT_U16_NE
    };

    ioctl(ezx->dspFd, SNDCTL_DSP_RESET, NULL);

    for (f = 0; f < 6; f++) {
	if (set_format_oss(ezx->dspFd, formats[f]) < 0)
	    continue;

	for (r = 0; r < 9; r++) {
	    int channels = 0;

	    if (set_channels_oss(ezx->dspFd, 1) >= 0)
		channels = 1;

	    if (set_channels_oss(ezx->dspFd, 2) >= 0) {
		if (channels)
		    channels = 3;
		else
		    channels = 2;
	    }

	    if (!channels)
		continue;

	    long rate = rates[r];

	    if (set_rate_oss(ezx->dspFd, rate) < 0)
		continue;

	    if (n == (int)max)
		return n;

	    dspFormats[n].format = formats[f];
	    dspFormats[n].channels = channels;
	    dspFormats[n].rate = rate;
	    n++;
	}
    }

    return n;
}

size_t EZXRadio::writeDsp(void *data, void *ptr, size_t len)
{
    EZXRadio *ezx = (EZXRadio *)data;
    return write(ezx->dspFd, ptr, len);
}

int EZXRadio::syncDsp(void *data, bool now)
{
    EZXRadio *ezx = (EZXRadio *)data;
    return ioctl(ezx->dspFd, now ? SNDCTL_DSP_SYNC : SNDCTL_DSP_POST, NULL);
}

int EZXRadio::dspVolume(void *data)
{
    EZXRadio *ezx = (EZXRadio *)data;
    int v;

    if (ioctl(ezx->mixerFd, SOUND_MIXER_READ_OGAIN, &v) == -1) {
	std::cerr << "/dev/mixer: " << strerror(errno) << std::endl;
	return -1;
    }

    return v;
}

int EZXRadio::setDspVolume(void *data, int value)
{
    EZXRadio *ezx = (EZXRadio *)data;
    int v = value;

    if (ezx->mixerFd == -1)
	return 0;

    if (ioctl(ezx->mixerFd, SOUND_MIXER_WRITE_VOLUME, &v) == -1) {
	std::cerr << "/dev/mixer: " << strerror(errno) << std::endl;
	return -1;
    }

    return 0;
}

int EZXRadio::openDsp(void *data)
{
    EZXRadio *ezx = (EZXRadio *)data;
    int n;

    ezx->dspFd = open("/dev/dsp", O_WRONLY);
    n = errno;

    if (ezx->dspFd == -1) {
	std::cerr << "/dev/dsp: " << strerror(errno) << std::endl;
	return n;
    }

    ezx->mixerFd = open("/dev/mixer", O_RDONLY);
    n = errno;

    if (ezx->mixerFd == -1) {
	std::cerr << "/dev/mixer: " << strerror(errno) << std::endl;
	using ::close;
	close(ezx->dspFd);
	ezx->dspFd = -1;
	return n;
    }

    return 0;
}

int EZXRadio::setDspFormat(void *data, int ch, long rate, int fmt)
{
    EZXRadio *ezx = (EZXRadio *)data;

    if (ezx->dspFd == -1)
	return 0;

    ioctl(ezx->dspFd, SNDCTL_DSP_RESET, NULL);
    ioctl(ezx->dspFd, SNDCTL_DSP_SETFMT, &fmt);
    int c = ch-1;
    ioctl(ezx->dspFd, SNDCTL_DSP_STEREO, &c);
    ioctl(ezx->dspFd, SNDCTL_DSP_SPEED, &rate);
    return 0;
}

int EZXRadio::closeDsp(void *data)
{
    EZXRadio *ezx = (EZXRadio *)data;

    if (ezx->dspFd == -1)
	return -1;

    using ::close;
    close(ezx->dspFd);

    if (ezx->mixerFd != -1)
       close(ezx->mixerFd);

    ezx->mixerFd = ezx->dspFd = -1;
    return 0;
}

#endif
