/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <qdir.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <errno.h>
#include <dlfcn.h>
#include <unistd.h>
#include "stream.h"
#include "fetch.h"

RadioStream::RadioStream(QObject *parent, RadioStreamDsp *d, int verbosity) :
    decodeThread(this), readThread(this), recordThread(this),
    scanFormatThread(this)
{
    struct timeval tv;

    _connectTimeout = DEFAULT_CONNECT_TIMEOUT;
    fileFd = -1;
    dsp = d;
    dspFormatLen = 0;
    memset(dspFormats, 0, MAX_DSP_FORMATS);
    dspMutex = new QMutex(true);
    recordFd = -1;
    _decodeInterval = 100;
    _readInterval = 250;
    curlHandle = NULL;
    eof = false;
    gettimeofday(&tv, NULL);
    /* From perldoc's srand. */
    srandom((tv.tv_sec + tv.tv_usec) ^ (getpid() + (getpid() << 15)));
    _fillOnEmpty = doFill = false;
    _decodeWait = 0;
    decModule = NULL;
    _skipBad = false;
    parentO = parent;
    paused = false;
    verbose = verbosity;
    _playList = new RadioPlayList();
    playListBrowsing = playListCurrent = NULL;
    _recordFilename = QString::null;
    _proxy = QString::null;
    _proxyUsername = QString::null;
    _proxyPassword = QString::null;
    _proxyType = Http;
    curlHeaders = NULL;
    curlHeaders = curl_slist_append(curlHeaders, "Icy-MetaData: 1");
    curlMHandle = curl_multi_init();
    inbufSize = newInbufSize = INBUFSIZE;
    outbufSize = OUTBUFSIZE;
    _recordBufferSize = RECORD_BUFSIZE;
    urlType = UrlTypeUnknown;
    _volume = origVolume = _soundVolume = 0;
    readMutex = new QMutex();
    fetchMutex = new QMutex(true);
    decodeMutex = new QMutex(true);
    recordMutex = new QMutex();
    playListMutex = new QMutex(true);
    icyData.setAutoDelete(true);
    fetchList.setAutoDelete(true);
    fetchErrorList.setAutoDelete(true);
    readTimer = new QTimer(this, NULL);
    connect(readTimer, SIGNAL(timeout()), SLOT(slotRead()));
    fetchTimer = new QTimer(this, NULL);
    connect(fetchTimer, SIGNAL(timeout()), SLOT(slotFetch()));
    restarting = false;
    decodeStartTimer = new QTimer(this);
    scanTimer = new QTimer(this);
    connect(scanTimer, SIGNAL(timeout()), SLOT(slotScanTimer()));
    scanElapsed = 0;
    scanInterval = 15;
    playSoundW = new RadioStreamSound(this);
    QApplication::postEvent(parentO, EVENT(StreamInit));

    if (!openDsp(false)) {
	origVolume = dsp->volume();
	closeDsp(true);
    }
}

void RadioStream::setConnectTimeout(long t)
{
    _connectTimeout = t;
}

long RadioStream::connectTimeout()
{
    return _connectTimeout;
}

void RadioStream::closeDsp(bool force)
{
    MUTEX_LOCK(dspMutex);

    if (origVolume && (dsp->refCount()-1 == 0 || force))
	setVolume(origVolume);

    dsp->close(force);
    MUTEX_UNLOCK(dspMutex);
}

RadioStream::~RadioStream()
{
    icyData.clear();
    stop();
    curl_multi_cleanup(curlMHandle);
    closeDsp(true);
}

void RadioStream::setInputBufferSize(size_t size)
{
    MUTEX_LOCK(readMutex);
    newInbufSize = size;
    MUTEX_UNLOCK(readMutex);
}

size_t RadioStream::getInputBufferSize(void)
{
    return readThread.running() ? inbufSize : newInbufSize;
}

size_t RadioStream::getInputBufferUsed(void)
{
    return total;
}

void RadioStream::setOutputBufferSize(size_t size)
{
    MUTEX_LOCK(decodeMutex);
    outbufSize = size;
    MUTEX_UNLOCK(decodeMutex);
}

size_t RadioStream::getOutputBufferSize(void)
{
    return outbufSize;
}

void RadioStream::setRecordBufferSize(size_t size)
{
    if (size <= 0)
	return;

    MUTEX_LOCK(readMutex);
    flushRecordBuffer();
    freeVector(recordBuf);
    _recordBufferSize = size;
    recordBuf.resize(_recordBufferSize);
    MUTEX_UNLOCK(readMutex);
}

size_t RadioStream::recordBufferSize(void)
{
    return _recordBufferSize;
}

void RadioStream::setPlayList(RadioPlayList *pl, bool rm)
{
    MUTEX_LOCK(playListMutex);
    RadioPlayList *old = _playList;

    stop();
    _playList = pl;

    if (rm)
	delete old;

    playListCurrent = playListScanning = playListBrowsing = NULL;
    playListAt((unsigned)0, false);
    MUTEX_UNLOCK(playListMutex);
}

RadioPlayList *RadioStream::playList(void)
{
    return _playList;
}

bool RadioStream::updatePlayList(bool browseMode)
{
    MUTEX_LOCK(playListMutex);
    RadioPlayListItem *p;

    if (browseMode)
	p = playListBrowsing = playList()->playList.current();
    else
	p = playListCurrent = playList()->playList.current();

    if (!p) {
	MUTEX_UNLOCK(playListMutex);
	return false;
    }

    if (p != playListScanning) {
	sendPlayingEvent(p);
	scanFormatThread.wait();

	if (!scanFormat(p, browseMode)) {
	    if (isPlaying() && !browseMode && isLocal())
		QApplication::postEvent(parentO, EVENT(StreamScanFormat));
	    else
		QApplication::postEvent(parentO, EVENT(StreamFormat));
	}
    }
    else if (browseMode)
	sendStreamEvents(p);

    MUTEX_UNLOCK(playListMutex);
    return true;
}

bool RadioStream::isPlaying()
{
    return readTimer->isActive();
}

bool RadioStream::playListNextPrev(bool next, bool browseMode)
{
    if (!playListCurrent)
	return false;

    MUTEX_LOCK(playListMutex);
    RadioPlayListItem *e, *p = browseMode ? playListBrowsing : playListCurrent;
    bool rc;

    playList()->playList.findRef(p);

    do {
	e = next ? playList()->playList.next() : playList()->playList.prev();
    } while (!browseMode && skipBad() && e && e->isBad() && e != p);

    /* This will reset the internal QList index. */
    if (!e) {
	if (next) {
	    if (browseMode)
		e = playList()->playList.first();
	    else
		e = playList()->playList.last();
	}
	else {
	    if (browseMode)
		e = playList()->playList.last();
	    else
		e = playList()->playList.first();
	}
    }

    if (e == p && !browseMode)
	rc = false;
    else
	rc = updatePlayList(browseMode);

    MUTEX_UNLOCK(playListMutex);

    if (rc && !browseMode) {
	if (isPlaying())
	    restart();
    }

    return rc;
}

bool RadioStream::playListNext(bool browseMode)
{
    return playListNextPrev(true, browseMode);
}

bool RadioStream::playListPrev(bool browseMode)
{
    return playListNextPrev(false, browseMode);
}

void RadioStream::setVolume(int value)
{
    _volume = value;

    if (soundId())
	return;

    dsp->setVolume(value);
}

int RadioStream::volume(void)
{
    return _volume;
}

QString RadioStream::frameVersion(bool browseMode)
{
    if (browseMode)
	return playListBrowsing->frameVersion();

    return playListCurrent->frameVersion();
}

long RadioStream::frameRate(bool browseMode)
{
    decodeModuleFrameInfo *i = browseMode ? playListBrowsing->frameInfo() :
	playListCurrent->frameInfo();

    return i ? i->frameRate : 0;
}

int RadioStream::frameBitRate(bool browseMode)
{
    decodeModuleFrameInfo *i = browseMode ? playListBrowsing->frameInfo() :
	playListCurrent->frameInfo();

    return i ? i->bitRate : 0;
}

QString RadioStream::frameMode(bool browseMode)
{
    decodeModuleFrameInfo *i = browseMode ? playListBrowsing->frameInfo() :
	playListCurrent->frameInfo();

    if (i) {
	switch (i->mode) {
	    case DEC_MODE_STEREO:
		return QString(tr("Stereo"));
	    case DEC_MODE_JSTEREO:
		return QString(tr("Joint Stereo"));
	    case DEC_MODE_DUAL:
		return QString(tr("Dual"));
	    case DEC_MODE_MONO:
		return QString(tr("Mono"));
	    default:
	       	break;
	}
    }

    return QString::null;
}

bool RadioStream::start()
{
    if (playList()->playList.isEmpty())
	return false;

    if (isPlaying())
	return false;

    scanFormatThread.wait();

    if (!initStream())
	return false;

    playListCurrent->setContentType(QString::null);
    emit streamStart();
    readTimer->start(readInterval());
    return true;
}

void RadioStream::stop(void)
{
    emit streamStop();

    if (!isPlaying()) {
	QApplication::postEvent(parentO, EVENT(StreamInit));
	return;
    }

    cleanup();
}

bool RadioStream::restart()
{
    restarting = true;
    stop();
    return start();
}

int RadioStream::dspSync(bool now)
{
    return dsp->sync(now);
}

size_t RadioStream::dspWrite(void *data, size_t len)
{
    return dsp->write(data, len);
}

int RadioStream::openDsp(bool reopen)
{
    MUTEX_LOCK(dspMutex);
    int n = dsp->open(reopen);

    if (!n && !dspFormatLen)
	dspFormatLen = dsp->formats(dspFormats, MAX_DSP_FORMATS);

    if (!reopen && !n && origVolume)
	setVolume(origVolume);
    else if (!n)
	setVolume(volume());

    MUTEX_UNLOCK(dspMutex);
    return n;
}

void RadioStream::sendPlayingEvent(RadioPlayListItem *cur)
{
    if (cur && cur == playListCurrent) {
	playListCurrent->setPlayed(true);
	parseMetaTags(cur);
    }

    QApplication::postEvent(parentO, EVENT(StreamPlaying));
}

void RadioStream::setDspFormat(int ch, long rate, int fmt)
{
    MUTEX_LOCK(dspMutex);
    dsp->setFormat(ch, rate, fmt);
    MUTEX_UNLOCK(dspMutex);
}

void RadioStream::updateInfo(RadioPlayListItem *cur, decodeModule *m)
{
    if (!m || !cur)
	return;

    MUTEX_LOCK(decodeMutex);
    m->info = (*m->getInfo)(m->index);
    MUTEX_UNLOCK(decodeMutex);

    if (!m->info)
	return;

    if (m->info->infoTypes)
	cur->setFrameInfo(m->info);

    if (isPlaying() && cur == playListCurrent && m->info &&
	    (m->info->infoTypes & DEC_INFO_FORMAT) && !soundId())
	setDspFormat(m->info->channels, m->info->frameRate, m->info->format);

    if ((m->info->infoTypes & DEC_INFO_FORMAT) && cur == playListCurrent)
	QApplication::postEvent(parentO, EVENT(StreamFormat));

    sendPlayingEvent(cur);
}

bool RadioStreamDecodeThread::decode(const unsigned char *buf, size_t len)
{
    int n;

    do {
	size_t done;
	void *p;

	while (stream->soundId() || stream->isPaused() || stream->isBuffering()) {
	    if (!stream->isPlaying())
		return false;

	    QApplication::postEvent(stream->parentO, EVENT(StreamConnected));

	    if (stream->isBuffering()) {
		MUTEX_LOCK(stream->readMutex);
		bool b = (size_t)stream->total >= stream->inbufSize / 2 ? true : false;
		MUTEX_UNLOCK(stream->readMutex);

		if (b || stream->eof || stream->doFlushFillBuffer) {
		    stream->doFill = stream->doFlushFillBuffer = false;
		    emit stream->streamBufferEmpty(false);

		    if (!stream->isPaused() && !stream->soundId())
			break;
		}
		else if (!stream->isPaused())
		    emit stream->streamBufferEmpty(true);
	    }

	    msleep(stream->decodeInterval());
	}

	if (len) {
	    MUTEX_LOCK(stream->decodeMutex);
	    n = (*stream->decModule->feed)(stream->decModule->index, buf, len);
	    MUTEX_UNLOCK(stream->decodeMutex);

	    if (n != DEC_MOD_OK)
		return false;

	    len = 0;
	}

	/* Should set done to 0 and return DEC_MOD_OK at EOF. */
	MUTEX_LOCK(stream->decodeMutex);
	n = (*stream->decModule->decodeFrame)(stream->decModule->index, &p, &done);
	MUTEX_UNLOCK(stream->decodeMutex);

	if (n == DEC_MOD_ERR)
	    return false;

	if (n == DEC_MOD_NEWINFO) {
	    MUTEX_LOCK(stream->playListMutex);
	    stream->updateInfo(stream->playListCurrent, stream->decModule);
	    MUTEX_UNLOCK(stream->playListMutex);
	}

	if (done && !stream->isPaused()) {
	    MUTEX_LOCK(stream->dspMutex);
	    size_t len = stream->dspWrite(p, done);
	    MUTEX_UNLOCK(stream->dspMutex);

	    if (len != done)
		return false;
	}
	else if (!n && !stream->isPaused()) {
	    MUTEX_LOCK(stream->dspMutex);
	    stream->dspSync();
	    MUTEX_UNLOCK(stream->dspMutex);
	    stream->eof = true;
	}
    } while (n != DEC_MOD_ERR && n != DEC_MOD_NEED_MORE && stream->isPlaying());

    return n == DEC_MOD_ERR ? false : true;
}

void RadioStreamDecodeThread::run(void)
{
    QApplication::postEvent(stream->parentO, EVENT(StreamConnected));

    if (!stream->isPaused() && !stream->soundId()) {
	MUTEX_LOCK(stream->dspMutex);
	stream->dspSync();
	MUTEX_UNLOCK(stream->dspMutex);
    }

    if ((!stream->isLocal() && stream->decodeWait() > 0) || stream->soundId()) {
	stream->doFill = true;

	if (!stream->isLocal())
	    stream->decodeStartTimer->start(stream->decodeWait()*1000, true);

	while ((stream->decodeStartTimer->isActive() && stream->isPlaying() &&
		!stream->doFlushFillBuffer) ||
		(stream->soundId() && stream->isPlaying())) {
	    if (!stream->isLocal())
		emit stream->streamBufferEmpty(stream->decodeStartTimer->isActive() && !stream->doFlushFillBuffer);
	    msleep(500);
	}

	stream->doFill = false;
    }

    for (;;) {
	MUTEX_LOCK(stream->readMutex);
	bool update = false;
	unsigned char *buf = NULL;
	size_t len = 0;

	if (!stream->isPlaying()) {
	    MUTEX_UNLOCK(stream->readMutex);
	    exit();
	}

	if (!stream->decModule || (!stream->total && stream->eof)) {
	    /* No more data in the read buffer and EOF detected. Exit. */
	    if (stream->eof) {
		MUTEX_UNLOCK(stream->readMutex);
		goto fail;
	    }

	    goto done;
	}

	if (stream->total && !stream->isBuffering() && !stream->soundId()) {
	    MUTEX_LOCK(stream->decodeMutex);
	    len = stream->total > stream->outbufSize ? stream->outbufSize : stream->total;
	    MUTEX_UNLOCK(stream->decodeMutex);

	    update = updateMetaData(len);
	    buf = new unsigned char[len];

	    if (!buf)
		goto done;

	    memcpy(buf, &stream->inbuf.front(), len);
	    memmove(&stream->inbuf.front(), &stream->inbuf[len], stream->inbufSize - len);
	    stream->total -= len > stream->total ? stream->total : len;

	    if (update) {
		MUTEX_LOCK(stream->playListMutex);
		stream->sendPlayingEvent(stream->playListCurrent);
		MUTEX_UNLOCK(stream->playListMutex);
	    }

	    MUTEX_UNLOCK(stream->readMutex);
	}
	else {
	    MUTEX_UNLOCK(stream->readMutex);

	    if (!stream->isBuffering())
		goto done;
	}

	if (!decode(buf, len)) {
	    stream->eof = true;
	    goto done;
	}

	QApplication::postEvent(stream->parentO, EVENT(StreamConnected));

done:
	if (buf)
	    delete[] buf;

	if (!stream->eof && !stream->total && stream->fillOnEmpty() &&
		!stream->isLocal()) {
	    stream->doFill = true;
	    stream->doFlushFillBuffer = false;
	    emit stream->streamBufferEmpty(true);
	}

	msleep(stream->decodeInterval());
	continue;

fail:
	if (buf)
	    delete[] buf;

	QApplication::postEvent(stream->parentO, EVENT(StreamEof));
	exit();
    }
}

int RadioStream::parseInput(const QString &str)
{
    int offset = str.find(QString("\r\n\r\n"), 0, FALSE);

    /* Remote playlist (.m3u) */
    if (str.find(QString("#EXTM3U"), 0, FALSE) != -1)
	return offset != -1 ? UrlTypeM3uDone : UrlTypeM3u;

    /* Remote playlist (.pls) */
    if (str.find(QString("[playlist]"), 0, FALSE) != -1)
	return offset != -1 ? UrlTypePlsDone : UrlTypePls;

    int n = str.find(QRegExp("ICY [0-9]* .*\n"));

    if (n != -1) {
	QString tmp = str.mid(n+4);
	QString rc = tmp.left(tmp.find(' '));

	if (rc.toInt() != 200) {
	    playListCurrent->setError(str.mid(n, str.find('\n', n)));
	    return UrlTypeIcy;
	}
    }

    n = str.find(QRegExp("content-type:.*\n", false));

    if (n != -1) {
	QString t = str.mid(n+13);

	n = t.find(QChar('\n'));
	playListCurrent->setContentType(t.left(n-1));
    }

    /* Maybe a continueation from UrlTypeIcy. We're looking for a header
     * that a module supports now. */
    if (offset != -1) {
	offset += 4;
	bool isIcy = false;

	/* ICY HTTP metadata header. */
	if (str.find(QString("icy-url"), 0, FALSE) != -1) {
	    playListCurrent->setIcyHttpData(str.left(offset));
	    isIcy = true;
	}

	total -= offset;
	received = total;
	memmove(&inbuf.front(), &inbuf[offset], total);
	    
	if (isIcy)
	    return UrlTypeIcy;

	/* Broken playlist, try and parse it anyway. */
	if (str.mid(offset).left(7).lower() == "http://") {
	    eof = true;
	    return UrlTypeBrokenDone;
	}
    }

    /* Gotta do this after trying to parse any ICY data (above). */
    if (!decModule && total) {
	bool match;

	decModule = findModuleFromData(playListCurrent, &inbuf.front(), total, &match);

	/* No module matches the content-type header. */
	if (!match)
	    return UrlTypeError;

	if (decModule)
	    return UrlTypeFound;
    }

    return UrlTypeNeedMore;
}

bool RadioStream::parsePlayList(const unsigned char *buf, size_t total,
	RadioPlayList::Type which)
{
    RadioPlayList *pl = new RadioPlayList();
    pl->parse(buf, total, which);
    eof = true;

    if (!pl->playList.count()) {
	playListCurrent->setError(tr("empty playlist"));
	delete pl;
	return false;
    }

    QCustomEvent *ev = new QCustomEvent(QEvent::Type(StreamPlayList));
    ev->setData(pl);
    QApplication::postEvent(parentO, ev);
    return true;
}

void RadioStream::slotRead()
{
    bool unlocked = false;

    if (readThread.running() || !isPlaying())
	return;

    /* It's a little tricky to get EOF handled correctly. EOF is set in the
     * readThread when the curl handle detects EOF. There may be data left in
     * the read buffer that needs to be processed so the decodeThread will do
     * the actual sending of the StreamEof event unless an error occurred in
     * which case that event is sent in the readThread. The following error
     * test checks for both a stream error (set from readThread) or only an
     * bad handle which is set here after determining the urlType. */
    if (readRc)
	return;

    if (!decodeThread.running() && total && !decModule && lastReadSize != total) {
	MUTEX_LOCK(readMutex);
	unsigned char c = inbuf[total];

	lastReadSize = total;
	inbuf[total] = 0;
	QString s((const char *)&inbuf.front());
	inbuf[total] = c;
	urlType = parseInput(s);

	switch (urlType) {
	    case UrlTypeBroken:
		if (!eof)
		    break;
	    case UrlTypeBrokenDone:
		parsePlayList(&inbuf.front(), total, RadioPlayList::PlayListBroken);
		break;
	    case UrlTypePls:
		if (!eof)
		    break;
	    case UrlTypePlsDone:
		parsePlayList(&inbuf.front(), total, RadioPlayList::PlayListPLS);
		break;
	    case UrlTypeM3u:
		if (!eof)
		    break;
	    case UrlTypeM3uDone:
		parsePlayList(&inbuf.front(), total, RadioPlayList::PlayListM3U);
		break;
	    case UrlTypeIcy:
		if (playListCurrent->error())
		    eof = true;

		break;
	    case UrlTypeFound:
		MUTEX_LOCK(playListMutex);
		sendPlayingEvent(playListCurrent);
		MUTEX_UNLOCK(playListMutex);
		break;
	    case UrlTypeError:
		eof = true;
		break;
	    case UrlTypeNeedMore:
	    default:
		break;
	}

	/* The entire file was received or a limit was reach while trying to
	 * parse a playlist.
	 */
	if ((eof && urlType != UrlTypeFound) ||
		(total >= PARSE_MAX && urlType != UrlTypeFound)) {
	    MUTEX_UNLOCK(readMutex);

	    if ((urlType == UrlTypeError) ||
		    (!playListCurrent->error() && total >= PARSE_MAX))
		playListCurrent->setError(tr("unknown file format"));

	    playListCurrent->setBad(true);
	    playListCurrent->setPlayed(true);
	    QApplication::postEvent(parentO, EVENT(StreamError));
	    return;
	}

	/* Set from readThread? */
	if (eof) {
	    readRc = CURLM_BAD_HANDLE;
	    MUTEX_UNLOCK(readMutex);
	    QApplication::postEvent(parentO, EVENT(StreamEof));
	    return;
	}

	if (decModule) {
	    unlocked = true;
	    MUTEX_UNLOCK(readMutex);
	    decodeThread.start();
	    QApplication::postEvent(parentO, EVENT(StreamRecord));
	}

	if (!unlocked)
	    MUTEX_UNLOCK(readMutex);
    }
    else if (decModule && !decodeThread.running() && !eof) {
	decodeThread.start();
	QApplication::postEvent(parentO, EVENT(StreamRecord));
    }
    else if (eof && urlType == UrlTypeNeedMore) {
	playListCurrent->setError(tr("unknown file format"));
	playListCurrent->setBad(true);
	playListCurrent->setPlayed(true);
	QApplication::postEvent(parentO, EVENT(StreamError));
	return;
    }

    MUTEX_LOCK(fetchMutex);
    readThread.start();
    MUTEX_UNLOCK(fetchMutex);

    if (total)
	QApplication::postEvent(parentO, EVENT(StreamConnected));
}

void RadioStream::closeDecModule(decodeModule **module)
{
    if (!module || !*module)
	return;

    decodeModule *m = *module;

    MUTEX_LOCK(decodeMutex);
    (*m->finish)(m->index);
    MUTEX_UNLOCK(decodeMutex);
    delete m;
    *module = NULL;
}

void RadioStream::cleanup()
{
    MUTEX_LOCK(readMutex);
    readTimer->stop();
    MUTEX_UNLOCK(readMutex);
    stopRecord();

    if (!restarting)
	QApplication::postEvent(parentO, EVENT(StreamInit));

    readThread.wait();
    decodeThread.wait();
    scanFormatThread.wait();
    emit streamBufferEmpty(false);

    if (!restarting)
	QApplication::postEvent(parentO, EVENT(StreamInit));

    restarting = false;
    curl_multi_remove_handle(curlMHandle, curlHandle);
    curl_easy_cleanup(curlHandle);
    curlHandle = NULL;
    freeVector(inbuf);
    eof = false;
    freeVector(recordBuf);
    freeVector(metaintBuf);
    closeDecModule(&decModule);

    if (fileFd != -1)
	close(fileFd);

    fileFd = -1;
    MUTEX_LOCK(dspMutex);
    dspSync(false);
    MUTEX_UNLOCK(dspMutex);
    closeDsp();
}

bool RadioStreamDecodeThread::updateMetaData(size_t len)
{
    IcyMetaDataItem *i;

    for (i = stream->icyData.first(); i; i = stream->icyData.next()) {
	i->setOffset(i->offset()-len);
	ssize_t offset = i->offset();

	if (offset <= 0) {
	    MUTEX_LOCK(stream->playListMutex);
	    stream->playListCurrent->setIcyData(i->data());
	    MUTEX_UNLOCK(stream->playListMutex);
	    stream->icyData.remove();
	    stream->icyData.first();
	    return true;
	}
    }

    return false;
}

bool RadioStream::testInputBuf(size_t len)
{
    while ((total + len > inbufSize)) {
	struct timeval tv = {0, 100000};

	MUTEX_UNLOCK(readMutex);
#if 0
	std::cerr << "input buffer full, waiting ... total=" << std::dec << total << ", len=" << std::dec << len << std::endl;
#endif
	select(0, NULL, NULL, NULL, &tv);

	if (!isPlaying())
	    return false;

	/* Local file. */
	if (!decodeThread.running()) {
	    int n = parseInput(QString::null);
	    
	    if (n != RadioStream::UrlTypeFound) {
		MUTEX_LOCK(readMutex);
		return false;
	    }

	    decodeThread.start();
	}

	MUTEX_LOCK(readMutex);
    }

    return true;
}

void RadioStream::flushRecordBuffer()
{
    if (!isRecording())
	return;

    recordThread.start();
    recordThread.wait();
}

CurlWrite::CurlWrite(RadioStream *s, unsigned char *data, size_t size)
{
    ptr = data;
    len = size;
    stream = s;
    optr = NULL;
}


CurlWrite::~CurlWrite()
{
    delete[] optr;
}

bool CurlWrite::updateInbufSize()
{
    MUTEX_LOCK(stream->readMutex);

    while (stream->total > stream->newInbufSize) {
	struct timeval tv = {0, 50000};

	MUTEX_UNLOCK(stream->readMutex);
	select(0, NULL, NULL, NULL, &tv);

	if (!stream->isPlaying())
	    return false;

	MUTEX_LOCK(stream->readMutex);
    }

    if (stream->inbufSize != stream->newInbufSize) {
	stream->inbufSize = stream->newInbufSize;
	stream->inbuf.resize(stream->inbufSize);
    }

    MUTEX_UNLOCK(stream->readMutex);
    return true;
}

void CurlWrite::updateRecordBuf()
{
    MUTEX_LOCK(stream->recordMutex);

    if (stream->isRecording()) {
	if (stream->recordBufLen + len > stream->recordBufferSize()) {
	    std::cerr << "record buffer full, dumping: TOTAL="
		<< stream->recordBufferSize() << ", LEN=" << std::dec << len << std::endl;
	    stream->flushRecordBuffer();
	}

	memcpy(&stream->recordBuf[stream->recordBufLen], ptr, len);
	stream->recordBufLen += len;
    }

    MUTEX_UNLOCK(stream->recordMutex);
}

size_t CurlWrite::process(bool isLocal)
{
    /* olen is the original length of this read. It should be returned
     * otherwise it's a libcurl failure. */
    typeof(len) olen = len;
    size_t received;
    char *hbuf = NULL;

    MUTEX_LOCK(stream->readMutex);
    received = stream->received+len;

    if (!stream->isPlaying()) {
	MUTEX_UNLOCK(stream->readMutex);
	return 0;
    }

    if (!stream->testInputBuf(len)) {
	MUTEX_UNLOCK(stream->readMutex);
	return 0;
    }

    if (isLocal) {
	memcpy(&stream->inbuf[stream->total], ptr, len);
	stream->total += len;
	stream->received += len;
	MUTEX_UNLOCK(stream->readMutex);
	return olen;
    }

    stream->init = false;

    /* See if the header can be completed during this read. */
    if (stream->metaintLen) {
	stream->metaintBuf.resize(stream->metaintBufLen+len);
	memcpy(&stream->metaintBuf[stream->metaintBufLen], ptr, len);
	stream->metaintBufLen += len;

	/* The previous read was right on the money (received == ICY-METAINT).
	 * This read should contain the first byte which is the header length.
	 */
	if (stream->metaintLen == -1)
	    stream->metaintLen = *ptr * 16;

	if ((size_t)stream->metaintLen > stream->metaintBufLen) {
	    MUTEX_UNLOCK(stream->readMutex);
	    return olen;
	}

	if (stream->metaintLen) {
	    hbuf = new char[stream->metaintLen];
	    memcpy(hbuf, &stream->metaintBuf.front(), stream->metaintLen);
	    stream->icyData.append(new IcyMetaDataItem(hbuf, stream->total));
	    delete[] hbuf;
	    hbuf = NULL;
	}

	stream->metaintLen++;
	len = stream->metaintBufLen-stream->metaintLen;

	if (!stream->testInputBuf(len)) {
	    MUTEX_UNLOCK(stream->readMutex);
	    return 0;
	}

	optr = ptr = new typeof(*ptr)[len];
	memcpy(ptr, &stream->metaintBuf[stream->metaintLen],
		stream->metaintBufLen-stream->metaintLen);
	received = len;
	freeVector(stream->metaintBuf);
	stream->metaintBufLen = stream->metaintLen = 0;
    }

    if (stream->icyMetaInt && (received / stream->icyMetaInt)) {
	size_t offset;
	typeof(ptr) p;
	size_t hlen;

	/* len may be bigger than ICY-METAINT */
	while ((received / stream->icyMetaInt > 1)) {
	    offset = len - (received % stream->icyMetaInt) - stream->icyMetaInt;
	    memcpy(&stream->inbuf[stream->total], ptr, offset);
	    stream->total += offset;
	    ptr = ptr + offset;
	    len -= offset;
	    p = ptr;
	    hlen = *p * 16;

	    if (hlen) {
		hbuf = new char[hlen];
		memcpy(hbuf, p+1, hlen);
		stream->icyData.append(new IcyMetaDataItem(hbuf, stream->total));
		delete[] hbuf;
		hbuf = NULL;
	    }

	    hlen++;
	    ptr = ptr + hlen;
	    len -= hlen;
	    received -= stream->icyMetaInt + hlen; 
	}

	offset = len - (received % stream->icyMetaInt);
	p = ptr + offset;
	hlen = *p * 16;

	if (!(received % stream->icyMetaInt)) {
	    memcpy(&stream->inbuf[stream->total], ptr, len);
	    stream->total += len;
	    stream->received = 0;
	    stream->metaintLen = -1;
	    MUTEX_UNLOCK(stream->readMutex);
	    return olen;
	}

	/* If the header can't be parsed completely during this read then wait
	 * for the next read. */
	if (hlen && hlen > len-offset) {
	    memcpy(&stream->inbuf[stream->total], ptr, offset);
	    stream->total += offset;
	    len -= offset;

	    if (len) {
		stream->metaintBuf.resize(len);
		memcpy(&stream->metaintBuf[0], ptr+offset, len);
		stream->metaintBufLen = len;
		stream->metaintLen = hlen;
	    }
	    else
		stream->metaintLen = -1;

	    stream->received = 0;
	    MUTEX_UNLOCK(stream->readMutex);
	    return olen;
	}

	if (hlen) {
	    hbuf = new char[hlen];
	    memcpy(hbuf, p+1, hlen);
	}

	hlen++;
	memcpy(&stream->inbuf[stream->total], ptr, offset);
	stream->total += offset;
	memcpy(&stream->inbuf[stream->total], p+hlen, len-(offset+hlen));
	stream->total += len-(offset+hlen);
	stream->received = len-(offset+hlen);

	if (hbuf) {
	    stream->icyData.append(new IcyMetaDataItem(hbuf, stream->total));
	    delete[] hbuf;
	}

	MUTEX_UNLOCK(stream->readMutex);
	return olen;
    }

    memcpy(&stream->inbuf[stream->total], ptr, len);
    stream->total += len;
    stream->received += len;
    MUTEX_UNLOCK(stream->readMutex);
    return olen;
}

size_t RadioStream::curlWriteCb(void *ptr, size_t size, size_t nmemb,
	void *data)
{
    CurlWrite c((RadioStream *)data, (unsigned char *)ptr, size*nmemb);

    if (!c.updateInbufSize())
	return 0;

    size_t len = c.process();

    /* Must be done after process() so ptr and len can be updated. */
    c.updateRecordBuf();
    return len;
}

void RadioStream::setProxyType(ProxyType t)
{
    _proxyType = t;
}

RadioStream::ProxyType RadioStream::proxyType()
{
    return _proxyType;
}

void RadioStream::setProxyUsername(const QString &str)
{
    _proxyUsername = str;
}

QString &RadioStream::proxyUsername()
{
    return _proxyUsername;
}

void RadioStream::setProxyPassword(const QString &str)
{
    _proxyPassword = str;
}

QString &RadioStream::proxyPassword()
{
    return _proxyPassword;
}

void RadioStream::setProxy(const QString &str)
{
    _proxy = str;
}

QString &RadioStream::proxy(void)
{
    return _proxy;
}

QStringList &RadioStream::proxyPorts(void)
{
    return _proxyPorts;
}

void RadioStream::setProxyPorts(const QStringList &l)
{
    _proxyPorts = l;
}

#define BUFSIZE		32768

decodeModule *RadioStream::findModuleFromFilename(RadioPlayListItem *cur,
	const QString &filename)
{
    if (filename.isEmpty())
	return NULL;

    QFileInfo fi(filename);

    if (!fi.size())
	return NULL;

    int fd = open(filename.data(), O_RDONLY);
    decodeModule *m = NULL;

    if (fd == -1)
	return NULL;

    unsigned char *buf = new unsigned char[BUFSIZE];
    std::vector<unsigned char> fill;
    size_t total = 0;

    if (!buf)
	goto fail;

    do {
	using :: read;
	size_t len = read(fd, buf, BUFSIZE);

	if (len <= 0)
	    break;

	fill.resize(total+len);
	memcpy(&fill[total], buf, len);
	total += len;
	m = findModuleFromData(cur, &fill.front(), total, NULL);
    } while (!m && total < PARSE_MAX);

fail:
    freeVector(fill);

    if (buf)
	delete[] buf;

    close(fd);
    return m;
}

decodeModule *RadioStream::findModuleFromData(RadioPlayListItem *cur,
	const unsigned char *data, size_t len, bool *match)
{
    return findModule(cur, data, len, match);
}

decodeModule *RadioStream::findModule(RadioPlayListItem *cur,
	const unsigned char *data, size_t len, bool *match)
{
    QDir d(QDir::homeDirPath() + "/.ezxradio/plugins");

    decodeModule *m = new decodeModule;
    memset(m, 0, sizeof(decodeModule));
    MUTEX_LOCK(decodeMutex);

    for (unsigned i = 0; i < d.count(); i++) {
	QString fn = QDir::homeDirPath() + "/.ezxradio/plugins/" + d[i];
	void *h;

	if (match)
	    *match = true;

	if (d[i] == "." || d[i] == "..")
	    continue;

	h = dlopen(fn.data(), RTLD_NOW);
	std::cerr << "TRYING (LEN=" << std::dec << len << "): " << fn.data() << std::endl;

	if (!h) {
	    std::cerr << "NO GOOD: " << d[i].data() << ", " << dlerror() << std::endl;
	    continue;
	}

	m->finish = (void(*)(int))dlsym(h, "finish");

	if (!m->finish)
	    goto fail;

	m->init = (int(*)(int, const char ***))dlsym(h, "init");

	if (!m->init)
	    goto fail;

	const char **types, **p;

	if ((*m->init)(verbose, &types) != DEC_MOD_OK)
	    goto fail;

	if (cur && cur->contentType() && match) {
	    *match = false;

	    for (p = types; p && *p; p++) {
		QString t = QString(*p).lower();

		if (!QString::compare(cur->contentType().lower(), t)) {
		    *match = true;
		    break;
		}
	    }
	}

	if (cur && !cur->isLocal() && cur->contentType() && match && !*match)
	    goto fail;

	m->openFeed = (int(*)(const decodeModuleFormats *, int))dlsym(h, "openFeed");

	if (!m->openFeed)
	    goto fail;

	m->valid = (int(*)(int, const void *, size_t))dlsym(h, "valid");

	if (!m->valid)
	    goto fail;

	m->scan = (int(*)(const char *))dlsym(h, "scan");

	if (!m->scan)
	    goto fail;

	m->feed = (int(*)(int, const void *, size_t))dlsym(h, "feed");

	if (!m->feed)
	    goto fail;

	m->decodeFrame = (int(*)(int, void **, size_t*))dlsym(h, "decodeFrame");

	if (!m->decodeFrame)
	    goto fail;

	m->getInfo = (decodeModuleFrameInfo*(*)(int))dlsym(h, "getInfo");

	if (!m->getInfo)
	    goto fail;

	if ((m->index = (*m->openFeed)(dspFormats, dspFormatLen)) == DEC_MOD_ERR)
	    goto fail;

	if ((*m->valid)(m->index, data, len) != DEC_MOD_NEWINFO)
	    goto fail;

	// FIXME playListMutex deadlock.
	if (cur)
	    updateInfo(cur, m);

	(*m->finish)(m->index);
	m->index = (*m->openFeed)(dspFormats, dspFormatLen);

	if (m->index == DEC_MOD_ERR)
	    goto fail;

	std::cerr << "USING MODULE: " << d[i].data() << std::endl;
	m->handle = h;
	break;

fail:
	std::cerr << "NO GOOD: " << d[i].data() << std::endl;

	if (m->finish)
	    (*m->finish)(m->index);

	dlclose(h);
    }

    if (!m->handle) {
	delete m;
	m = NULL;
    }

    MUTEX_UNLOCK(decodeMutex);
    return m;
}

bool RadioStream::addCurlHandle(CURL *handle, const QString &url,
	bool internal)
{
    MUTEX_LOCK(fetchMutex);
    CURLcode crc;
    CURLMcode mrc;

    std::cerr << __FUNCTION__ << "(): URL='" << url.data() << "'" << std::endl;
    crc = curl_easy_setopt(handle, CURLOPT_VERBOSE, verbose);
    crc = curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1);
    crc = curl_easy_setopt(handle, CURLOPT_FAILONERROR, 1);
    crc = curl_easy_setopt(handle, CURLOPT_URL, url.data());
    crc = curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1);
    crc = curl_easy_setopt(handle, CURLOPT_CONNECTTIMEOUT, _connectTimeout);

    /* Not called from fetch(). */
    if (internal) {
	crc = curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, curlWriteCb);
	crc = curl_easy_setopt(handle, CURLOPT_WRITEDATA, this);
	crc = curl_easy_setopt(handle, CURLOPT_WRITEHEADER, this);
	crc = curl_easy_setopt(handle, CURLOPT_HTTPHEADER, curlHeaders);
    }

    crc = curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1);
    crc = curl_easy_setopt(handle, CURLOPT_MAXREDIRS, 8);
    crc = curl_easy_setopt(handle, CURLOPT_UNRESTRICTED_AUTH, 1);

    if (proxy() != QString::null &&
	    (url.left(7) == "http://" || url.left(8) == "https://")) {
	int n = url.find(QRegExp(":[0-9]+[/]?"));
	int p = 80;
	bool found = false;

	if (n != -1)
	    p = atoi(url.mid(++n).data());

	QStringList l = proxyPorts();

	for (QStringList::Iterator it = l.begin(); it != l.end(); ++it) {
	    bool ok;

	    if ((*it).toInt(&ok, 10) == p) {
		found = true;
		break;
	    }
	}

	if (!proxyPorts().count() || found) {
	    std::cerr << "Using proxy " << proxy().data() << std::endl;
	    // Needed for some proxys. Some IceCAST servers use the
	    // User-agent: to determine what to send (html or audio).
	    // So this may break some (IceCAST) servers (rare).
	    crc = curl_easy_setopt(handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2");
	    crc = curl_easy_setopt(handle, CURLOPT_PROXY, proxy().data());

	    if (proxyUsername() || proxyPassword()) {
		QString auth(QString("%1:%2").arg(proxyUsername() ? proxyUsername() : "").arg(proxyPassword() ? proxyPassword() : ""));
		crc = curl_easy_setopt(handle, CURLOPT_PROXYUSERPWD, auth.data());
	    }

	    int n;

	    switch (_proxyType) {
		case Socks4:
		    n = CURLPROXY_SOCKS4;
		    break;
		case Socks4a:
		    n = CURLPROXY_SOCKS4A;
		    break;
		case Socks5:
		    n = CURLPROXY_SOCKS5;
		    break;
		default:
		    n = CURLPROXY_HTTP;
		    break;
	    }

	    crc = curl_easy_setopt(handle, CURLOPT_PROXYTYPE, n);
	}
    }

    mrc = curl_multi_add_handle(curlMHandle, handle);
    MUTEX_UNLOCK(fetchMutex);
    return true;
}

bool RadioStream::initStream(void)
{
    std::cerr << __FILE__ << "(" << std::dec << __LINE__ << "): " << __FUNCTION__ << std::endl;
    MUTEX_LOCK(playListMutex);
    int n = openDsp(false);

    if (n) {
	setError(strerror(n));
	MUTEX_UNLOCK(playListMutex);
	QApplication::postEvent(parentO, EVENT(StreamError));
	return false;
    }

    setVolume(volume());

    scanElapsed = 0;
    playListCurrent->setError(QString::null);
    playListCurrent->setBad(false);
    doFill = doFlushFillBuffer = false;
    init = true;
    eof = false;
    lastReadSize = 0;
    received = 0;
    total = 0;
    icyMetaInt = 0;
    readRc = CURLM_OK;
    QApplication::postEvent(parentO, EVENT(StreamConnecting));
    inbufSize = newInbufSize;
    inbuf.resize(inbufSize);
    total = 0;
    urlType = UrlTypeUnknown;
    metaintLen = 0;
    metaintBufLen = 0;
    recordBufLen = 0;
    icyData.clear();

    /* libcurl will block in curl_multi_perform() when playing a file://
     * (version 7.19.7). It's a known bug and they have plans on fixing it. */
    if (!isLocal()) {
	curlHandle = curl_easy_init();
	addCurlHandle(curlHandle, playListCurrent->url(), true);
    }
    else {
	QString s = playListCurrent->url();
	fileFd = open(s.right(s.length()-7).data(), O_RDONLY);

	if (fileFd == -1) {
	    setError(QString("%1").arg(strerror(errno)));
	    playListCurrent->setBad(true);
	}
    }

    MUTEX_UNLOCK(playListMutex);
    return true;
}

void RadioStream::parseMetaTags(RadioPlayListItem *cur)
{
    bool updated = false;

    if (cur->icyData() != QString::null) {
	QStringList list = QStringList::split(';', cur->icyData());
	int n;

	for (QStringList::Iterator it = list.begin(); it != list.end(); ++it) {
	    QString p((*it));

	    std::cerr << "ICY HEADER: " << p.data() << std::endl;
	    n = p.find("StreamTitle='");

	    if (n != -1) {
		QString tmp = p.mid(n+13);

		tmp.truncate(tmp.length()-1);

		if (!tmp.isEmpty())
		    cur->setTitle(tmp);

		updated = true;
		continue;
	    }

	    n = p.find("StreamUrl='");

	    if (n != -1) {
		QString tmp = p.mid(n+11);

		tmp.truncate(tmp.length()-1);

		if (!tmp.isEmpty())
		    cur->setIcyUrl(tmp);

		updated = true;
		continue;
	    }
	}

	cur->setIcyData(QString::null);
    }

    if (cur->icyHttpData() != QString::null) {
	QStringList list = QStringList::split('\n', cur->icyHttpData());

	for (QStringList::Iterator it = list.begin(); it != list.end(); ++it ) {
	    int n;
	    QString p((*it).data());

	    p = p.simplifyWhiteSpace();
	    n = p.find("icy-url:", 0, FALSE);

	    if (n != -1) {
		cur->setIcyUrl(p.right(p.length() - 8));
		updated = true;
		continue;
	    }

	    n = p.find("icy-genre:", 0, FALSE);

	    if (n != -1) {
		cur->setGenre(p.right(p.length() - 10));
		updated = true;
		continue;
	    }

	    n = p.find("icy-name:", 0, FALSE);

	    if (n != -1) {
		cur->setTitle(p.right(p.length() - 9));
		cur->setStreamTitle(cur->title());
		updated = true;
		continue;
	    }

	    n = p.find("icy-metaint:", 0, FALSE);

	    if (n != -1) {
		p = p.right(p.length() - 12);
		bool ok;
		icyMetaInt = p.toLong(&ok);

		if (ok)
		    std::cerr << "ICY-METAINT: " << icyMetaInt << std::endl;

		updated = true;
		continue;
	    }
	}

	cur->setIcyHttpData(QString::null);
    }

    if (updated) {
	if (cur->title())
	    std::cerr << "TITLE: " << cur->title().data() << std::endl;

	if (cur->artist())
	    std::cerr << "ARTIST: " << cur->artist().data() << std::endl;

	if (cur->album())
	    std::cerr << "ALBUM: " << cur->album().data() << std::endl;

	if (cur->genre())
	    std::cerr << "GENRE: " << cur->genre().data() << std::endl;

	if (cur->url())
	    std::cerr << "URL: " << cur->url().data() << std::endl;

	if (cur->icyUrl())
	    std::cerr << "ICYURL: " << cur->icyUrl().data() << std::endl;
    }
}

void RadioStreamReadThread::run()
{
    CURLMcode rc;

    if (stream->isLocal()) {
	std::vector<unsigned char> buf;
	using ::read;
	MUTEX_LOCK(stream->readMutex);

	if (stream->total > stream->inbufSize/2) {
	    MUTEX_UNLOCK(stream->readMutex);
	    goto fetch;
	}

	buf.resize(stream->inbufSize/2);
	ssize_t len = read(stream->fileFd, &buf[0], stream->inbufSize/2);
	MUTEX_UNLOCK(stream->readMutex);

	if (len == -1) {
	    stream->setError(QString(strerror(errno)));
	    stream->readRc = CURLM_INTERNAL_ERROR;
	}
	else if (len == 0)
	    stream->eof = true;
	else {
	    CurlWrite c(stream, &buf.front(), len);

	    if (!c.updateInbufSize())
		stream->readRc = CURLM_INTERNAL_ERROR;

	    c.process(true);
	    c.updateRecordBuf();
	}

	freeVector(buf);
    }

fetch:
    do {
	int running;
	CURLMsg *m;
	int count;

	rc = curl_multi_perform(stream->curlMHandle, &running);

	do {
	    m = curl_multi_info_read(stream->curlMHandle, &count);

	    if (m && m->msg == CURLMSG_DONE) {
		if (!stream->isPlaying() && !stream->fetchList.count())
		    exit();

		if (m->easy_handle == stream->curlHandle) {
		    stream->eof = true;

		    if (m->data.result) {
			stream->readRc = rc;
			stream->playListCurrent->setError(QString(curl_easy_strerror(m->data.result)));
			stream->playListCurrent->setBad(true);
			stream->playListCurrent->setPlayed(true);
			QApplication::postEvent(stream->parentO, EVENT(StreamError));
		    }

		    continue;
		}

		MUTEX_LOCK(stream->fetchMutex);

		for (unsigned i = 0; i < stream->fetchList.count(); i++) {
		    RadioStreamFetch *f = stream->fetchList.at(i);

		    if (f->handle() == m->easy_handle) {
			if (m->data.result)
			    stream->setFetchError(f->id(),
				    curl_easy_strerror(m->data.result));

			emit stream->streamFetch(f->id(), NULL);
			break;
		    }
		}

		MUTEX_UNLOCK(stream->fetchMutex);
	    }
	} while (m);
    } while (rc == CURLM_CALL_MULTI_PERFORM && stream->init);

    exit();
}

int RadioStream::startRecord(const QString &filename)
{
    if (recordBufferSize() <= 0)
	return ENOMEM;

    MUTEX_LOCK(recordMutex);
    freeVector(recordBuf);
    recordBuf.resize(recordBufferSize());
    recordBufLen = 0;
    _recordFilename = filename;
    recordFd = open(filename.data(), O_APPEND|O_CREAT|O_WRONLY);
    MUTEX_UNLOCK(recordMutex);
    return recordFd == -1 ? errno : 0;
}

void RadioStream::stopRecord(void)
{
    MUTEX_LOCK(recordMutex);

    if (recordFd != -1) {
	recordThread.start();
	recordThread.wait();
	freeVector(recordBuf);
	close(recordFd);
	recordFd = -1;

	QFileInfo f(_recordFilename);

	if (f.size() == 0)
	    unlink(_recordFilename.data());
    }

    MUTEX_UNLOCK(recordMutex);
    MUTEX_LOCK(playListMutex);
    sendPlayingEvent(playListCurrent);
    MUTEX_UNLOCK(playListMutex);
}

bool RadioStream::isRecording(void)
{
    return recordFd != -1;
}

void RadioStreamRecordThread::run(void)
{
    if (stream->recordFd != -1 && stream->recordBufLen > 0) {
	ssize_t len = write(stream->recordFd, &stream->recordBuf.front(),
		stream->recordBufLen);

	if (len > 0)
	    stream->recordBufLen -= len;
    }

    exit();
}

QString &RadioStream::recordFilename(void)
{
    return _recordFilename;
}

void RadioStream::setPaused(bool p)
{
    bool old = paused;

    paused = p;
    emit streamPaused(paused);
    MUTEX_LOCK(decodeMutex);

    if (paused)
	closeDsp(true);
    else if (old) {
	if (!soundId() && !isPlaying()) {
	    MUTEX_UNLOCK(decodeMutex);
	    return;
	}

	int n = openDsp(true);

	if (n) {
	    setError(strerror(n));
	    QApplication::postEvent(parentO, EVENT(StreamError));
	}

	MUTEX_LOCK(playListMutex);
	updateInfo(playListCurrent, decModule);
	MUTEX_UNLOCK(playListMutex);
    }

    MUTEX_UNLOCK(decodeMutex);
}

bool RadioStream::isPaused()
{
    return paused;
}

void RadioStreamScanThread::run(void)
{
    decodeModule *m = NULL;

    RadioPlayListItem *cur = stream->playListScanning;

    if (!cur->isLocal())
	return;

    QString s = QString(cur->url().right(cur->url().length()-7));

    if (access(s.data(), R_OK) == -1) {
	cur->setError(strerror(errno));
	goto fail;
    }

    QApplication::postEvent(stream->parentO, EVENT(StreamScanFormat));
    m = stream->findModuleFromFilename(cur, s);

    if (!m) {
	cur->setError(tr("unknown file format"));
	goto fail;
    }

    /* stream->scanBrowse is set in scanFormat(). */
    if (!stream->decModule && cur->isLocal() && !stream->scanBrowse &&
	    cur == stream->playListCurrent)
	stream->decModule = m;
    else
	stream->closeDecModule(&m);

    stream->playListScanning = NULL;
    QApplication::postEvent(stream->parentO, EVENT(StreamScanFormatComplete));
    exit();

fail:
    if (m)
	stream->closeDecModule(&m);

    cur->setBad(true);
    cur->setPlayed(true);

    if (cur == stream->playListCurrent && stream->isPlaying())
	stream->stop();
    else
	QApplication::postEvent(stream->parentO, EVENT(StreamFormat));

    stream->playListScanning = NULL;
    QApplication::postEvent(stream->parentO, EVENT(StreamScanFormatComplete));
    exit();
}

bool RadioStream::scanFormat(RadioPlayListItem *cur, bool browseMode)
{
    if (!cur)
	return false;

    if (scanFormatThread.running())
	return true;

    if (playListScanning != cur) {
	if (cur->frameInfo() || (!browseMode && isPlaying()) || cur->isBad()
		|| !isLocal(browseMode))
	    return false;

	playListScanning = cur;
	scanBrowse = browseMode;
	scanFormatThread.start();
    }

    return true;
}

void RadioStream::setSkipBad(bool value)
{
    _skipBad = value;
};

bool RadioStream::skipBad()
{
    return _skipBad;
}

bool RadioStream::removeCurrentPlayListEntry(bool browseMode)
{
    MUTEX_LOCK(playListMutex);
    bool b = false;

    if (playList()->playList.count() <= 1) {
	stop();
	playList()->playList.remove();
	playListCurrent = playListBrowsing = NULL;
	goto done;
    }

    if (browseMode) {
	if (playList()->playList.findRef(playListBrowsing) == -1)
	    playList()->playList.findRef(playListCurrent);
    }

    if (isPlaying() && playListCurrent == playList()->playList.current())
	b = true;

    playList()->playList.remove();

    if (browseMode) {
	unsigned cur = playList()->playList.at();

	if (playList()->playList.findRef(playListCurrent) == -1)
	    playListCurrent = playList()->playList.at(cur);

	playList()->playList.at(cur);
    }

done:
    bool rc = updatePlayList(browseMode);
    MUTEX_UNLOCK(playListMutex);
    
    if (b)
	restart();

    return rc;
}

bool RadioStream::removePlayListEntry(unsigned n)
{
    if ((int)n == playList()->playList.at())
	return removeCurrentPlayListEntry();

    MUTEX_LOCK(playListMutex);
    playList()->playList.remove(n);
    updatePlayList();
    MUTEX_UNLOCK(playListMutex);
    return true;
}

bool RadioStream::addPlayListItem(RadioPlayListItem *i)
{
    if (!i)
	return false;

    MUTEX_LOCK(playListMutex);
    int n = playList()->playList.at();
    playList()->append(i);
    playList()->playList.at(n);
    MUTEX_UNLOCK(playListMutex);
    return true;
}

int RadioStream::savePlayList(const QString &f)
{
    MUTEX_LOCK(playListMutex);
    int n = playList()->writeToFile(f);
    MUTEX_UNLOCK(playListMutex);
    return n;
}

void RadioStream::flushFillBuffer()
{
    doFlushFillBuffer = true;
}

void RadioStream::setFillOnEmpty(bool value)
{
    _fillOnEmpty = value;

    if (doFill && !value)
	doFill = false;
};

bool RadioStream::fillOnEmpty()
{
    return _fillOnEmpty;
}

bool RadioStream::isBuffering()
{
    return doFill;
}

bool RadioStream::isLocal(bool browseMode)
{
    if (browseMode)
	return playListBrowsing ? playListBrowsing->isLocal() : true;

    return playListCurrent ? playListCurrent->isLocal() : true;
}

unsigned RadioStream::playTTS(const QString &str, bool cancel,
	bool loop, int interval)
{
#ifndef WITH_TTS
    (void)str;
    (void)cancel;
    (void)loop;
    (void)interval;
    return 0;
#else
    if (str == QString::null)
	return soundId();

    return playSoundW->play(str, cancel, loop, interval, true);
#endif
}

unsigned RadioStream::playSound(const QString &filename, bool cancel,
	bool loop, int interval)
{
    if (filename == QString::null)
	return soundId();

    std::cerr << __FUNCTION__ << "(): " << filename.data() << std::endl;
    return playSoundW->play(filename, cancel, loop, interval);
}

void RadioStream::setSoundVolume(int n)
{
    _soundVolume = n;
}

int RadioStream::soundVolume()
{
    return _soundVolume;
}

unsigned RadioStream::soundId()
{
    return playSoundW->soundId();
}

void RadioStream::stopSound()
{
    playSoundW->stop();
}

int RadioStream::playListAt(bool browseMode)
{
    if (!playList()->playList.count())
	return -1;

    if (browseMode) {
	MUTEX_LOCK(playListMutex);
	int cur = playList()->playList.at();
	int n = playList()->playList.findRef(playListBrowsing);
	playList()->playList.at(cur);

	if (n == -1)
	    playListBrowsing = playList()->playList.current();

	MUTEX_UNLOCK(playListMutex);
	return n == -1 ? cur : n;
    }

    return playList()->playList.findRef(playListCurrent);
}

bool RadioStream::playListAt(unsigned n, bool browseMode)
{
    MUTEX_LOCK(playListMutex);
    unsigned cur = playList()->playList.at();
    RadioPlayListItem *p = playList()->playList.at(n);

    if (!p) {
	playList()->playList.at(cur);
	MUTEX_UNLOCK(playListMutex);
	return false;
    }

    bool rc = updatePlayList(browseMode);
    MUTEX_UNLOCK(playListMutex);

    if (rc && !browseMode && isPlaying() && !isBad())
	restart();

    return rc;
}

bool RadioStream::playListAt(RadioPlayListItem  *item, bool browseMode)
{
    MUTEX_LOCK(playListMutex);
    unsigned cur = playList()->playList.at();
    int n = playList()->playList.findRef(item);

    if (n < 0) {
	playList()->playList.at(cur);
	MUTEX_UNLOCK(playListMutex);
	return false;
    }

    bool rc = updatePlayList(browseMode);
    MUTEX_UNLOCK(playListMutex);

    if (rc && !browseMode && isPlaying() && !isBad())
	restart();

    return rc;
}

void RadioStream::resetPlayed()
{
    MUTEX_LOCK(playListMutex);
    unsigned cur = playList()->playList.at();
    unsigned i;

    for (i = 0; i < playList()->playList.count(); i++) {
	RadioPlayListItem *t = playList()->playList.at(i);
	t->setPlayed(false);
    }

    playList()->playList.at(cur);
    MUTEX_UNLOCK(playListMutex);
}

int RadioStream::randomPlayListItem(bool *reset)
{
    MUTEX_LOCK(playListMutex);
    QList<RadioPlayListRandomItem> items;
    items.setAutoDelete(true);
    unsigned i;
    int cur = playList()->playList.at(), result;

    *reset = false;

    if (cur == -1)
	return -1;

again:
    for (i = 0; i < playList()->playList.count(); i++) {
	RadioPlayListItem *t = playList()->playList.at(i);

	if (!t->played() && !t->isBad())
	    items.append(new RadioPlayListRandomItem(i));
    }

    if (!*reset && !items.count()) {
	*reset = true;
	resetPlayed();
	goto again;
    }

    do {
	RadioPlayListRandomItem *t = items.at(random() % items.count());
	result = t->value();
    } while (result == cur && playList()->playList.count() > 1);

    items.clear();
    playList()->playList.at(cur);
    MUTEX_UNLOCK(playListMutex);
    return result;
}

RadioPlayListItem *RadioStream::current(bool browseMode)
{
    MUTEX_LOCK(playListMutex);

    if (browseMode) {
       if (!playListBrowsing)
	   playListBrowsing = playListCurrent;
       else {
	   int cur = playList()->playList.at();

	   if (playList()->playList.findRef(playListBrowsing) == -1)
	       playListBrowsing = playListCurrent;

	   playList()->playList.at(cur);
       }
    }

    RadioPlayListItem *p = browseMode ? playListBrowsing : playListCurrent;
    MUTEX_UNLOCK(playListMutex);
    return p;
}

void RadioStream::sendStreamEvents(RadioPlayListItem *cur)
{
    MUTEX_LOCK(playListMutex);
    sendPlayingEvent(cur);
    MUTEX_UNLOCK(playListMutex);
    QApplication::postEvent(parentO, EVENT(StreamFormat));
    QApplication::postEvent(parentO, EVENT(StreamConnected));
}

unsigned RadioStream::playListTotal()
{
    return playList()->playList.count() ? playList()->playList.count() : 0;
}

bool RadioStream::isBad(bool browseMode)
{
    if (browseMode)
	return playListBrowsing ? playListBrowsing->isBad() : true;

    return playListCurrent->isBad();
}

QString RadioStream::error(int n, bool browseMode)
{
    MUTEX_LOCK(playListMutex);

    if ((n == -1 && !browseMode && !playListCurrent) ||
	    (n == -1 && browseMode && !playListBrowsing)) {
	MUTEX_UNLOCK(playListMutex);
	return QString::null;
    }
    else if (n == -1) {
	MUTEX_UNLOCK(playListMutex);
	return browseMode ? playListBrowsing->error() : playListCurrent->error();
    }

    unsigned cur = playList()->playList.at();
    RadioPlayListItem *tmp = playList()->playList.at(n);
    playList()->playList.at(cur);
    MUTEX_UNLOCK(playListMutex);
    return tmp ? tmp->error() : QString::null;
}

void RadioStream::setError(const QString &s)
{
    if (playListCurrent)
	playListCurrent->setError(s);
}

void RadioStream::setDecodeWait(unsigned n)
{
    _decodeWait = n;
}

unsigned RadioStream::decodeWait()
{
    return _decodeWait;
}

void RadioStream::setReadInterval(unsigned n)
{
    _readInterval = n;
}

unsigned RadioStream::readInterval()
{
    return _readInterval;
}

void RadioStream::setDecodeInterval(unsigned n)
{
    _decodeInterval = n;
}

unsigned RadioStream::decodeInterval()
{
    return _decodeInterval;
}

void RadioStream::setScanEnabled(bool v, unsigned interval)
{
    scanTimer->stop();

    if (!v)
	scanElapsed = 0;
    else {
	scanInterval = interval;
	scanTimer->start(1000, true);
    }
}

bool RadioStream::isScanning()
{
    return scanTimer->isActive();
}

void RadioStream::slotScanTimer()
{
    if (decodeThread.running() && !isBuffering() && !isPaused() &&
	    ++scanElapsed >= scanInterval) {
	scanElapsed = 0;
	QApplication::postEvent(parentO, EVENT(StreamScan));
    }

    scanTimer->start(1000, true);
}

bool RadioStream::sortPlayList(int type, bool reverse)
{
    MUTEX_LOCK(playListMutex);
    bool rc = playList()->setSortType(type, reverse);
    playList()->playList.sort();
    MUTEX_UNLOCK(playListMutex);
    sendStreamEvents(NULL);
    return rc;
}

int RadioStream::sortType(bool *r)
{
    return playList()->sortType(r);
}

bool RadioStream::hasSoundId(unsigned id)
{
    return playSoundW->hasSoundId(id);
}

bool RadioStream::removeSoundId(unsigned n)
{
    return playSoundW->removeId(n);
}

void RadioStream::slotFetch()
{
    if (readThread.running())
	return;

    init = false;
    MUTEX_LOCK(fetchMutex);

    for (unsigned i = 0; i < fetchList.count(); i++) {
	RadioStreamFetch *f = fetchList.at(i);

	if (f->canceled()) {
	    i = 0;
	    fetchList.remove(f);
	}
    }

    if (!fetchList.count()) {
	fetchTimer->stop();
	MUTEX_UNLOCK(fetchMutex);
	return;
    }

    readThread.start();
    MUTEX_UNLOCK(fetchMutex);
}

unsigned RadioStream::fetches()
{
    return fetchList.count();
}

unsigned RadioStream::fetch(const QString &url)
{
    MUTEX_LOCK(fetchMutex);
    fetchList.append(new RadioStreamFetch(this, url));
    init = true;

    if (!fetchTimer->isActive())
	fetchTimer->start(readInterval());

    MUTEX_UNLOCK(fetchMutex);
    return fetchList.current()->id();
}

bool RadioStream::unfetch(unsigned id)
{
    MUTEX_LOCK(fetchMutex);

    for (unsigned i = 0; i < fetchList.count(); i++) {
	RadioStreamFetch *f = fetchList.at(i);

	if (f->id() == id) {
	    f->cancel();
	    MUTEX_UNLOCK(fetchMutex);
	    return true;
	}
    }

    MUTEX_UNLOCK(fetchMutex);
    return false;
}

void RadioStream::setFetchError(unsigned id, const QString str)
{
    MUTEX_LOCK(fetchMutex);
    fetchErrorList.append(new RadioStreamFetchError(id, str));
    MUTEX_UNLOCK(fetchMutex);
}

const QString RadioStream::fetchError(unsigned id)
{
    MUTEX_LOCK(fetchMutex);

    for (unsigned i = 0; i < fetchErrorList.count(); i++) {
	RadioStreamFetchError *f = fetchErrorList.at(i);

	if (f->id() == id) {
	    unfetch(id);
	    QString s = f->error();
	    fetchErrorList.remove(f);
	    MUTEX_UNLOCK(fetchMutex);
	    return s;
	}
    }

    unfetch(id);
    MUTEX_UNLOCK(fetchMutex);
    return QString::null;
}

bool RadioStream::fetchId(unsigned id)
{
    MUTEX_LOCK(fetchMutex);

    for (unsigned i = 0; i < fetchList.count(); i++) {
	RadioStreamFetch *f = fetchList.at(i);

	if (f->id() == id) {
	    MUTEX_UNLOCK(fetchMutex);
	    return true;
	}
    }

    MUTEX_UNLOCK(fetchMutex);
    return false;
}

bool RadioStream::search(const QString &str, SearchType type, bool browseMode)
{
    MUTEX_LOCK(playListMutex);
    unsigned cur = playList()->playList.at();
    int found = -1;
    unsigned i = cur+1;
    bool last = false;
    bool cs = !QString::compare(str.lower(), str) ? false : true;
    
again:
    for (; i < playList()->playList.count() && i != cur; i++) {
	RadioPlayListItem *p = playList()->playList.at(i);

	if (!p)
	    break;

	switch (type) {
	    case Id:
		if (!QString::compare(QString("%1").arg(p->id()), str))
		    found = i;

		break;
	    case StreamTitle:
		if (p->streamTitle().find(str, 0, cs) != -1)
		    found = i;

		break;
	    case Title:
		if (p->title().find(str, 0, cs) != -1)
		    found = i;

		break;
	    case Album:
		if (p->album().find(str, 0, cs) != -1)
		    found = i;

		break;
	    case Artist:
		if (p->artist().find(str, 0, cs) != -1)
		    found = i;

		break;
	    case Url:
		if (p->url().find(str, 0, cs) != -1)
		    found = i;

		break;
	    default:
		break;
	}

	if (found != -1)
	    break;
    }

    if (found == -1 && !last) {
	i = 0;
	last = true;
	goto again;
    }

    bool rc = false;

    if (found != -1)
	rc = playListAt(found, browseMode);
    else
	playList()->playList.at(cur);

    MUTEX_UNLOCK(playListMutex);
    return rc;
}
