//qt
#ifndef FILEBROWSER_H
#define FILEBROWSER_H
#include <qlistbox.h>
#include <ezxutillistbox.h>
class FileBrowser : public QListBox
{
    Q_OBJECT
public:
	enum {
	    FilesOnly, DirsOnly, All
	};

	//dirfile = 0,只显示目录.  dirfile =1,显示所有   by  nukin
    FileBrowser(const QString &filter, QWidget *parent = 0, const char *name = 0, const int &dirfile = 1, int keyUp = 0, int keyDown = 0, int keySelect = 0);
    virtual ~FileBrowser();
	
    void setDir(const QString &path);
    QString getFileName() { return fileName; }            //文件完整路径
    QString getDirPath() { return curPath; }              //当前列表的父路径(当前目录)
    QString getFilePath() { return basePath; }            //被选文件(夹)所在目录
    QString getdirName() { return DirPath; }              //被选文件夹的完整路径
	
signals:
    void picked(const QString &fileName);
    void isFilePicked(bool);
    void isDirPicked(bool);
    void isFileClicked(bool);
    void dirpicked(const QString &dirName);
    void mouseEvent1(const QString &dirName);
private slots:
    void itemHighlighted(int index);
    void itemSelected(int index);    
    void itemClicked(QListBoxItem *);
    void keyPressEvent(QKeyEvent *);
	
	
private:	
	QString fileview;
	QString nameFilter;
    QString basePath;
    QString fileName;
    QString DirPath;
    QString curPath;
    int type;
    int keyUp;
    int keyDown;
    int keySelect;
};
#endif //FILEBROWSER_H
