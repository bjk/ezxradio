/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <qfontmetrics.h>
#include "EZXRadioMarquee.h"

void EZXRadioMarquee::startTimer()
{
    pos = 0;
    timer->start(interval());
}

void EZXRadioMarquee::sleep()
{
    if (timer->isActive()) {
	timer->stop();
	QTimer::singleShot(sleepInterval(), this, SLOT(sleep()));
    }
    else {
	if (pos) {
	    pos = 0;
	    updateLabel();
	    QTimer::singleShot(sleepInterval(), this, SLOT(startTimer()));
	}
	else
	    timer->start(interval());
    }
}

void EZXRadioMarquee::updateLabel()
{
    QFontMetrics fm(this->font());
    QWidget *p = parentWidget();
    int w = p->geometry().width();
    int a = fm.width(orig);
    unsigned i, n;
    unsigned len = orig.length();
    int sw = 0;

    /* Failsafe. */
    if (a <= w) {
	timer->stop();
	return;
    }

    if (!interval())
	pos = 0;

    n = w / fm.maxWidth();

    for (i = pos; i < len; i++, n++) {
	sw = fm.width(orig.mid(pos, n));

	if (sw >= w)
	    break;
    }

    QLabel::setText(orig.mid(pos++, n));

    if (!interval()) {
	timer->stop();
	return;
    }

    if ((n-1) + pos-1 > len) {
	timer->stop();
	QTimer::singleShot(sleepInterval(), this, SLOT(sleep()));
	return;
    }
}

EZXRadioMarquee::EZXRadioMarquee(const QString &str, QWidget *parent, int i,
	int si) : QLabel(str, parent)
{
    _interval = i;
    _sleepInterval = si;
    timer = new QTimer(this, NULL);
    timer->changeInterval(_interval);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateLabel()));
    pos = 0;
    list.setAutoDelete(true);
}

void EZXRadioMarquee::setText(const QString &str)
{
    QFontMetrics fm(this->font());
    QWidget *p = parentWidget();
    int a = fm.width(str);
    int b = p->geometry().width();
    unsigned len = str.length();

    timer->stop();
    pos = 0;
    orig = str;

    if (a > b) {
	for (unsigned i = 1; i < len; i++) {
	    int sw = fm.width(str, i);

	    if (sw > b) {
		pos = i;
		break;
	    }
	}

	QLabel::setText(str.left(pos));
	QLabel::setAlignment(AlignLeft);
	QTimer::singleShot(sleepInterval(), this, SLOT(sleep()));
    }
    else {
	QLabel::setAlignment(AlignHCenter);
	QLabel::setText(str);
    }
}

EZXRadioMarquee::~EZXRadioMarquee()
{
    timer->stop();
    delete timer;
    list.clear();
}

void EZXRadioMarquee::setInterval(int value)
{
    if (value < 0)
	value = 0;

    _interval = value;
    timer->changeInterval(_interval);
}

int EZXRadioMarquee::interval(void)
{
    return _interval;
}

void EZXRadioMarquee::start(void)
{
    timer->start(interval());
}

void EZXRadioMarquee::stop(void)
{
    timer->stop();
}

bool EZXRadioMarquee::isActive(void)
{
    return timer->isActive();
}

void EZXRadioMarquee::setSleepInterval(int value)
{
    _sleepInterval = value;
}

int EZXRadioMarquee::sleepInterval(void)
{
    return _sleepInterval;
}

void EZXRadioMarquee::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    emit MarqueeMousePressEvent(this);
}

void EZXRadioMarquee::mouseReleaseEvent(QMouseEvent *ev)
{
    (void)ev;
    emit MarqueeMouseReleaseEvent(this);
}

void EZXRadioMarquee::mouseDoubleClickEvent(QMouseEvent *ev)
{
    (void)ev;
    emit MarqueeMouseDoubleClickEvent(this);
}

void EZXRadioMarquee::addText(const QString &str, bool set)
{
    int n = list.at();

    list.append(new MarqueeString(str.isEmpty() ? tr("unspecified") : str));

    if (!set)
	list.at(n);
    else {
	MarqueeString *m = list.current();
	setText(m->text());
    }
}

void EZXRadioMarquee::resetText(void)
{
    list.clear();
    setText("");
}

void EZXRadioMarquee::next(void)
{
    if (list.count() <= 1)
	return;

    if (!list.next())
	list.first();

    MarqueeString *m = list.current();
    setText(m->text());
}

void EZXRadioMarquee::prev(void)
{
    if (!list.prev())
	list.last();

    MarqueeString *m = list.current();
    setText(m->text());
}

int EZXRadioMarquee::at()
{
    return list.at();
}

bool EZXRadioMarquee::at(int n)
{
    MarqueeString *m = list.at(n);

    if (!m)
	return false;

    setText(m->text());
    return true;
}
