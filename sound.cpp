/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/soundcard.h>
#include "sound.h"

#define READ_BUFSIZE	8196

RadioStreamSound::RadioStreamSound(RadioStream *parent) : thread(this)
{
#ifdef WITH_TTS
    espeak_Initialize(AUDIO_OUTPUT_SYNCHRONOUS, 500, "/mmc/mmca1/.system/QTDownLoad/EZXRadio/lib", 0);
    espeak_SetSynthCallback(espeakCallback);
#endif
    fileList.setAutoDelete(true);
    stream = parent;
    playing = _playing = paused = false;
    connect(stream, SIGNAL(streamPaused(bool)), this, SLOT(slotStreamPaused(bool)));
    mutex = new QMutex(true);
}

unsigned RadioStreamSound::soundId()
{
    return playing ? fileList.current()->soundId() : 0;
}

unsigned RadioStreamSound::play(const QString &file, bool c, bool l,
	unsigned i, bool t)
{
    if (!file || file.isEmpty() || paused)
	return 0;

    MUTEX_LOCK(mutex);
    fileList.append(new RadioStreamSoundItem(file, c, l, i, t));
    unsigned id = fileList.current()->soundId();
    RadioStreamSoundItem *item = fileList.at(1);

    if (item) {
	while (fileList.count() > 2 && fileList.current()->cancel()) {
	    fileList.remove(fileList.current());
	    fileList.at(1);
	}
    }

    fileList.at(0);

    if (fileList.current()->cancel())
	stop();

    MUTEX_UNLOCK(mutex);

    if (!thread.running())
	thread.start();

    return id;
}

void RadioStreamSound::stop()
{
    MUTEX_LOCK(mutex);
    _playing = false;
    MUTEX_UNLOCK(mutex);
}

#ifdef WITH_TTS
int RadioStreamSound::espeakCallback(short *wav, int n_samples,
	espeak_EVENT *ev)
{
    int rc = 0;
    RadioStreamSound *sound = (RadioStreamSound *)ev->user_data;

    MUTEX_LOCK(sound->mutex);

    if (sound->paused && sound->fileList.current()->cancel()) {
	rc = 1;
	goto done;
    }

    if (sound->paused) {
	sound->stream->closeDsp();

	while (sound->paused && sound->_playing) {
	    MUTEX_UNLOCK(sound->mutex);
	    usleep(150);
	    MUTEX_LOCK(sound->mutex);
	}

	if (!sound->paused) {
	    if (sound->stream->openDsp(false)) {
		rc = 1;
		goto done;
	    }

	    sound->stream->dsp->setVolume(sound->stream->soundVolume() < sound->stream->volume() ? sound->stream->soundVolume() : sound->stream->volume());
	}
    }

    if (!sound->_playing) {
	rc = 1;
	goto done;
    }

    switch (ev->type) {
	case espeakEVENT_LIST_TERMINATED:
	case espeakEVENT_END:
	case espeakEVENT_SENTENCE:
	case espeakEVENT_WORD:
	    if (n_samples)
		sound->stream->dspWrite(wav, sizeof(short) * n_samples);

	    if (ev->type == espeakEVENT_LIST_TERMINATED)
		sound->stream->dspSync(false);

	    break;
	default:
	    break;
    }

done:
    MUTEX_UNLOCK(sound->mutex);
    return rc;
}
#endif

bool RadioStreamSoundThread::doPlay(RadioStreamSoundItem *item)
{
    int rc = true;
    decodeModule *m;
    int fd;
    unsigned char *buf;

#ifdef WITH_TTS
    if (item->tts()) {
	sound->stream->setDspFormat(1, 22050/2, AFMT_S16_NE);
	espeak_Synth(item->data(), item->data().length(), 0, POS_CHARACTER,
		espeakCHARS_AUTO, 0, NULL, sound);
	goto done;
    }
#endif

    m = sound->stream->findModuleFromFilename(NULL, item->data());

    if (!m)
	return false;

    fd = open(item->data().data(), O_RDONLY);

    if (fd == -1) {
	sound->stream->closeDecModule(&m);
	return false;
    }

    buf = new unsigned char[READ_BUFSIZE];

    if (!buf) {
	rc = false;
	goto fail;
    }

    rc = true;

    do {
	MUTEX_LOCK(sound->mutex);

	if (!sound->_playing) {
	    MUTEX_UNLOCK(sound->mutex);
	    break;
	}

	MUTEX_UNLOCK(sound->mutex);
	using :: read;
	size_t len = read(fd, buf, READ_BUFSIZE);
	void *data;

	if (len <= 0) {
	    rc = false;
	    break;
	}

	MUTEX_LOCK(sound->stream->decodeMutex);
	rc = (*m->feed)(m->index, buf, len);
	MUTEX_UNLOCK(sound->stream->decodeMutex);

	if (rc != DEC_MOD_OK) {
	    rc = false;
	    break;
	}

	do {
	    MUTEX_LOCK(sound->mutex);

	    if (sound->paused && sound->fileList.current()->cancel()) {
		rc = DEC_MOD_ERR;
		MUTEX_UNLOCK(sound->mutex);
		break;
	    }

	    if (sound->paused) {
		sound->stream->closeDsp();

		while (sound->paused && sound->_playing) {
		    MUTEX_UNLOCK(sound->mutex);
		    msleep(150);
		    MUTEX_LOCK(sound->mutex);
		}

		if (!sound->paused) {
		    if (sound->stream->openDsp(false)) {
			rc = DEC_MOD_ERR;
			MUTEX_UNLOCK(sound->mutex);
			break;
		    }

		    sound->stream->dsp->setVolume(sound->stream->soundVolume() < sound->stream->volume() ? sound->stream->soundVolume() : sound->stream->volume());
		}
	    }

	    if (!sound->_playing) {
		MUTEX_UNLOCK(sound->mutex);
		break;
	    }

	    MUTEX_UNLOCK(sound->mutex);
	    MUTEX_LOCK(sound->stream->decodeMutex);
	    rc = (*m->decodeFrame)(m->index, &data, &len);
	    MUTEX_UNLOCK(sound->stream->decodeMutex);

	    if (rc == DEC_MOD_NEWINFO) {
		MUTEX_LOCK(sound->stream->decodeMutex);
		m->info = (*m->getInfo)(m->index);
		MUTEX_UNLOCK(sound->stream->decodeMutex);

		if (m->info && (m->info->infoTypes & DEC_INFO_FORMAT))
		    sound->stream->setDspFormat(m->info->channels,
			    m->info->frameRate, m->info->format);
	    }

	    if (len) {
		MUTEX_LOCK(sound->mutex);

		if (!sound->_playing) {
		    MUTEX_UNLOCK(sound->mutex);
		    break;
		}

		MUTEX_UNLOCK(sound->mutex);
		MUTEX_LOCK(sound->stream->dspMutex);
		sound->stream->dspWrite(data, len);
		MUTEX_UNLOCK(sound->stream->dspMutex);
	    }
	    else if (!len && rc == DEC_MOD_OK)
		break;
	} while (rc == DEC_MOD_OK || rc == DEC_MOD_NEWINFO);

	rc = rc != DEC_MOD_ERR ? true : false;

	if (rc && sound->_playing)
	    msleep(sound->stream->decodeInterval());
    } while (rc);

    rc = rc != DEC_MOD_ERR ? true : false;

fail:
    if (buf)
	delete[] buf;

    close(fd);
    sound->stream->closeDecModule(&m);

#ifdef WITH_TTS
done:
#endif
    if (sound->_playing) {
	MUTEX_LOCK(sound->stream->dspMutex);
	sound->stream->dspSync();
	MUTEX_UNLOCK(sound->stream->dspMutex);
    }

    if (sound->stream->decModule && sound->stream->decModule->info &&
	    (sound->stream->decModule->info->infoTypes & DEC_INFO_FORMAT))
	sound->stream->setDspFormat(sound->stream->decModule->info->channels,
		sound->stream->decModule->info->frameRate,
		sound->stream->decModule->info->format);

    return rc;
}

void RadioStreamSoundThread::run()
{
    if (sound->stream->openDsp(false))
	return;

    MUTEX_LOCK(sound->mutex);
again:
    sound->stream->dsp->setVolume(sound->stream->soundVolume() < sound->stream->volume() ? sound->stream->soundVolume() : sound->stream->volume());
    sound->_playing = sound->playing = true;
    RadioStreamSoundItem *item = sound->fileList.current();
    int s = item->interval() * 1000;
    MUTEX_UNLOCK(sound->mutex);

    if (!doPlay(item))
	goto done;

    while (item->loop()) {
	MUTEX_LOCK(sound->mutex);

	if (!sound->_playing) {
	    MUTEX_UNLOCK(sound->mutex);
	    break;
	}

	MUTEX_UNLOCK(sound->mutex);
	unsigned n = sound->stream->decodeInterval();

	msleep(n);
	s -= n;

	if (s <= 0) {
	    MUTEX_LOCK(sound->mutex);
	    goto again;
	}
    }

done:
    MUTEX_LOCK(sound->mutex);
    sound->fileList.remove(sound->fileList.current());

    /* Find a new sound that cannot be canceled and remove all previous ones
     * from the queue. */
    for (unsigned i = 0; i < sound->fileList.count(); i++) {
	sound->fileList.at(i);

	if (!sound->fileList.current()->cancel()) {
	    sound->fileList.at(0);

	    while (sound->fileList.count() > 1 &&
		    sound->fileList.current()->cancel()) {
		sound->fileList.remove(sound->fileList.current());
		sound->fileList.at(0);
	    }

	    break;
	}
    }

    if (sound->fileList.at(0))
	goto again;

    sound->stream->closeDsp();
    sound->stream->dsp->setVolume(sound->stream->volume());
    sound->playing = sound->_playing = false;
    MUTEX_UNLOCK(sound->mutex);
}

void RadioStreamSound::slotStreamPaused(bool b)
{
    MUTEX_LOCK(mutex);
    paused = b;
    MUTEX_UNLOCK(mutex);
}

bool RadioStreamSound::hasSoundId(unsigned id)
{
    MUTEX_LOCK(mutex);
    int cur = fileList.at();

    for (unsigned i = 0; i < fileList.count(); i++) {
	RadioStreamSoundItem *s = fileList.at(i);

	if (s->soundId() == id) {
	    fileList.at(cur);
	    MUTEX_UNLOCK(mutex);
	    return true;
	}
    }

    fileList.at(cur);
    MUTEX_UNLOCK(mutex);
    return false;
}

bool RadioStreamSound::removeId(unsigned id)
{
    MUTEX_LOCK(mutex);
    int cur = fileList.at();

    for (unsigned i = 0; i < fileList.count(); i++) {
	RadioStreamSoundItem *s = fileList.at(i);

	if (s->soundId() == id) {
	    if (soundId() == id)
		stop();
	    else {
		fileList.remove(s);
		fileList.at(cur);
	    }

	    MUTEX_UNLOCK(mutex);
	    return true;
	}
    }

    MUTEX_UNLOCK(mutex);
    return false;
}
