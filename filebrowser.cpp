#include "filebrowser.h"

//ezx
#include <ZGlobal.h>
#include <ezxres.h>
#include <qtimer.h> 
#include <qdatetime.h>

//qt
#include <qdir.h>

FileBrowser::FileBrowser(const QString &filter, QWidget *parent, const char *name, const int &dirfile, int ku, int kd, int ks)
        :QListBox(parent, name)
{
      
    fileName = "";
    nameFilter = filter;
    type = dirfile;
    keyUp = ku;
    keyDown = kd;
    keySelect = ks;
    connect(this, SIGNAL(highlighted(int)), this, SLOT(itemHighlighted(int)));
    connect(this, SIGNAL(selected(int)), this, SLOT(itemSelected(int)));
    connect(this, SIGNAL(clicked(QListBoxItem *)), this, SLOT(itemClicked(QListBoxItem *)));

//setSelectionMode(QListBox::Extended);
}
    
FileBrowser::~FileBrowser()
{
}
    
void FileBrowser::setDir(const QString &path)
{
    RES_ICON_Reader ir;
    
    QDir dir(path, nameFilter);    
    dir.setMatchAllDirs(true);
    
    dir.setFilter(QDir::Dirs | QDir::Hidden);
    
    if (!dir.isReadable())
        return;
    clear();

		QStringList entries = dir.entryList();
   if (!dir.isRoot()){
				QString st = "..";
				insertItem(ir.getIcon("FMMS_Personal_Folder_S.gif",1),st);
			}
    QStringList::ConstIterator it = entries.begin();
    while (it != entries.end()) {
        if ((*it != ".")&&(*it != "..")) {
            insertItem(ir.getIcon("FMMS_Personal_Folder_S.gif",1), *it);
        }
        ++it;
    }

	dir.setFilter(QDir::Files | QDir::Hidden);    
	entries = dir.entryList();
	it = entries.begin();
	while (it != entries.end()) {
	    if (*it != "." && *it != "..") {
		insertItem(ir.getIcon("FMMS_Personal_File_S.gif",1), *it);
	    }
	    ++it;
	}
	
	curPath = dir.absPath();
    basePath = dir.canonicalPath();
    setSelected(0, true);
}
    
void FileBrowser::keyPressEvent(QKeyEvent *k)
{
    if (currentItem() == 0 && k->key() == keyUp)
	setSelected(count()-1, true);
    else if (currentItem() == (int)count()-1 && k->key() == keyDown)
	setSelected(0, true);
    else if (k->key() == keyUp)
	setSelected(currentItem()-1, true);
    else if (k->key() == keyDown)
	setSelected(currentItem()+1, true);
    else if (k->key() == keySelect)
	emit itemSelected(currentItem());
    else
	k->ignore();

    centerCurrentItem();
}

void FileBrowser::itemHighlighted(int index)
{
    QString path = basePath + "/" + text(index);
    if (QFileInfo(path).isFile()) {
        fileName = path;
        emit picked(path);
        emit isFilePicked(TRUE);
    } else {
        fileName = "";
        emit isFilePicked(FALSE);
    }
      
if (QFileInfo(path).isDir()/*QDir(path).dirName()!= ".."*/){
       DirPath =path;
    emit dirpicked(path);
    emit isDirPicked(TRUE);
   
    }else {
        DirPath = "";
        emit isDirPicked(FALSE);
    }

if (QDir(basePath).isRoot()){
       DirPath = basePath+ text(index);
    //emit dirpicked(DirPath);
    emit isDirPicked(TRUE);
    }

}

void FileBrowser::itemSelected(int index)
{
    QString path = basePath + "/" + text(index);
    if (QFileInfo(path).isDir())
       
      setDir(path);
    
    if (QFileInfo(path).isFile()) {
        fileName = path;
       
        emit isFileClicked(TRUE);
    } else {
       
        emit isFileClicked(FALSE);
    }
}
    
void FileBrowser::itemClicked(QListBoxItem *item)
{
   QString path = basePath + "/" + text(index(item));
   if (QFileInfo(path).isDir())
      setDir(path);
}
